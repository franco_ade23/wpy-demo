export const colors = {
    accentColor: '#67bc87',
    primaryColor: '#0051c0',
    footerBgColor: '#242323',
    grey3: '#7c7c7c',
    grey4: '#939393',
}