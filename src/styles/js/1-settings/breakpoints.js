export default {
    breakpointXs: '320px',
    breakpointSm: '480px',
    breakpointMd: '768px',
    breakpointLg: '992px',
    breakpointXl: '1200px'
}