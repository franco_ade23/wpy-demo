import moment from 'moment';

export const formatToDDMMYYYY = ( date ) => {
    return moment(date).format('DD/MM/YYYY');
}