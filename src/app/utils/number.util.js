export const formatAmount = (amount) => {
    return String(amount).replace(/(?!^)(?=(?:\d{3})+(?:\.|$))/gm, ',')
}