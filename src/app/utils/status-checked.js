import {APPROVED, ERROR, NO_AVAILABLE, PAYED, PENDING, REJECTED, StatusHuman} from '../constants/status.constants';

export const isStatusApproved = (status) => status.toUpperCase() === APPROVED;
export const isStatusError = (status) => status.toUpperCase() === ERROR;
export const isStatusNoAvailable = (status) => status.toUpperCase() === NO_AVAILABLE;
export const isStatusPayed = (status) => status.toUpperCase() === PAYED;
export const isStatusPending = (status) => status.toUpperCase() === PENDING;
export const isStatusRejected = (status) => status.toUpperCase() === REJECTED;


export const displayStatusHuman = (status) => (StatusHuman[status].text);
export const displayStatusColor = (status) => (StatusHuman[status].color);

/**
 * Obtiene datos del Stado
 * @params {string}
 * @return {object:{text,color}}
 * */
export const getStatus = (status) => (StatusHuman[status]);