/**
 * Función utilizada para solventar Bug de styled-components al cambiar de ruta
 * (la librería re-solicita el bg lo que ocasiona un parpadeo en la imagen de fondo).
 * TODO: Crear issue en Github
 * @param {*} param0 
 */
export const getBackgroundImageStyle = ({ backgroundImageUrl, position = 'center', size = 'cover' }) => {
    return {
        backgroundImage: `url(${backgroundImageUrl})`,
        position,
        size
    }
}