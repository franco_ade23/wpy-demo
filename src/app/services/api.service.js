import { StorageKeys } from "../constants/storage.constans";
import { EnvConstants } from "../constants/env.constants";

const validateResponse = async ( response ) => {
    if( response.ok ){
        return response.json()
    }else if( response.status === 400 ){
        throw await response.json();
    }else{
        throw response;
    }
}

export class Api{
    
    constructor(baseUrl){
        this.baseUrl = baseUrl;
    }

    setToken(token){
        this.token = token;
        localStorage.setItem(StorageKeys.AUTHENTICATION_TOKEN,token);
    }

    get token(){
        return localStorage.getItem(StorageKeys.AUTHENTICATION_TOKEN);
    }

    getHeaders(header = {}){
        header = {
            ...header,
            'Content-Type': 'application/json'
        }
        return header;
    }

    get(url, queryParams = {}){
        return fetch(
            this.setQueryParams(
                `${this.baseUrl}${url}`,
                queryParams
            ),{
            headers: this.getHeaders()
        })
        .then( validateResponse )
        .then( resp => resp.json ? resp.json() : resp );
    }

    post(url, body={}, queryParams = {}){
        console.log('----');
        console.log(body);
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            body: body instanceof FormData ? body : JSON.stringify(body),
            headers: this.getHeaders(),
            method: 'POST'
        })
        .then( validateResponse )
        .then( resp => resp.json ? resp.json() : resp );
    }

    put(url, body={}, queryParams = {}){
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            body: JSON.stringify(body),
            headers: this.getHeaders(),
            method: 'PUT'
        })
        .then( validateResponse )
        .then( resp => resp.json ? resp.json() : resp );
    }

    patch(url, body={}, queryParams = {}){
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            body: JSON.stringify(body),
            headers: this.getHeaders(),
            method: 'PATCH'
        })
        .then( validateResponse )
        .then( resp => resp.json ? resp.json() : resp );
    }

    delete(url, queryParams = {}){
        return fetch(this.setQueryParams(
            `${this.baseUrl}${url}`,
            queryParams
        ),{
            headers: this.getHeaders(),
            method: 'DELETE'
        })
        .then( validateResponse )
        .then( resp => resp.json ? resp.json() : resp );
    }

    setQueryParams(url, queryParams = {}){
        const queryParamsList = Object.keys(queryParams);
        if(queryParamsList.length === 0) return `${url}`;
        
        let queryString = Object.keys(queryParams).map( key => {
            return `${key}=${queryParams[key]}`;
        }).join('&');
        return `${url}?${queryString}`;
    }
}

var api = new Api( EnvConstants.BACKEND_URL );

export default api; 
