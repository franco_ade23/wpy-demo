import fetchIntercept from 'fetch-intercept';
import { unauthorizedRoutes } from '../constants/api.constants';
import { loadersService } from '../modules/Loaders/services/loaders.service';
import { StorageKeys } from '../constants/storage.constans';

const isUnauthorizedRoute = ( route = '' ) => {
    return unauthorizedRoutes.some( unauthorizedRoute => route.indexOf( unauthorizedRoute ) >= 0 );
}

const getToken = () => {
    return localStorage.getItem(StorageKeys.AUTHENTICATION_TOKEN);
}

const registerInterceptors = () => {
    fetchIntercept.register({
        request: ( url, config ) => {
            loadersService.showHttpLoader();
            if( !isUnauthorizedRoute( url ) ){
                config.headers = {
                    ...config.headers,
                    Authorization: `Token ${getToken()}`
                }
            }
            return [ url, config ];
        },
        response: ( response ) => {
            loadersService.hideHttpLoader();
            return response;
        },
        responseError: (response ) => {
            loadersService.hideHttpLoader();
            return response;
        }
    })
}

export const initInterceptors = () => {
    registerInterceptors();
}