import React from 'react';
import { Provider } from 'react-redux';
import store from './state/store';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Auth from './modules/Auth';
import ToastsCenter from './modules/Toasts/containers/ToastsCenter';
import { initInterceptors } from './services/http-interceptors.service';
import HttpLoaderListener from './modules/Loaders/containers/HttpLoaderListener';
import SelectProfile from './modules/SelectProfile';
import Initialize from './components/Initialize';
import NotLoggedRoute from './components/NotLoggedRoute';
import ProtectedRoute from './components/ProtectedRoute';
import Dashboard from './modules/Dashboard';
// import { RUTA_LANDING } from './constants/routes.constants';
import Landing from './modules/Landing';

initInterceptors();

function App() {
  console.log('APP');
  return (
    <Provider store={store}>
      <HttpLoaderListener></HttpLoaderListener>
      <ToastsCenter>
        <Initialize>
          <Router>
            <Switch>
              <NotLoggedRoute path="/auth" component={Auth}></NotLoggedRoute>
              <ProtectedRoute path="/seleccionar-perfil" component={SelectProfile}></ProtectedRoute>
              <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
              <Route component={Landing}></Route>
              {/* <Redirect to="/dashboard"></Redirect> */}
            </Switch>
          </Router>
        </Initialize>
      </ToastsCenter>
    </Provider>
  );
}

export default App;
