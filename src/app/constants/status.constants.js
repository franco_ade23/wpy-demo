/**
 * Lista de 'Status' usadas en la aplicacion por diferentes modelos
 */

export const NO_AVAILABLE = 'NO_AVAILABLE';
export const APPROVED = 'APPROVED';
export const PENDING = 'PENDING';
export const PAYED = 'PAYED';
export const REJECTED = 'REJECTED';
export const ERROR = 'ERROR';


export const StatusHuman = {
    [NO_AVAILABLE]: {
        text: 'NO DISPONIBLE',
        color: '#C3C1BE'
    },
    [APPROVED]: {
        text: 'APROBADO',
        color: '#169373'
    },
    [PENDING]: {
        text: 'PENDIENTE',
        color: '#E8B500'
    },
    [PAYED]: {
        text: 'PAGADO',
        color: '#00ADEF'
    },
    [REJECTED]: {
        text: 'RECHAZADO',
        color: '#F57421'
    },
    [ERROR]: {
        text: 'ERROR',
        color: '#E6534C'
    },
};