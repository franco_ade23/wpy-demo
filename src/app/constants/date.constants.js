import 'moment/locale/es';
import moment from 'moment';

moment.locale('es');

export const MONTHS = moment.months();
export const WEEKDAYS_LONG = moment.weekdays();
export const WEEKDAYS_SHORT = moment.weekdaysShort();

