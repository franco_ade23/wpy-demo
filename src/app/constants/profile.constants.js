export const WITHDRAWER_PROFILE = 'client-withdraw';
export const PAYMENT_PROFILE = 'client-payment';
export const NO_PROFILE = 'No defined';
export const WITHDRAWER_PROFILE_FOR_UPDATE = 'client_withdraw';
export const PAYMENT_PROFILE_FOR_UPDATE = 'client_payment';