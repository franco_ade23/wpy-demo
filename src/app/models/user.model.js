export class User {
  constructor({
    birthday = "",
    cellphone = "",
    dni = "",
    email = "",
    first_name = "",
    last_name = "",
    photo_url = "",
    referred_code = "",
    services = [],
    type_client = "",
    uid = "",
    wepoints = 0
  }) {
    this.birthday = birthday;
    this.cellphone = cellphone;
    this.dni = dni;
    this.email = email;
    this.firstName = first_name;
    this.lastName = last_name;
    this.photoUrl = photo_url;
    this.referredCode = referred_code;
    this.services = services;
    this.typeClient = type_client;
    this.uid = uid;
    this.wepoints = wepoints;
  }
}
