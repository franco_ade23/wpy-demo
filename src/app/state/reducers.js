import { combineReducers } from "redux";
import { toastsReducer } from "../modules/Toasts/state/reducer";
import { loadersReducer } from "../modules/Loaders/state/reducer";
import { authReducer } from "../modules/Auth/state/reducers";
import { selectProfileReducer } from "../modules/SelectProfile/state/reducers";
import paymentsReducer from "../modules/Payments/state/reducers";
import withdrawsReducer from '../modules/Withdraws/state/reducers'

const rootReducer = combineReducers({
	toasts: toastsReducer,
	pendingTasks: loadersReducer,
	auth: authReducer,
	selectProfile: selectProfileReducer,
	payments: paymentsReducer,
	withdraws: withdrawsReducer
});

export default rootReducer;
