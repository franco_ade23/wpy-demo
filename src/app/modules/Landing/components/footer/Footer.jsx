import React from 'react';
import styled from 'styled-components';
import Logo from '../../../../components/Logo';
import '../footer/footer.css';

const StyledFotter = styled.div`
    background-image: url('/images/landing/footer/footer-bg.png');
    background-position: center;
    background-size: 100% 100%;
    padding: 60px 0;
    color: #FFF;
    .footer__content{
        display: flex;
        margin: auto;
        max-width: 950px;
        padding-left:20px;
        padding-right:20px;
    }
    .footer__section{
        flex: 0 0 33%;
        *:first-child, p{
            margin-bottom: 35px;
        }
        .phoneIcon{
            margin: 0;
            margin-right: 20px;
        }
    }
`;

const Footer = () => {
    return (
        <StyledFotter>
            <div className="footer__content">
                <div className="footer__section">
                    <Logo variant="gray"></Logo>
                    <p>
                        La manera más fácil de obtener dinero <br/>
                        y ahorrar en tus pagos
                    </p>
                    <p>
                        contacto@wepayu.pe
                    </p>
                    <p>
                        RUC 20603531052
                    </p>
                    <p>
                        2019. WePayU. All rights reserved.
                    </p>
                </div>
                <div className="footer__section">
                    <h3><b>SOPORTE</b></h3>
                    {/* <p>
                        Preguntas frecuentes
                    </p>
                    <p>
                        Contáctanos
                    </p> */}
                    <p className="flex-row-layout ">
                        <img src="/images/landing/footer/telephone.png" alt="" className="phoneIcon"/>
                        <span>924695692</span>
                    </p>
                </div>
                <div className="footer__section">
                    <h3><b>OTROS</b></h3>
                    <p>
                        <a target="_blank"href="https://drive.google.com/file/d/10FfhrFVXh306Xo01Bhw91-73volp8abr/view">Términos y condiciones</a>
                    </p>
                    <p>
                        Políticas de Privacidad
                    </p>
                    {/* <p>
                        Libro de reclamaciones
                    </p> */}
                </div>
            </div>
        </StyledFotter>
    );
}

export default Footer;