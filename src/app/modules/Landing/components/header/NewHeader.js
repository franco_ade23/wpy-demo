import React, { Component, Fragment } from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
//import NavDropdown from 'react-bootstrap/NavDropdown';
import {Link} from 'react-router-dom';
import { RUTA_LANDING_AHORRA, RUTA_LANDING_RETIROS, RUTA_LANDING_NOSOTROS, RUTA_LANDING_HOME, RUTA_LOGIN,RUTA_REGISTRO } from '../../../../constants/routes.constants';
import Logo from '../../../../components/Logo';

import '../header/header.css';


export default class NewHeader extends Component {
    state = {
        isTop: true,
        clicked:true
      };
      
      componentDidMount() {
        document.addEventListener('scroll', () => {
          const isTop = window.scrollY < 50;
          if (isTop !== this.state.isTop) {
              this.setState({ isTop })
          }
        });
      }
    render() {
        
        return (
            <Fragment>
                <div className={this.state.isTop ? 'principal' : 'principal-2'} >
                    <div className="nav-trans">
                        
                    <Navbar collapseOnSelect expand="lg"  variant="dark">
                        <Navbar.Brand>
                            <Link to={RUTA_LANDING_HOME}>
                                <Logo variant="white"></Logo>
                            </Link>
                        </Navbar.Brand>
                        <Navbar.Toggle className="show" aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav" >
                            <Nav className="mr-auto">
                                <a href={RUTA_LANDING_AHORRA} className="nav-link">Ahorra</a>
                                <a href={RUTA_LANDING_RETIROS} className="nav-link">Retiros</a>
                                <a href={RUTA_LANDING_NOSOTROS} className="nav-link">Nosotros</a>
                                
                            </Nav>
                            <Nav>
                            {/* <Nav.Link href="#deets">Inicio</Nav.Link> */}
                            <Link to={RUTA_LOGIN} className="button-custom">Iniciar sesión</Link>
                            <Link to={RUTA_REGISTRO} className="button-custom-2">Registrate</Link>
{/* 
                            <Nav.Link eventKey={2} href="#memes">
                                Dank memes
                            </Nav.Link> */}
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                    </div>
                </div>
                
            </Fragment>
        )
    }
}
