import React, { useEffect } from 'react';
import styled from 'styled-components';
import { NavLink as Link } from 'react-router-dom';

import Logo from '../../../../components/Logo';
import { colors } from '../../../../../styles/js/1-settings/colors';
import { RUTA_LANDING_AHORRA, RUTA_LANDING_RETIROS, RUTA_LANDING_NOSOTROS, RUTA_LANDING_HOME, RUTA_LOGIN } from '../../../../constants/routes.constants';
import '../header/header.css';

const StyledLink = styled(Link)`
    color: #FFF;
    &.active{
        color: #00f6d5;
    }
`;

const StyledHeader = styled.div`
    .header__push{
        height: 100px;
    }
    .header__bar{
        height: 100px;
        left: 0;
        position: fixed;
        width: 100%;
        transition: all 0.2s linear;
        top: 0;
        z-index: 1000;
        ${Logo} {
            width: 125px;
        }
        &__content{
            margin: auto;
            max-width: 980px;
            padding: 30px 20px;
            width: 100%;
        }
        button{
            padding: 10px 15px;
            border-radius: 10px;
            cursor: pointer;
            min-width: 110px;
        }
        .btnSignin{
            border: 1px solid #FFF;
            background-color: transparent;
            color: #FFF;
        }
        .btnSignup{
            border: 1px solid #FFF;
            background-color: #FFF;
            color: ${colors.primaryColor}
        }
        nav{
            display: flex;
            justify-content: center;
            ul{
                display: flex;
                align-items: center;
                li{
                    min-width: 85px;
                    text-align: center;
                    ${StyledLink}{
                    }
                }
            }
        }
        &.fixed{
            background-color: rgba(0,0,0,.4);
        }
    }
`;

const Header = ({ push }) => {
    
    const bodyScrollListener = () => {
        const doc = document.documentElement;
        const scrollTop = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
        if( scrollTop > 5 ){
            document.getElementById('headerContent').classList.add('fixed');
        }else{
            document.getElementById('headerContent').classList.remove('fixed');
        }
    }

    const addBodyScrollWatcher = () => {
        window.addEventListener('scroll',bodyScrollListener);
    }

    const removeBodyScrollWatcher = () => {
        window.removeEventListener('scroll',bodyScrollListener);
    }

    useEffect(()=>{
        addBodyScrollWatcher();
        return removeBodyScrollWatcher;
    },[]);

    return (
        <StyledHeader>
            
             { push && <div className="header__push"></div> }
                <div className="header__bar" id="headerContent">
                    <div className="header__bar__content flex-row-layout ai-center">
                        <Link to={RUTA_LANDING_HOME}>
                            <Logo variant="white"></Logo>
                        </Link>
                        <nav className="flex">
                        
                            <ul>
                                <li>
                                    <StyledLink to={RUTA_LANDING_AHORRA} activeClassName="active">
                                        Ahorra
                                    </StyledLink>
                                </li>
                                <li>
                                    <StyledLink to={RUTA_LANDING_RETIROS} activeClassName="active">
                                        Retiros
                                    </StyledLink>
                                </li>
                                <li>
                                    <StyledLink to={RUTA_LANDING_NOSOTROS} activeClassName="active">
                                        Nosotros
                                    </StyledLink>
                                </li>
                            </ul>
                        </nav>
                        <Link to={RUTA_LOGIN}>
                            <button className="btnSignin">
                                Iniciar sesión
                            </button>
                        </Link>
                        <div className="spacer-h-20"></div>
                        <Link to={RUTA_LOGIN}>
                            <button className="btnSignup">
                                Mi cuenta
                            </button>
                        </Link>
                    </div>
                </div>
                
        </StyledHeader>
    );
}

export default Header;

