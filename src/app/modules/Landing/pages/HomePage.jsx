import React, { useState } from 'react';
import styled from 'styled-components';
// import CountUp from 'react-countup';
// import Fade from 'react-reveal/Fade';
import YouTube from 'react-youtube';
// import { colors } from '../../../../styles/js/1-settings/colors';
import SaveMoneyCarrousel from './carrousel/SaveMoneyCarrousel';
import Comment from './carrousel/chat.png'
import Aliados from './carrousel/marcas.png'
import {Link} from 'react-router-dom'
import { RUTA_LANDING_AHORRA, RUTA_LANDING_RETIROS  } from '../../../constants/routes.constants';
//RUTA_LANDING_NOSOTROS, RUTA_LANDING_HOME, RUTA_LOGIN


// const StyledCountUp = styled(CountUp)`
//     font-size: 2em;
// `;

const StyledHomePage = styled.div`
    .section{
        overflow: hidden;
        position: relative;
        &__content{
            align-items: center;
            display: flex;
            height: 100vh;
            margin: auto;
            padding: 30px;
            max-width: 1050px;
            width: 100%;
            &--fitHeight{
                height: auto;
                padding-top: 80px;
                padding-bottom: 80px;
            }
            &--fullWidth{
                max-width: none;
            }
            &--vertical{
                flex-direction: column
            }
        }
        &__left, &__right{
            flex: 0 0 50%;
        }
        &__goToNext{
            //background-image: url('/images/landing/arrow-down.png');
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
            bottom: 40px;
            cursor: pointer;
            height: 40px;
            width: 50px;
            left: 50%;
            position: absolute;
            transform: translateX(-50%);
        }
        &__calculator{
            background-image: url('/images/landing/calculator-bg.png');
            background-position: center;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            color: #FFF;
            padding: 40px 60px;
            position: absolute;
            right: -30px;
            top: 50%;
            width: 50%;
            transform: translateY(-50%);
            button{
                background-color: #FFF;
                border-radius: 8px;
                color: #0074ff;
            }
        }
        &--presentation{
            background-image: url('/images/landing/home-presentation-bg.png');
            background-position: center;
            background-size: cover;
            font-size: 18px;
            height: auto;
            min-height: 100vh;
            // padding-top: 100px;
            // padding-bottom: 100px;
        }
        &--form{
            background-image: url('/images/landing/form-bg.png');
            background-position: center;
            background-size: cover;
            color: #FFF;
            input{
                background-color: transparent;
                border: 0;
                border-bottom: 3px solid #FFF;
                border-radius: 0px !important;
                color: #FFF;
                ::placeholder{
                    color: #FFF;
                }
            }
            button{
                color: #0074ff;
                background-color: #FFF;
                border-radius: 8px;
            }
        }
        &--video{
            background-image: url('/images/landing/home/video-bg-and-play.png');
            background-position: center;
            background-size: cover;
            display: flex;
            justify-content: center;
            padding: 80px 0;
            position: relative;
            .video{
                opacity: 0;
                &.visible{
                    opacity: 1;
                }
            }
            .playVideoTrigger{
                background-image: url('/images/landing/home/video-bg-and-play.png');
                background-position: center;
                background-size: cover;
                cursor: pointer;
                height: 100%;
                left: 0;
                position: absolute;
                top: 0;
                width: 100%;
                &.invisible{
                    display: none;
                }            
            }
        }
        &--benefits{
            button{
                background-color: #0047d4;
                border-radius: 12px;
                cursor: pointer;
                padding: 10px;
                margin-top: 30px;
                min-width: 100px;
                img{
                    height: 15px;
                }
            }
        }
    }
    .yourInfoForm{
        background-color: #FFF;
        border-radius: 8px;
        padding: 40px;
        h1 {
            margin: 0;
        }
        input{
            background-color: transparent;
            border: 0;
            border-bottom: 1px solid #545454;
            border-radius: 0px !important;
            ::placeholder{
                color: #545454;
            }
        }
    }
`;

const HomePage = () => {


    const initialState = { showVideo: false, videoPlayer: null };

    const [ state, setState ] = useState(initialState);

    const showVideo = () => {
        setState({
            ...state,
            showVideo: true
        });
        state.videoPlayer.playVideo();
    }

    const onVideoPlayerReady = ( ev ) => {
        setState({
            ...state,
            videoPlayer: ev.target
        });
    }

    const videoOpts = {
        heigth: '720',
        width: '840'
    };

    const videoId = 'DR1ZGELnRBs';

    return (
        <StyledHomePage>
            <div className="float">
            <a className="float" target="_blank" rel="noopener noreferrer" href="https://api.whatsapp.com/send?phone=51924695692&amp;text=Quiero%20más%20información%20sobre%20WePayU"><i className="fa fa-whatsapp my-float intern"></i></a>
            </div>
            <div className="section section--presentation text-white">
                <div className="section__content prueba">
                    <div className="section__left ">
                        <h2>
                            <b>La manera más fácil</b><br/>
                            de obtener dinero y ahorrar<br/>
                            en tus pagos
                        </h2>
                        <br className="space-mobile"/>
                        <br/>
                        <p>
                            Obtén efectivo inmediato de tu tarjeta de crédito<br/>
                            ahorra dinero pagando tu universidad
                        </p>
                    </div>
                    <div className="section__right pl-100-new ">
                        {/* <form className="yourInfoForm">
                            <h1 className="text-bold text-primary mb-20 h1-mobile">
                                Ingresa tus datos
                            </h1>

                            <div className="flex-row-layout ai-center jc-space-between mb-20">
                                <div className="flex-100">
                                    <input type="text" placeholder="Nombre y Apeliido"
                                    className="fill-width"/>
                                </div>
                            </div>
                            <div className="flex-row-layout ai-center jc-space-between mb-20">
                                <div className="flex-100">
                                    <input type="text" placeholder="Correo electrónico"
                                    className="fill-width"/>
                                </div>
                            </div>
                            <div className="flex-row-layout ai-center jc-space-between mb-20">
                                <div className="flex-100">
                                    <input type="number" placeholder="Teléfono Móvil"
                                    className="fill-width"/>
                                </div>
                            </div>
                            <div className="flex-row-layout ai-center jc-space-between mb-20">
                                <div className="flex-100">
                                    <input type="text" placeholder="Dirección"
                                    className="fill-width"/>
                                </div>
                            </div>
                            <div className="flex-row-layout ai-center jc-space-between mb-40">
                                <div className="flex-100">
                                    <input type="text" placeholder="Selecciona documento"
                                    className="fill-width"/>
                                </div>
                            </div>
                            <div className="text-center">
                                <button className="wpy-btn wpy-btn--primary">
                                    Regístrate
                                </button>
                            </div>
                        </form> */}
                        <div className="home-flex">
                            <div className="col-md-6">
                                <div className="quare-home">
                                    <Link to={RUTA_LANDING_AHORRA}>
                                        <img src="/images/landing/home/benefits-1.png" alt="quiero ahorrar" height="60"/>
                                        <p>Quiero Ahorrar</p>
                                    </Link>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="quare-home">
                                    <Link to={RUTA_LANDING_RETIROS}>
                                        <img src="/images/landing/home/benefits-2.png" alt="necesito efectivo" height="60"/>
                                        <p >Necesito Efectivo</p>
                                    </Link>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div className="section__goToNext icon-bounce">
                    <a href="#container-form">
                        <i className="fa fa-arrow-down bounce" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div className="section">
                <div className="section__content section__content--fitHeight">
                    <div className="fill-width">
                        <h2 className="text-bold text-black">
                            ¿Cómo funciona?
                        </h2>
                        <div className="spacer-20"></div>
                        <p>
                            Actuamos como intermediarios, Gestionamos,Verificamos<br/>
                            y aseguramos la transacción
                        </p>
                        <div className="spacer-50"></div>
                        <ul className="flex-row-layout jc-space-between text-center">
                            <li className="flex-30">
                                <img src="/images/landing/home/how-works-student.png" alt="estudiante" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Estudiante</b>
                                </h4>
                                <p>
                                    Recibe un descuento en el pago<br/>
                                    de su universidad por enviar<br/>
                                    el dinero a WePayU
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/home/how-works-wepayu.png" alt="gestiona, verifica" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>WePayU</b>
                                </h4>
                                <p>
                                    Gestiona, verifica y asegura<br/>
                                    la transacción
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/home/how-works-money.png" alt="solicitante" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Solicitante</b>
                                </h4>
                                <p>
                                    Recibe el dinero menos una<br/>
                                    comisión al retirar dinero con<br/>
                                    tarjeta de crédito
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="section section--video">
                <div className={ `video ${ state.showVideo ? 'visible': '' }` }>
                    <YouTube
                        videoId={videoId}
                        opts={videoOpts}
                        onReady={onVideoPlayerReady}
                    />
                </div>
                <div className={ `playVideoTrigger ${ state.showVideo ? 'invisible' : '' }` }
                onClick={showVideo}></div>
            </div>
            <div className="section section--benefits">
                <div className="section__content section__content--fitHeight">
                    <div className="fill-width text-center">
                        <h2 className="text-bold text-black">
                            Nuestros beneficios
                        </h2>
                        <div className="spacer-20"></div>
                        <p>
                            Obtén beneficios únicos al empezar a usar nuestros beneficios
                        </p>
                        <div className="spacer-50"></div>
                        <ul className="flex-row-layout jc-space-between">
                            <li className="flex-50 mb-40-form">
                                <img src="/images/landing/home/benefits-1.png" alt="gana dinero" height="60"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10 text-bold">
                                    Gana dinero por pagar tu<br/>
                                    universidad
                                </h4>
                                <p>
                                    Pagando con WePayU ganas un 2% de<br/>
                                    descuento en tu universidad<br/>
                                    *Si tu boleta es de S/2,000 ahorrarías S/40
                                </p>
                                <Link to={RUTA_LANDING_AHORRA}>
                                    <button>
                                        <img src="/images/landing/home/right-arrows.png" alt="flechas"/>
                                    </button>
                                </Link>
                            </li>
                            <li className="flex-50">
                                <img src="/images/landing/home/benefits-2.png" alt="obten efectivo" height="60"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10 text-bold">
                                    Obtén efectivo a una<br/>
                                    comisión justa
                                </h4>
                                <p>
                                    Con WePayU podrás retirar efectivo de<br/>
                                    tu tarjeta de crédito inmediatamente y<br/>
                                    tan solo al 10% de comisión
                                </p>
                                <Link to={RUTA_LANDING_RETIROS}>
                                    <button>
                                        <img src="/images/landing/home/right-arrows.png" alt="flechas 2"/>
                                    </button>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="section-container-home">
                <div className="col-md-6 space-col">
                    <h2 className="text-bold text-black">Nuestros Usuarios</h2>
                    <p>Nuestra reputacion es muy importante en nuestro
                        crecimiento como plataforma, asi que mira aquí
                        lo que opinan de nosotros</p>
                </div>
                <div className="container-position col-md-6">
                    <SaveMoneyCarrousel></SaveMoneyCarrousel>
                    <div className="back-comment">
                        <img src={Comment} alt='comentario' height="150px" ></img>
                    </div>
                </div>
            </div>
            <div className="section-container-home">
                <div className="row flex-column-reverse flex-md-row">
                    <div className="col-md-6 order-md-last"> 
                        <img src={Aliados} width="100%" alt='aliados'></img>
                    </div>
                    <div className="col-md-6 name-coment order-md-first">
                        <h2 className="text-bold text-black">Nos Respaldan</h2>
                        <p className="mt-20">A lo largo de nuestro camino, muchas marcas
                            han reconocido nuestro trabajo y ese respaldo brindado
                            refleja en la calidad de servicio de damos</p>
                    </div>
                </div>
            </div>
        </StyledHomePage>
    );
}

export default HomePage;
