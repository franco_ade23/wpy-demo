import React from 'react';
import styled from 'styled-components';
// import CountUp from 'react-countup';
// import Fade from 'react-reveal/Fade';
import '../../Landing/pages/style.css'

// const StyledCountUp = styled(CountUp)`
//     font-size: 2em;
// `;


const StyledWithdrawPage = styled.div`
    .section{
        overflow: hidden;
        position: relative;
        &__content{
            align-items: center;
            display: flex;
            height: 60vh;
            margin: auto;
            padding: 30px;
            max-width: 900px;
            width: 100%;
            &--fitHeight{
                height: auto;
                padding-top: 80px;
                padding-bottom: 80px;
            }
            &--fullWidth{
                max-width: none;
            }
            &--vertical{
                flex-direction: column
            }
        }
        &__left, &__right{
            flex: 0 0 50%;
        }
        &__goToNext{
            background-image: url('/images/landing/arrow-down.png');
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
            bottom: 40px;
            cursor: pointer;
            height: 40px;
            width: 50px;
            left: 50%;
            position: absolute;
            transform: translateX(-50%);
        }
        &__calculator{
            background-image: url('/images/landing/calculator-bg.png');
            background-position: center;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            color: #FFF;
            padding: 40px 60px;
            position: absolute;
            right: -30px;
            top: 50%;
            width: 50%;
            transform: translateY(-50%);
            button{
                background-color: #FFF;
                border-radius: 8px;
                color: #0074ff;
            }
        }
        &--presentation{
            background-image: url('/images/landing/fondo-us.png');
            background-position: center;
            background-size: cover;
            font-size: 18px;
        }
        &--form{
            background-image: url('/images/landing/form-bg.png');
            background-position: center;
            background-size: cover;
            color: #FFF;
            input{
                background-color: transparent;
                border: 0;
                border-bottom: 3px solid #FFF;
                border-radius: 0px !important;
                color: #FFF;
                ::placeholder{
                    color: #FFF;
                }
            }
            button{
                color: #0074ff;
                background-color: #FFF;
                border-radius: 8px;
            }
        }
    }
`;

const UsPage = () => {

    return (
        <StyledWithdrawPage>
            <div className="float"><i className="fa fa-whatsapp my-float intern"></i></div>
            <div className="section section--presentation text-white">
                <div className="section__content">
                    <div className="section__left">
                        <h2>
                            <b>Somos la StartUp</b><br/>
                            que te ayuda a ganar dinero
                        </h2>
                        <br/>
                        <br/>
                        {/* <p>
                            Con WePayU podrás obtener un préstamo de tu tarjeta<br/>
                            de crédito a tan solo un 10% de comisión.
                        </p> */}
                    </div>
                </div>
                {/* <div className="section__goToNext"></div> */}
            </div>
            <div className="section section-center">
                <div className="section__content section__content--fitHeight">
                    <div>
                        <h2 className="text-bold text-black">
                            ¿Cómo funciona?
                        </h2>
                        <div className="spacer-20"></div>
                        {/* <p className="prueba">
                            Nuestro proceso es fácil y sencillo
                        </p> */}
                        {/* <div className="spacer-50"></div> */}
                        <ul className="flex-row-layout jc-space-between text-center">
                            <li className="flex-0 mb-4">
                                {/* <img src="/images/landing/contract.png" alt="" height="100"/> */}
                                {/* <div className="spacer-25"></div> */}
                                {/* <h4 className="mb-10">
                                    <b>Ingresa tus datos</b>
                                </h4> */}
                                <p className="us-content">
                                Fundada en Julio del 2017 y miembro de la Asociación Fintech Perú. WePayU
                                te brinda de manera rápida y confiable la opción de ahorrar al pagar tus servicios
                                y boletas de universidad con descuentos en efectivo de hasta S/60.00.
                                También te brindamos la opción de poder obtener efectivo al instante de tu
                                tarjeta de crédito, a cambio de una mínima comisión.
                                </p>
                            </li>
                            <li className="flex-30 ">
                                <img src="/images/landing/home/how-works-wepayu.png" alt="how works" height="100"/>
                                {/* <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Envía la orden</b>
                                </h4>
                                <p>
                                    Paga la boleta de univesitario
                                    y envíanos el comprobante de pago
                                </p> */}
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            {/* <div className="section">
                <div className="section__content section__content--fitHeight">
                    <div>
                        <h2 className="text-bold text-black">
                            ¿Cómo funciona?
                        </h2>
                        <div className="spacer-20"></div>
                        <p>
                            Nuestro proceso es fácil y sencillo
                        </p>
                        <div className="spacer-50"></div>
                        <ul className="flex-row-layout jc-space-between text-center">
                            <li className="flex-30">
                                <img src="/images/landing/contract.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Diego Revilla</b>
                                </h4>
                                <p>
                                    CEO & Co-Founder
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/cost.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Richard Cancino</b>
                                </h4>
                                <p>
                                    Director de Tecnología
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/pay.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Paola Salvador</b>
                                </h4>
                                <p>
                                    CMO & Co-Founder
                                </p>
                            </li>
                        </ul>
                        <div className="spacer-50"></div>
                        <ul className="flex-row-layout jc-space-between text-center">
                            <li className="flex-30">
                                <img src="/images/landing/contract.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Jose Guelac</b>
                                </h4>
                                <p>
                                    COO & Ejecutivo
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/cost.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Aldaire Yngaruca</b>
                                </h4>
                                <p>
                                    Front-end developer
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/pay.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>RDiego Reviella</b>
                                </h4>
                                <p>
                                    Te enviaremos inmediatamente
                                    el dinero a tu cuenta bancaria

                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> */}
            <div className="container">
                <h1>Equipo</h1>

                <div className="row">
                    <div className="column">
                        <img src="/images/landing/team/diego.png" alt="ceo" height="100"/>
                        <p className="team-title">Diego Revilla</p>
                        <p className="team-subtitle">Director General</p>
                    </div>
                    <div className="column" >
                        <img src="/images/landing/team/richard_profile.jpg" alt="director tech" height="100"/>
                        <p className="team-title">Richard Cancino</p>
                        <p className="team-subtitle">Director de Tecnología</p>
                    </div>
                    <div className="column" >
                        <img src="/images/landing/team/Paola.jpg" alt="cmo" height="100"/>
                        <p className="team-title">Paola Salvador</p>
                        <p className="team-subtitle">Directora de Marketing</p>
                    </div>


                    <div className="column">
                        <img src="/images/landing/team/jose.jpeg" alt="coo" height="100"/>
                        <p className="team-title">Jose Guelac</p>
                        <p className="team-subtitle">Director de Operaciones</p>
                    </div>
                    <div className="column" >
                        <img src="/images/landing/team/aldaire_2.jpeg" alt="frontend" height="100"/>
                        <p className="team-title">Aldaire Yngaruca</p>
                        <p className="team-subtitle">Front-end developer</p>
                    </div>
                    <div className="column" >
                        <img src="/images/landing/team/john.png" alt="ejecutivo" height="100"/>
                        <p className="team-title">John Lagos</p>
                        <p className="team-subtitle">Ejecutivo</p>
                    </div>

                    </div>
            </div>
            <div className="container">
                <div className="social-media">
                    <p>Si quieres conocerun poco más siguenos en:</p>
                    <img src="/images/landing/home/fb.png" alt='facebook'/>
                    <img src="/images/landing/home/instagram.png" alt='instagram'/>
                </div>

            </div>
        </StyledWithdrawPage>
    );
}

export default UsPage;
