import React, { Component } from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import '../carrousel/customCarrousel.css'

export default class WithDrawCarrousel extends Component {
    componentWillMount() {

        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <Carousel showArrows={false} showThumbs={false} autoPlay={true} swipeable={false} showStatus={false} infiniteLoop={true}>
                <div className="back">
                    {/* <img src="https://www.tecnologiadetuatu.elcorteingles.es/wp-content/uploads/2007/11/c%C3%B3mo-despixelar-una-imagen.jpg" /> */}
                    {/* <p className="legend">
                        <p>hola</p>
                        <p>hola</p>
                    </p> */}
                    <p>Muy confiable y excelente servicio!!</p>
                    <div className="footer-comment">
                        <div className="name-coment">
                            <h5 className="text-bold text-black">Ana Paula Roque </h5>
                            {/* <p>UPC</p> */}
                        </div>
                    </div>
                    <div className="new-img">
                        <img  className="img-coment" alt='imagen profile' src="https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/40523056_2075799542470853_28768442567884800_n.jpg?_nc_cat=100&_nc_oc=AQkQ1MYEeac82EA_z3PgJwvWTrVTnPq46tYnHycgt_agvuBcH5j7L0m85d6S2Tzp4cc&_nc_ht=scontent.flim15-2.fna&oh=669045e507ec1105e400accb6758ec1e&oe=5E1E7595" />

                    </div>
                   
                </div>
                <div className="back">
                    {/* <img src="https://www.tecnologiadetuatu.elcorteingles.es/wp-content/uploads/2007/11/c%C3%B3mo-despixelar-una-imagen.jpg" /> */}
                    {/* <p className="legend">Legend 2</p> */}
                    <p>Un servicio único, al comienzo tuve dudas, pero al final todo fue como me indicaron. Totalmente recomendado!</p>
                    <div className="footer-comment">
                        <div className="name-coment">
                            <h5 className="text-bold text-black">Jorge Castillo </h5>
                            {/* <p>UPC</p> */}
                        </div>
                    </div>
                    <div className="new-img">
                        <img  className="img-coment" alt='imagen profile' src="https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-1/c0.27.160.160a/p160x160/18921959_10155443490956908_6357216184688851692_n.jpg?_nc_cat=100&_nc_oc=AQnEH-QLvtImWSw_niT2d14djgzR8VkBvDqea6q8nklipIZ4DsB1yoyXiu6Qa2Ajsb8&_nc_ht=scontent.flim15-2.fna&oh=355fd71eb0296f6ec3ff3c708c14c38a&oe=5E5EE0F6" />

                    </div>

                </div>
                <div className="back">
                    {/* <img src="https://www.tecnologiadetuatu.elcorteingles.es/wp-content/uploads/2007/11/c%C3%B3mo-despixelar-una-imagen.jpg" /> */}
                    {/* <p className="legend">Legend 3</p> */}
                    <p>Súper confiables y muy amables!! Excelente servicio</p>
                    
                    <div className="footer-comment">
                        <div className="name-coment">
                            <h5 className="text-bold text-black">Flavia Gutiérrez </h5>
                            {/* <p>UPC</p> */}
                        </div>
                    </div>
                    <div className="new-img">
                        <img  className="img-coment" alt='imagen profile' src="https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-1/c0.0.160.160a/p160x160/49210820_1942825572452627_6051459901878697984_n.jpg?_nc_cat=108&_nc_oc=AQn-Gy0CBdrKlrydaOHjXdCG88LS6qdJPKCC6OhqMzNfoXgVMfmXEOUpya-kxAwXemU&_nc_ht=scontent.flim15-2.fna&oh=9bd5314b45fa183ce08a5bac39d9077d&oe=5E5F0665" />

                    </div>
                    
                </div>
            </Carousel>
            </div>
        )
    }
}
