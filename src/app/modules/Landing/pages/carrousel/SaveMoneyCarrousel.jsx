import React, { Component } from 'react'
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import '../carrousel/customCarrousel.css'

export default class SaveMoneyCarrousel extends Component {
    componentWillMount() {

        window.scrollTo(0, 0);
    }

    render() {
        return (
            <div>
                <Carousel showArrows={false} showThumbs={false} autoPlay={true} swipeable={false} showStatus={false} infiniteLoop={true}>
                <div className="back">
                    {/* <img src="https://www.tecnologiadetuatu.elcorteingles.es/wp-content/uploads/2007/11/c%C3%B3mo-despixelar-una-imagen.jpg" /> */}
                    {/* <p className="legend">
                        <p>hola</p>
                        <p>hola</p>
                    </p> */}
                    <p>Recomendado 100%. Son confiables y seguros, cumplieron con rapidez</p>
                    <div className="footer-comment">
                        <div className="name-coment">
                            <h5 className="text-bold text-black">Joel Luque</h5>
                            <p>UPC</p>
                        </div>
                    </div>
                    <div className="new-img">
                        <img  className="img-coment" alt='imagen profile' src="https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-1/c1.0.160.160a/p160x160/24294136_10211467845793852_6638435212995335668_n.jpg?_nc_cat=100&_nc_oc=AQltS8ItbMk1k846_Qmd2-vQp8i2BSYXOSzPcsgU5K8GJ6LMCf_LgNingOkhb8yk0aM&_nc_ht=scontent.flim15-2.fna&oh=f15568dc4a3e73cd9dfc25a953fd7b9e&oe=5E21E25F" />

                    </div>
                   
                </div>
                <div className="back">
                    {/* <img src="https://www.tecnologiadetuatu.elcorteingles.es/wp-content/uploads/2007/11/c%C3%B3mo-despixelar-una-imagen.jpg" /> */}
                    {/* <p className="legend">Legend 2</p> */}
                    <p>Buena atención, rápido y seguro.</p>
                    <div className="footer-comment">
                        <div className="name-coment">
                            <h5 className="text-bold text-black">John Tarazona</h5>
                            <p>UPC</p>
                        </div>
                    </div>
                    <div className="new-img">
                        <img  className="img-coment" alt='imagen profile' src="https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-1/p160x160/26992737_148409769210519_1298072266750427229_n.jpg?_nc_cat=111&_nc_oc=AQnQUvcsCNWv1dsLes30YlQlHapGdrO2Ux6079G5VHmgykY-o8yIwg9aFC9gb_2rJVM&_nc_ht=scontent.flim15-2.fna&oh=786f2684ca335b620b0aaeadf1051112&oe=5E1A6114" />

                    </div>

                </div>
                <div className="back">
                    {/* <img src="https://www.tecnologiadetuatu.elcorteingles.es/wp-content/uploads/2007/11/c%C3%B3mo-despixelar-una-imagen.jpg" /> */}
                    {/* <p className="legend">Legend 3</p> */}
                    <p>Los encontré en Google y me pareció una alternativa confiable, acabo de hacer una transacción con ellos, súper confiables y el pago fue inmediato.
Recomendados</p>
                    
                    <div className="footer-comment">
                        <div className="name-coment">
                            <h5 className="text-bold text-black">Edward CH</h5>
                            {/* <p>UPC</p> */}
                        </div>
                    </div>
                    <div className="new-img">
                        <img  className="img-coment" alt='imagen profile' src="https://scontent.flim15-2.fna.fbcdn.net/v/t1.0-1/p160x160/34906668_10216020192993669_9047865491158204416_n.jpg?_nc_cat=108&_nc_oc=AQmyNrk9YABY-cvQWO2D724jsBiuXIr5liDAaoy4MVYgifdCdd9wpdiwwG-tlqwUtZc&_nc_ht=scontent.flim15-2.fna&oh=8d7bd62e5b829d4ff061173f4f129a63&oe=5E579A08" />

                    </div>
                    
                </div>
            </Carousel>
            </div>
        )
    }
}
