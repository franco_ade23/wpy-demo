import React from 'react';
import styled from 'styled-components';
import CountUp from 'react-countup';
import Fade from 'react-reveal/Fade';
import './style.css'
import { Component } from 'react'
import Validator from 'validator'
import axios from 'axios'
import jQuery from 'jquery'
const StyledCountUp = styled(CountUp)`
    font-size: 2em;
`;

const StyledWithdrawPage = styled.div`
    .section{
        overflow: hidden;
        position: relative;
        &__content{
            align-items: center;
            display: flex;
            height: 100vh;
            margin: auto;
            padding: 30px;
            max-width: 1050px;
            width: 100%;
            &--fitHeight{
                height: auto;
                padding-top: 80px;
                padding-bottom: 80px;
            }
            &--fullWidth{
                max-width: none;
            }
            &--vertical{
                flex-direction: column
            }
        }
        &__left, &__right{
            flex: 0 0 50%;
        }
        &__goToNext{
            //background-image: url('/images/landing/arrow-down.png');
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
            bottom: 40px;
            cursor: pointer;
            height: 40px;
            width: 50px;
            left: 50%;
            position: absolute;
            transform: translateX(-50%);
        }
        &__calculator{
            background-image: url('/images/landing/calculator-bg.png');
            background-position: center;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            color: #FFF;
            padding: 40px 60px;
            position: absolute;
            right: -30px;
            top: 50%;
            width: 50%;
            transform: translateY(-50%);
            button{
                background-color: #FFF;
                border-radius: 8px;
                color: #0074ff;
            }
        }
        &--presentation{
            background-image: url('/images/landing/fondo-ahorra.png');
            background-position: center;
            background-size: cover;
            font-size: 18px;
        }
        &--form{
            background-image: url('/images/landing/form-bg.png');
            background-position: center;
            background-size: cover;
            color: #FFF;
            input{
                background-color: transparent;
                border: 0;
                border-bottom: 3px solid #FFF;
                border-radius: 0px !important;
                color: #FFF;
                ::placeholder{
                    color: #FFF;
                }
            }
            button{
                color: #0074ff;
                background-color: #FFF;
                border-radius: 8px;
            }
            
        }
    }
    .yourInfoForm{
        background-color: #FFF;
        border-radius: 8px;
        padding: 30px 40px;
        font-size:14px;
        width:90%;
        h2 {
            margin: 0;
        }
        input{
            height:40px;
            background-color: transparent;
            border: 0;
            border-bottom: 1px solid #545454;
            border-radius: 0px !important;
            ::placeholder{
                color: #545454;
            }
        }
    }
    .feedback-form{
        color:red;
        font-size:12px;
    }
    .comision-text{
        font-size: 13px;
        color: gray;
        font-size:bold;
    }
    .text-center{
        text-align:center
    }
`;



 class Montos extends Component {
    constructor(props){
        super(props);
        this.state = {
            monto: '',
        }
        this.descuento = React.createRef();
        this.deposito = React.createRef();

    }
    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        }, ()=>{
            this.validateNumber()
        })
    }
    validateNumber = () => {
        const PORCENTAJE_DESCUENTO = 2
        const {monto} = this.state;
        if(monto > 10){
            const calc = (monto * PORCENTAJE_DESCUENTO / 100).toFixed(1)
            this.descuento.current.innerText = calc + '0'
            this.deposito.current.innerText = (monto - calc).toFixed(1) + '0'
        }else{
            this.descuento.current.innerText = '0.00'
            this.deposito.current.innerText = '0.00'
        }
    }
    componentWillMount() {

        window.scrollTo(0, 0);
    }

    render() {
        const {monto} = this.state;
        return (
            <div className="col-md-6 mb-4 order">
                <div className="calculator-content">
                    <h4 className="mb-25">Monto a Pagar</h4>
                    <input type="number" className="mb-25 input-savemoney" name="monto" id="amount" required value={monto} onChange={this.onChange} ></input>
                    <p >Descuento con WePayU (2%)</p>
                    <h1 id="discount"> S/. <span className="descuento-c" ref={this.descuento}>0.00</span></h1>
                    <p>Monto a despositar</p>
                    <h1  id="deposit">s/. <span className="deposito-c" ref={this.deposito}>0.00</span> </h1>
                    <a href="#container-form"className="button-save">Empieza ya</a>
                </div>
            </div>
        )
    }
}

const SaveMoneyPage = () => {

    return (
        <StyledWithdrawPage>
            <div className="float">
                <a className="float" target="_blank" rel="noopener noreferrer" href="https://api.whatsapp.com/send?phone=51946022477&amp;text=Quiero%20saber%20como%20pagar%20mi%20Universidad%20con%20WePayU"> <i className="fa fa-whatsapp my-float intern"></i></a>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
            <div className="section section--presentation text-white">
                <div className="section__content banner-responsive">
                    <div className="section__left">
                        <h2>
                            <b>Ahorra dinero</b><br/>
                            pagando tu universidad
                        </h2>

                        <br/>
                        <br/>
                        <p>
                            Con WePayU podrás ahorrar hasta 60 soles en el pago de tu Universidad.<br/>
                            Pagando con nosotros siempre ganas un 2% de descuento en tu boleta.
                        </p>
                    </div>
                    <div className="section__right pl-100 space-mobile">
                        <FormSaveMoney/>
                    </div>
                </div>
                <div className="section__goToNext icon-bounce">
                    <a href="#container-form">
                        <i className="fa fa-arrow-down bounce" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            {/* <div className="section">
                <div className="section__content">
                    <div className="section__left pr-30">
                        <h2 className="text-black">
                            <b>
                                Ahorra tiempo y dinero <br/>
                                con nosotros
                            </b>
                        </h2>
                        <p>
                            Puedes tener total seguridad de realizar tus pagos
                        </p>
                        <br/>
                        <ul>
                            <li>
                                <div className="flex-row-layout ai-center">
                                    <img src="/images/landing/wallet.png" alt="" height="50"
                                    className="mr-20"/>
                                    <div>
                                        <h4 className="text-bold text-black mb-10">
                                            Ahorra tiempo
                                        </h4>
                                        <p>
                                            Al realizar el pago de tu universidad
                                            se te cobrará un interés de cuotas.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="spacer-50"></div>
                            </li>
                            <li>
                                <div className="flex-row-layout ai-center">
                                    <img src="/images/landing/money.png" alt="" height="50"
                                    className="mr-20"/>
                                    <div>
                                        <h4 className="text-bold text-black mb-10">
                                            Seguro y confiable
                                        </h4>
                                        <p>
                                            El único requisito es tarjeta de crédito con saldo disponible.
                                            Nuestros servicios funcionan con varios tipos de tarjetas y
                                            bancos en el mercado
                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="section__calculator">
                        <h4 className="text-bold mb-25">
                            Monto a pagar<br/>

                        </h4>
                        <input type="number" className="mb-25"/>
                        <div className="mb-25">
                            <p>Descuento con WePayU (2%)</p>
                            <h1>S/. 0.00</h1>
                        </div>
                        <div className="mb-25">
                            <p>Monto a depositar</p>
                            <h1>S/. 0.00</h1>
                        </div>
                        <button>
                            Empieza ya
                        </button>
                    </div>
                </div>
            </div> */}
            <div className="section-container section-savemoney">
                <Montos/>
                <div className="col-md-6 mb-25">
                    <div className="text-container">
                        <h2>Ahorra Tiempo y dinero<br/>con nosotros</h2>
                        <p className="mb-25">
                            Puedes tener total seguridad de realizar tus retiros
                        </p>
                        <div className="flex-row-layout ai-center mb-25 home-img">
                            <img src="/images/landing/wallet.png" alt="" height="50"
                            className="mr-20 "/>
                            <div>
                                <h4 className="text-bold text-black mb-10">
                                    Ahorra tiempo
                                </h4>
                                <p>
                                    Todas las transacciones se realizan a través de cuentas bancarias.
                                </p>
                            </div>
                        </div>
                        <div className="flex-row-layout ai-center home-img">
                            <img src="/images/landing/money.png" alt="" height="50"
                            className="mr-20"/>
                            <div>
                                <h4 className="text-bold text-black mb-10">
                                    Seguro y confiable
                                </h4>
                                <p>
                                Cambia tu saldo para compras con tu tarjeta en dinero en efectivo.
                                Te depositamos inmediatamente el dinero en tu cuenta de ahorros.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            
            <div className="section section-center  ">
                <div className="section__content section__content--fitHeight">
                    <div>
                        <h2 className="text-bold text-black">
                            ¿Cómo funciona?
                        </h2>
                        <div className="spacer-20"></div>
                        <p>
                            Nuestro proceso es fácil y sencillo
                        </p>
                        <div className="spacer-50"></div>
                        <ul className="flex-row-layout jc-space-between text-center">
                            <li className="flex-30">
                                <img src="/images/landing/contract.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Ingresa tus datos</b>
                                </h4>
                                <p>
                                    Regístrate ingresando los datos
                                    requeridos en el formulario y te
                                    contactaremos para ayudarle
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/cost.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Haz la transferencia</b>
                                </h4>
                                <p>
                                    Transfiere o te depositamos el monto
                                    a pagar de tu boleta descontando
                                    el 2%. Y envíanos la foto de la constancia.
                                </p>
                            </li>
                            <li className="flex-30">
                                <img src="/images/landing/pay.png" alt="" height="100"/>
                                <div className="spacer-25"></div>
                                <h4 className="mb-10">
                                    <b>Hacemos tu pago</b>
                                </h4>
                                <p>
                                    Haremos el pago de tu boleta
                                    y te enviaremos la constancia
                                    de pago emitida. ¡Y LISTO!
                                    ¡YA GANASTE!
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="section section--form text-white" id="container-form">
                <div className="section__content section__content--fitHeight">
                   <SaveMoneyForm></SaveMoneyForm>
                </div>
            </div>
            <Fade>
                <div className="section ">
                    <div className="section__content section__content--fullWidth section__content--vertical
                    section__content--fitHeight">
                        <div className="section__content section__content--fitHeight no-padding">
                            <div className="flex-row-layout ai-center ai-center jc-space-between text-center
                            fill-width">
                                <div className="flex-33 text-center">
                                    <h1 className="text-primary flex-row-layout ai-center jc-center
                                    no-margin d-inline">
                                        <span className="no-margin text-2x">+</span>
                                        <StyledCountUp delay={2} start={0} end={500} duration={8}></StyledCountUp>
                                    </h1>
                                    <p>Usuarios WePayU</p>
                                </div>
                                <div className="flex-33 text-center">
                                    <h1 className="text-primary flex-row-layout ai-center jc-center
                                    no-margin d-inline">
                                        <span className="no-margin text-2x">+</span>
                                        <StyledCountUp delay={2} start={0} end={4200} duration={8}></StyledCountUp>
                                    </h1>
                                    <p>Operaciones realizadas</p>
                                </div>
                                <div className="flex-33 text-center">
                                    <h1 className="text-primary flex-row-layout ai-center jc-center
                                    no-margin d-inline">
                                        <span className="no-margin text-2x">+</span>
                                        <StyledCountUp delay={2} start={0} end={5250000} duration={8}></StyledCountUp>
                                    </h1>
                                    <p>Soles en Pagos</p>
                                </div>
                            </div>
                        </div>
                        <div className="spacer-50"></div>
                        <div className="spacer-50"></div>
                        <h4 className="text-center">
                            TE TRANSFERIMOS EL DINERO A TU CUENTA DE AHORRO DE LOS SIGUIENTES BANCOS
                        </h4>
                        <div className="spacer-50"></div>
                        {/* <div className="spacer-50"></div> */}

                        <div className="section__content section__content--fitHeight no-padding">

                            <div className="flex-row-layout ai-center ai-center jc-space-between text-center
                            fill-width">
                                <div className="flex-33 text-center">
                                    <img className="img-banks" src="/images/banks/bcp.png" alt="" width="180"/>
                                </div>
                                <div className="flex-33 text-center">
                                    <img className="img-banks" src="/images/banks/interbank.png" alt="" width="180"/>
                                </div>
                                <div className="flex-33 text-center">
                                    <img className="img-banks" src="/images/banks/bbva.png" alt="" width="180"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fade>

        </StyledWithdrawPage>
    );
}


export default SaveMoneyPage;
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
    axios.defaults.xsrfCookieName = 'csrftoken'
    axios.defaults.xsrfHeaderName = 'X-CSRFToken'



class SaveMoneyForm extends Component {
    constructor(props){
        super(props);
        this.state={
            data:{
                email:'',
                full_name:'',
                cellphone:'',
                amount_to_pay:'',
                university:''
            },
            errors:{}
        }
        this.button_submit = React.createRef();
        this.form = React.createRef();
    }

    componentWillMount() {
        window.scrollTo(0, 0);
    }
    onChangeForm = e =>{
        this.setState({
            data:{...this.state.data,[e.target.name]:e.target.value}
        })
    }
    onSubmit = (e) =>{
        e.preventDefault()
        const {data} = this.state;
        const errors = this.validateData(data)
        this.setState({errors})
        if(Object.keys(errors).length === 0){
            // console.log('enviando');
            var csrftoken = getCookie('csrftoken');
            this.button_submit.current.innerText = 'Enviando ...'
            // console.log(data)
            fetch('https://wepayu.pe/backend/api/v1.0/landing/contact-saveup/',{
                method: 'POST',
                body: JSON.stringify(data),
                headers:{
                    'Content-Type': 'application/json',
                }
            }).then( res => {
                //console.log(res)
                return res.json()
            }).then( res => {
                // console.log(res);
                this.setState({
                    data : {
                        email: '',
                        full_name: '',
                        cellphone: '',
                        amount_to_pay: '',
                        university:''
                    }
                })
                this.button_submit.current.innerText = 'Empieza ahora'
                // this.props.history.push('/muchas-gracias')
            })
            .catch( err => {
                this.button_submit.current.innerText = 'Envíar solicitud'
            })
        }else{
            // console.log('nada');
        }
    }
    validateData = data => {
        const errors = {}
        if (data.full_name.length < 3 ) errors.full_name = "* Ingrese un nombre completo"
        if (!Validator.isEmail(data.email)) errors.email = "* Email inválido"
        if (!Validator.isInt(data.cellphone, {min:900000000, max: 999999999})){
            errors.cellphone = "* Ingrese un número móvil válido"
        }
        if (data.amount_to_pay <= 10){
            errors.amount_to_pay = "* Se requiere un monto válido"
        }
        if (data.university.length < 3 ) errors.university = "*Ingrese su Universidad"

        return errors
    }
    render() {
        const {data,errors} = this.state;
        return (
            <React.Fragment>
                 <div className="fill-width">
                    <form onSubmit={this.onSubmit}>
                        <h1>
                            Empieza a retirar ahora. <b>Regístrate</b>
                        </h1>
                        <div className="spacer-25"></div>
                        <div className="flex-row-layout jc-space-between text-center">
                            <div className="flex-30">
                                <input className="input-withdraw" type="text" placeholder="Nombre y Apellido" id="full_name" name="full_name" value={data.full_name} onChange={this.onChangeForm}/>
                                <br/>
                                { errors.full_name && <span className="feedback-form">{errors.full_name}</span>}
                            </div>
                            <div className="flex-30">
                                <input className="input-withdraw" type="text" placeholder="Correo" id="email" name="email" value={data.email} onChange={this.onChangeForm}/>
                                <br/>
                                { errors.email && <span className="feedback-form">{errors.email}</span>}
                            </div>
                            <div className="flex-30">
                                <input className="input-withdraw" type="number" placeholder="Celular" id="cellphone" name="cellphone" value={data.cellphone} onChange={this.onChangeForm}/>
                                <br/>
                                { errors.cellphone && <span className="feedback-form">{errors.cellphone}</span>}
                            </div>
                        </div>
                        <div className="spacer-30 space-mobile"></div>
                        <div className="flex-row-layout jc-space-between text-center
                        ai-flex-end">
                            <div className="flex-30">
                                <input className="input-withdraw" type="number" placeholder="Monto a Pagar" id="amount_to_pay" name="amount_to_pay" value={data.amount_to_pay} onChange={this.onChangeForm}/>
                                <br/>
                                { errors.amount_to_pay && <span className="feedback-form">{errors.amount_to_pay}</span>}
                            </div>
                            <div className="flex-30">
                                <input className="input-withdraw" type="text" placeholder="Universidad" id="university" name="university" value={data.university} onChange={this.onChangeForm}/>
                                <br/>
                                { errors.university && <span className="feedback-form">{errors.university}</span>}
                            </div>
                            <div className="flex-30 button-withdraw">
                                <button ref={this.button_submit}>Enviar solicitud</button>
                            </div>
                        </div>
                    </form>
                </div>
            </React.Fragment>
        )
    }
}

class FormSaveMoney extends Component {
    constructor(props){
        super(props);
        this.state={
            data:{
                email:'',
                full_name:'',
                cellphone:'',
                amount_to_pay:'',
                university:''
            },
            errors:{}
        }
        this.button_submit = React.createRef();
        this.form = React.createRef();
    }

    componentWillMount() {
        window.scrollTo(0, 0);
    }
    onChangeForm = e =>{
        this.setState({
            data:{...this.state.data,[e.target.name]:e.target.value}
        })
    }
    onSubmit = (e) =>{
        e.preventDefault()
        const {data} = this.state;
        const errors = this.validateData(data)
        this.setState({errors})
        if(Object.keys(errors).length === 0){
            // console.log('enviando');
            var csrftoken = getCookie('csrftoken');
            this.button_submit.current.innerText = 'Enviando ...'
            // console.log(data)
            fetch('https://wepayu.pe/backend/api/v1.0/landing/contact-saveup/',{
                method: 'POST',
                body: JSON.stringify(data),
                headers:{
                    'Content-Type': 'application/json',
                }
            }).then( res => {
                //console.log(res)
                return res.json()
            }).then( res => {
                // console.log(res);
                this.setState({
                    data : {
                        email: '',
                        full_name: '',
                        cellphone: '',
                        amount_to_pay: '',
                        university:''
                    }
                })
                this.button_submit.current.innerText = 'Empieza ahora'
                // this.props.history.push('/muchas-gracias')
            })
            .catch( err => {
                this.button_submit.current.innerText = 'Envíar solicitud'
            })
        }else{
            // console.log('nada');
        }
    }
    validateData = data => {
        const errors = {}
        if (data.full_name.length < 3 ) errors.full_name = "* Ingrese un nombre completo"
        if (!Validator.isEmail(data.email)) errors.email = "* Email inválido"
        if (!Validator.isInt(data.cellphone, {min:900000000, max: 999999999})){
            errors.cellphone = "* Ingrese un número móvil válido"
        }
        if (data.amount_to_pay <= 10){
            errors.amount_to_pay = "* Se requiere un monto válido"
        }
        if (data.university.length < 3 ) errors.university = "*Ingrese su Universidad"

        return errors
    }
    render() {
        const {data,errors} = this.state;
        return (
            <React.Fragment>
                <form className="yourInfoForm" onSubmit={this.onSubmit}>
                    <h2 className="text-bold text-primary mb-20">
                        Ingresa tus datos
                    </h2>
                    <div className="flex-row-layout ai-center jc-space-between mb-20">
                        <div className="flex-100">
                            <input type="text" placeholder="Nombre y Apellido" id="full_name" name="full_name" value={data.full_name} onChange={this.onChangeForm}
                            className="fill-width"/>
                            { errors.full_name && <span className="feedback-form">{errors.full_name}</span>}
                        </div>

                    </div>
                    <div className="flex-row-layout ai-center jc-space-between mb-20">
                        <div className="flex-100">
                            <input type="text" placeholder="Correo electrónico" id="email" name="email" value={data.email} onChange={this.onChangeForm}
                            className="fill-width"/>
                            { errors.email && <span className="feedback-form">{errors.email}</span>}
                        </div>
                    </div>
                    <div className="flex-row-layout ai-center jc-space-between mb-20">
                        <div className="flex-100">
                            <input type="number" placeholder="Teléfono Móvil" id="cellphone" name="cellphone" value={data.cellphone} onChange={this.onChangeForm}
                            className="fill-width"/>
                            { errors.cellphone && <span className="feedback-form">{errors.cellphone}</span>}
                        </div>
                    </div>
                    <div className="flex-row-layout ai-center jc-space-between mb-20">
                        <div className="flex-100">
                            <input type="number" placeholder="Monto a Pagar"  id="amount_to_pay" name="amount_to_pay" value={data.amount_to_pay} onChange={this.onChangeForm}
                            className="fill-width"/>
                            { errors.amount_to_pay && <span className="feedback-form">{errors.amount_to_pay}</span>}
                        </div>
                    </div>
                    <div className="flex-row-layout ai-center jc-space-between mb-20">
                        <div className="flex-100">
                            <input type="text" placeholder="Universidad"  id="university" name="university" value={data.university} onChange={this.onChangeForm}
                            className="fill-width"/>
                            { errors.university && <span className="feedback-form">{errors.university}</span>}
                        </div>
                    </div>

                    <div className="text-center">
                        <button className="wpy-btn wpy-btn--primary" ref={this.button_submit}>
                            Regístrate
                        </button>
                    </div>
                </form>
            </React.Fragment>
        )
    }
}
