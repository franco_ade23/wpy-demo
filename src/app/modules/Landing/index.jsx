import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { RUTA_LANDING_HOME, RUTA_LANDING_AHORRA, RUTA_LANDING_RETIROS, RUTA_LANDING_NOSOTROS } from '../../constants/routes.constants';
import HomePage from './pages/HomePage';
//import Header from './components/header';
import SaveMoneyPage from './pages/SaveMoneyPage';
import WithdrawPage from './pages/WithdrawPage';
import UsPage from './pages/UsPage';
import Footer from './components/footer';
import NewHeader from './components/header/NewHeader';

const Landing = () => {
    return (
        <div>
            <NewHeader></NewHeader>
            <Switch>
                <Route path={RUTA_LANDING_HOME} exact component={HomePage}></Route>
                <Route path={RUTA_LANDING_AHORRA} exact component={SaveMoneyPage}></Route>
                <Route path={RUTA_LANDING_RETIROS} exact component={WithdrawPage}></Route>
                <Route path={RUTA_LANDING_NOSOTROS} exact component={UsPage}></Route>
                <Redirect to={RUTA_LANDING_HOME}></Redirect>
            </Switch>
            <Footer />
        </div>
    )
}

export default Landing;