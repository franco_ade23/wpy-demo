import React from "react";
import { connect } from "react-redux";
import Sidenav from "./Sidenav";

const SidenavContainer = props => {
	return <Sidenav {...props}></Sidenav>;
};

export default connect(state => ({ profile: state.auth.user.typeClient }))(
	SidenavContainer
);
