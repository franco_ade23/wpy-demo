import React from 'react';
import styled from 'styled-components';
import { NavLink as Link } from 'react-router-dom';

import { WITHDRAWER_PROFILE } from '../../../../constants/profile.constants';
import { withdrawProfileMenu, paymentProfileMenu } from '../../data/menu';
import { colors } from '../../../../../styles/js/1-settings/colors';

const StyledSidenav = styled.div`
    width: 225px;
    .menuItem{
        height: 50px;
        padding-left: 35px;
        border-right:4px solid ${colors.primaryColor};
        margin-top: 25px;
        display: block;
        transition: all .2s ease-in-out;
        &__content{
            height: 100%;
            display: flex;
            align-items: center;
            img{
                height: 25px;
                margin-right: 15px;
            }
            label{
                cursor: pointer;
            }
        }
        :not(.active){
            border-right-color: transparent;
            .menuItem__content{
                filter: brightness(0) opacity(0.5);
            }
        }
    }
`;

const renderMenuItem = (menuItem, index) => {
    return (
        <Link to={menuItem.actionUrl} activeClassName='active' className="menuItem" key={index}>
            <div className="menuItem__content">
                <img src={menuItem.iconUrl} alt="" />
                <label>
                    {menuItem.label}
                </label>
            </div>
        </Link>
    );
}

const Sidenav = ({ profile }) => {
    // console.log(profile);
    const menu = profile === WITHDRAWER_PROFILE ? withdrawProfileMenu : paymentProfileMenu;
    return (
        <StyledSidenav>
            {
                menu.map(renderMenuItem)
            }
        </StyledSidenav>
    );
}

export default Sidenav;