import React from 'react';
import styled from 'styled-components';

import Username from '../../../../components/Username';
import { withLogout } from '../../../../hoc/withLogout';
import avatar from '../../../../../assets/images/avatar_placeholder.jpg';
import './header.css';

import Sidenav from '../Sidenav/index';

const StyledHeader = styled.div`
    padding: 25px 45px;
    background-image: url('/images/dashboard/header-bottom-line.png');
    background-repeat: no-repeat;
    background-position: center calc(100% + 3px);
    background-size: 130% 5px;
    .img-profile {
        border-radius : 50px;
        width:35px;
    }
    .flex-nav-dashboard{
        display:flex;
        align-items:center;
    }
    .off-session{
        background:transparent;
        padding: 0px;
    }
    .nav-main-flex{
        display:flex;
        align-items:center;
    }
`;


const Header = ({ user, logout }) => {
    const { photoUrl } = user;
    return (
        <StyledHeader>
            <div className="flex-row-layout nav-main-flex">
                <nav role="navigation" className="navigation">
                    <div id="menuToggle">

                        <input type="checkbox" />


                        <span></span>
                        <span></span>
                        <span></span>


                        <ul id="menu">
                            <Sidenav></Sidenav>

                        </ul>
                    </div>
                </nav>
                <div>
                    <img src="/images/logo-wepayu.png" className="mobile-space" alt="" width="130" />
                </div>
                <span className="flex"></span>
                <div className="spacer-h-15"></div>
                <div className="flex-nav-dashboard" role="navigation" id="menuToggle2">
                    <img src={photoUrl || avatar} className="img-profile" alt="" width="35" />
                    <div className="spacer-h-15"></div>
                    <span>
                        Hola, <Username></Username>
                    </span>
                    <div className="spacer-h-15"></div>
                    <button className="off-session" onClick={logout}>
                        <i className="fa fa-power-off" aria-hidden="true"></i>

                    </button>
                </div>

            </div>
        </StyledHeader>
    );
}

export default withLogout(Header);
