import React from 'react';
import { connect } from 'react-redux';

import Header from './Header';

const HeaderContainer = (props) => {
    return (
        <Header {...props}></Header>
    )
}

export default connect(
    state => ({ user: state.auth.user })
)(HeaderContainer);