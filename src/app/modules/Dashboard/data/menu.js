import { RUTA_MI_PERFIL, RUTA_PAGOS, RUTA_RETIROS, RUTA_TARJETAS_CUENTAS, RUTA_HISTORIAL} from "../../../constants/routes.constants";

export const myAccountMenuItem = {
    iconUrl: '/images/dashboard/account-menu-icon.png',
    label: 'Mi perfil',
    actionUrl: RUTA_MI_PERFIL
};

export const paymentsMenuItem = {
    iconUrl: '/images/dashboard/payment-menu-icon.png',
    label: 'Pagos',
    actionUrl: RUTA_PAGOS
};

export const withdrawsMenuItem = {
    iconUrl: '/images/dashboard/withdraw-menu-icon.png',
    label: 'Retiros',
    actionUrl: RUTA_RETIROS
};

export const withdrawsCardsAndAccounts = {
    iconUrl: '/images/dashboard/withdraw-menu-icon.png',
    label: 'Cuentas y Tarjetas',
    actionUrl: RUTA_TARJETAS_CUENTAS
};

export const withdrawsHistory= {
    iconUrl: '/images/dashboard/withdraw-menu-icon.png',
    label: 'Historial',
    actionUrl: RUTA_HISTORIAL
};

export const withdrawProfileMenu = [
    myAccountMenuItem, withdrawsMenuItem, withdrawsCardsAndAccounts, withdrawsHistory
];

export const paymentProfileMenu = [
    myAccountMenuItem, paymentsMenuItem
];
