import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Switch, Route } from "react-router-dom";

import { NO_PROFILE, WITHDRAWER_PROFILE, PAYMENT_PROFILE } from "../../constants/profile.constants";
import { withToastService } from "../Toasts/hoc/withToastService";
import {
  RUTA_SELECIONAR_PERFIL,
  RUTA_DASHBOARD,
  RUTA_MI_PERFIL,
  RUTA_PAGOS,
  RUTA_RETIROS,
  RUTA_TARJETAS_CUENTAS,
  RUTA_HISTORIAL,
  RUTA_RETIROS_NEW,
  RUTA_RETIROS_BOLETAS
} from "../../constants/routes.constants";
import DashboardView from "./pages/DashboardView";
import MyProfile from "../MyProfile";
import Payments from "../Payments";
import Withdraws from "../Withdraws";

import WithdrawPage from '../Withdraws/pages/WithdrawPage'
import CardsAccountPage from '../Withdraws/pages/CardsAccountPage'
import HistorialPage from '../Withdraws/pages/HistorialPage'
import NewWithdrawPage from '../Withdraws/pages/NewWithdrawPage'
import SelectBoletasPage from "../Withdraws/pages/SelectBoletasPage"
import RouteWithdraw from "../../components/RouteWithdraw"

const DashboardPage = ({ user }) => {
  if (user.typeClient == PAYMENT_PROFILE) {
    return <Redirect to={RUTA_PAGOS} />
  } else if (user.typeClient == WITHDRAWER_PROFILE) {
    return <Redirect to={RUTA_RETIROS} />
  } else {
    return null
  }
}

const modulesRouting = ({ user }) => {
  console.log('MODULE ROUTING');
  return (
    <Switch>
      <Route exact path={RUTA_DASHBOARD} render={() => <DashboardPage user={user} />} />
      <Route exact path={RUTA_MI_PERFIL} component={MyProfile} />
      <Route path={RUTA_PAGOS} component={Payments} />
      <RouteWithdraw typeClient={user.typeClient}>
        <Route path={RUTA_RETIROS} exact component={WithdrawPage} />
        <Route path={RUTA_RETIROS_NEW} component={NewWithdrawPage} />
        <Route path={RUTA_RETIROS_BOLETAS} component={SelectBoletasPage} />
        <Route path={RUTA_TARJETAS_CUENTAS} component={CardsAccountPage} />
        <Route path={RUTA_HISTORIAL} component={HistorialPage} />
      </RouteWithdraw>
    </Switch>
  )
}

const Dashboard = ({ user, toastService }) => {
  console.log('DASHBOARD');
  useEffect(() => {
    toastService.success("Bienvenido");
  }, [toastService]);

  if (user.typeClient === NO_PROFILE) {
    toastService.info("Aún no cuentas con un perfil");
    return <Redirect to={RUTA_SELECIONAR_PERFIL}></Redirect>;
  }

  return (
    <Switch>
      <Route
        path={RUTA_DASHBOARD}
        render={() => (
          <DashboardView user={user} ModulesRouting={modulesRouting} />
        )}
      ></Route>
    </Switch>
  );
};

export default withToastService(
  connect(state => ({ user: state.auth.user }))(Dashboard)
);