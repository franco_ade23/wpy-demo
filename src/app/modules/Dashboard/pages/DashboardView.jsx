import React from 'react';
import styled from 'styled-components';
import Header from '../components/Header';
import Sidenav from '../components/Sidenav';

const StyledDashboardView = styled.div`
    height: 100vh;
    .dashboardContent{
        background-color: #f2f3f4;
    }
`;

const DashboardView = ({ ModulesRouting, user }) => {
    return (
        <StyledDashboardView>
            <div className="flex-col-layout fill-height">
                <div className="flex-auto no-padding">
                    <Header></Header>
                </div>
                <div className="flex no-padding flex-row-layout">
                    <div className="flex-auto no-padding flex-nav-none">
                        <Sidenav></Sidenav>
                    </div>
                    <div className="dashboardContent flex scrollable no-padding">
                        <ModulesRouting user={user}></ModulesRouting>
                    </div>
                </div>
            </div>
        </StyledDashboardView>
    )
}

export default DashboardView;