import api from "../../../services/api.service";
import { WITHDRAWER_PROFILE, WITHDRAWER_PROFILE_FOR_UPDATE, PAYMENT_PROFILE_FOR_UPDATE } from "../../../constants/profile.constants";

const saveProfile = ( profile ) => {
    return api.patch('user/',{
        type_client: profile === WITHDRAWER_PROFILE ? 
            WITHDRAWER_PROFILE_FOR_UPDATE : PAYMENT_PROFILE_FOR_UPDATE
    });
}

export const selectProfileService = {
    saveProfile
}