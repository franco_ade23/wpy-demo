import React from "react";
import styled from "styled-components";
import ProfileBox from "../components/ProfileBox";
import ProfileBoxResponsive from "../components/ProfileBoxResponsive";

import ProfilePicker from "../components/ProfilePicker";
import Username from "../../../components/Username";
import Header from "../components/Header";
import './profile.css'

const StyledSelectProfileView = styled.div`
  background-image: url("/images/select-profile/select-profile-bg.png");
  background-position: 105% 5%;
  background-size: 55% 125%;
  background-repeat: no-repeat;
  height: 100vh;
  display: flex;
  position: relative;
  .header {
    width: 100%;
    position: absolute;
    left: 0;
    top: 0;
  }
  .left {
    text-align: right;
    flex: 0 0 45%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-right: 50px;
    align-items: flex-end;
    p {
      font-size: 1.2em;
    }
  }
  .right {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
    .profileBoxWrapper {
      width: 400px;
    }
  }
`;

const SelectProfileView = () => {
  return (
    <StyledSelectProfileView>
      <div className="header">
        <div className="header__inner">
          <Header></Header>
        </div>
      </div>
      <div className="left">
        <h1 className="text-primary">
          <b>
            ¡BIENVENIDO, <Username></Username>!
          </b>
        </h1>
        {/* <p>
                    Mira todo lo que puedes hacer en WePayU, <br/>
                    con solo unos simples pasos!&nbsp;
                </p> */}
        <p>
          A continuación, escoge qué tipo de operaciones <br />
          harás en nuestra plataforma.
        </p>
        <div className="spacer-40"></div>
        <ProfilePicker></ProfilePicker>
        <div className="to-responsive">
          <ProfileBoxResponsive></ProfileBoxResponsive>

        </div>
        

      </div>
      <div className="right">
        <div className="profileBoxWrapper">
          <ProfileBox></ProfileBox>
        </div>
      </div>
    </StyledSelectProfileView>
  );
};

export default SelectProfileView;
