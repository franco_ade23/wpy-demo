import React from 'react';
import styled from 'styled-components';
import { colors } from '../../../../../styles/js/1-settings/colors';

const StyledHeader = styled.div`
    padding: 24px;
    background-image: url('/images/select-profile/header-bottom-line.png');
    background-repeat: no-repeat;
    background-position: bottom;
    background-size: 100% auto;
    .inner{
        max-width: 1150px;
        margin: auto;
    }
    button{
        padding: 10px 15px;
        border-radius: 10px;
        cursor: pointer;
        min-width: 110px;
    }
    .btnLogout{
        border: 1px solid #FFF;
        background-color: transparent;
        color: #FFF;
    }
    .btnMyAccount{
        border: 1px solid #FFF;
        background-color: #FFF;
        color: ${colors.primaryColor}
    }
`;

const Header = ({ logout }) => {
    return (
        <StyledHeader>
            <div className="inner">
                <div className="flex-row-layout ai-center">
                    <img src="/images/logo-wepayu.png" alt="" width="175"/>
                    <span className="flex"></span>
                    <button className="btnLogout" onClick={ () => logout() }>
                        Cerrar sesión
                    </button>
                    <div className="spacer-h-20"></div>
                    <button className="btnMyAccount">
                        Mi cuenta
                    </button>
                </div>
            </div>
        </StyledHeader>
    )
}

export default Header;