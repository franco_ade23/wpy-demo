import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../../../Auth/state/actions';
import Header from './Header';

const HeaderContainer = ( props ) => {
    return <Header {...props}></Header>
}

export default connect(
    null,
    dispatch => bindActionCreators({ logout }, dispatch)
)(HeaderContainer);