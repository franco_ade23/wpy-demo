import React, { useState } from 'react';
import styled from 'styled-components';
import { WITHDRAWER_PROFILE  } from '../../../../constants/profile.constants';
//PAYMENT_PROFILE
import { withToastService } from '../../../Toasts/hoc/withToastService';

const PaymentProfileBoxResponsive = ({ onStart, loading }) => {
    return (
        <div>
           
            <button className="wpy-btn" onClick={onStart}
            disabled={loading}>
                Empieza ya
            </button>
        </div>
    )
}

const WithdrawProfileBoxResponsive = ({ onStart, loading }) => {
    return (
        <div>
           
            <button className="wpy-btn" onClick={onStart}
            disabled={loading}>
                Empieza ya
            </button>
        </div>
    )
}

const StyledProfileBox = styled.div`
    color: #FFF;
    text-align: center;
    font-size: 1.15em;
    
    .inner{
        width:100%;
        margin-top:20px;
    }
    .title{
        font-size: 1.3em;
        font-weight: bold;
    }
    input{
        width: 80%;
        background-color: #FFF;
        border-color: #FFF;
        color: #000;
        text-align: center;
    }
    button{
        width: 180px;
    }
`;

const ProfileBoxResponsive = ({ profile, saveSelectedProfile, toastService }) => {
    
    const [ loading, setLoading ] = useState(false);

    const saveSelectedProfileCb = ( err ) => {
        setLoading(false);
        if( !!err ){
            toastService.error('Hubo un error al actualizar su perfil, por favor intente nuevamente');
            return;
        }
        toastService.success('Su perfil ha sido actualizado con éxito');
    }

    const onSaveSelectedProfile = () => {
        setLoading(true);
        saveSelectedProfile( saveSelectedProfileCb );
    }

    return (
        <StyledProfileBox>
            <div className="inner">
            {
                profile === WITHDRAWER_PROFILE ?
                <WithdrawProfileBoxResponsive onStart={ () => onSaveSelectedProfile() }
                    loading={loading}>
                </WithdrawProfileBoxResponsive>                
                :
                <PaymentProfileBoxResponsive onStart={ () => onSaveSelectedProfile() }
                loading={loading}>
                </PaymentProfileBoxResponsive>
            }
            </div>
        </StyledProfileBox>
    );
}

export default withToastService( ProfileBoxResponsive );