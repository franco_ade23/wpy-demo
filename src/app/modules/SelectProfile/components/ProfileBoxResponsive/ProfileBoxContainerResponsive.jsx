import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ProfileBoxResponsive from './ProfileBoxResponsive';
import { saveSelectedProfile } from '../../state/actions';

const ProfileBoxContainerResponsive = (props) => {
    return (
        <ProfileBoxResponsive {...props}></ProfileBoxResponsive>
    )
}

export default connect(
    state => ({
        profile: state.selectProfile.selectedProfile
    }),
    dispatch => bindActionCreators({ saveSelectedProfile },dispatch)
)(ProfileBoxContainerResponsive);