import React from 'react';
import styled from 'styled-components';
import { PAYMENT_PROFILE, WITHDRAWER_PROFILE } from '../../../../constants/profile.constants';
//PAYMENT_PROFILE_FOR_UPDATE, WITHDRAWER_PROFILE_FOR_UPDATE 
import { colors } from '../../../../../styles/js/1-settings/colors';

const profileOptions = [
    {
        iconUrl: '/images/select-profile/payment-profile-option.png',
        label: 'Ahorra en tus pagos',
        value: PAYMENT_PROFILE
    },
    {
        iconUrl: '/images/select-profile/withdraw-profile-option.png',
        label: 'Retira dinero rápido',
        value: WITHDRAWER_PROFILE
    }
]

const StyledProfilePicker = styled.div`
    .option{
        filter: opacity(0.5);
        transition: all 0.2s ease-in-out;
        display: flex;
        align-items: center;
        cursor: pointer;
        label{
            font-size: 1.3em;
            font-weight: 900;
            letter-spacing: 0.1px;
            margin-left: 20px;
        }
        &:not(:last-child){
            margin-bottom: 22px;
        }
        &.active{
            filter: opacity(1);
            color: ${colors.primaryColor};
        }        
    }
`;

const ProfilePicker = ({ selectedProfile, setProfile }) => {
    return (
        <StyledProfilePicker>
            {
                profileOptions.map( (option,i) => (
                    <div key={i}
                    className={ `option ${selectedProfile===option.value?'active':''}` }  
                    onClick={ () => setProfile(option.value) }>
                        <img src={ option.iconUrl } alt=""/>
                        <label>{ option.label }</label>
                    </div>
                ))
            }     
        </StyledProfilePicker>
    )
}

export default ProfilePicker;