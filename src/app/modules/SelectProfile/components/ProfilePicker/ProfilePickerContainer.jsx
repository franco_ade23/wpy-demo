import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ProfilePicker from './ProfilePicker';
import { setProfile } from '../../state/actions';

const ProfilePickerContainer = ( props ) => {
    return (
        <ProfilePicker {...props}></ProfilePicker>
    )
}

export default connect(
    state => ({ selectedProfile: state.selectProfile.selectedProfile }),
    dispatch => bindActionCreators({ setProfile },dispatch)
)(ProfilePickerContainer);