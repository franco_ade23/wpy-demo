import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ProfileBox from './ProfileBox';
import { saveSelectedProfile } from '../../state/actions';

const ProfileBoxContainer = (props) => {
    return (
        <ProfileBox {...props}></ProfileBox>
    )
}

export default connect(
    state => ({
        profile: state.selectProfile.selectedProfile
    }),
    dispatch => bindActionCreators({ saveSelectedProfile },dispatch)
)(ProfileBoxContainer);