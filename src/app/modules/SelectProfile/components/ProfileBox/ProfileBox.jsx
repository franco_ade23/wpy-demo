import React, { useState } from 'react';
import styled from 'styled-components';
import { WITHDRAWER_PROFILE  } from '../../../../constants/profile.constants';
//PAYMENT_PROFILE
import { withToastService } from '../../../Toasts/hoc/withToastService';

const PaymentProfileBox = ({ onStart, loading }) => {
    return (
        <div>
            <p className="title">
                AHORRA
            </p>
            <div className="spacer-20"></div>
            <p>
                Monto a pagar
            </p>
            <div className="spacer-15"></div>
            <input type="number"/>
            <div className="spacer-20"></div>
            <p>
                Descuento con WePayU
            </p>
            <p>
                <b>S/. 0.00</b>
            </p>
            <div className="spacer-20"></div>
            <p>
                Monto a depositar
            </p>
            <p>
                <b>S/. 0.00</b>
            </p>
            <div className="spacer-30"></div>
            <button className="wpy-btn" onClick={onStart}
            disabled={loading}>
                Empieza ya
            </button>
        </div>
    )
}

const WithdrawProfileBox = ({ onStart, loading }) => {
    return (
        <div>
            <p className="title">
                RETIRA
            </p>
            <div className="spacer-20"></div>
            <p>
                Saldo disponible en tu tarjeta
            </p>
            <div className="spacer-15"></div>
            <input type="number"/>
            <div className="spacer-20"></div>
            <p>
                Comisión con WePayU
            </p>
            <p>
                <b>S/. 0.00</b>
            </p>
            <div className="spacer-20"></div>
            <p>
                Te depositamos
            </p>
            <p>
                <b>S/. 0.00</b>
            </p>
            <div className="spacer-30"></div>
            <button className="wpy-btn" onClick={onStart}
            disabled={loading}>
                Empieza ya
            </button>
        </div>
    )
}

const StyledProfileBox = styled.div`
    color: #FFF;
    text-align: center;
    font-size: 1.15em;
    padding: 60px 40px;
    background-image: url('/images/select-profile/profile-box-bg.png');
    background-position: center;
    background-size: 100% 100%;
    .inner{
    }
    .title{
        font-size: 1.3em;
        font-weight: bold;
    }
    input{
        width: 80%;
        background-color: #FFF;
        border-color: #FFF;
        color: #000;
        text-align: center;
    }
    button{
        width: 180px;
    }
`;

const ProfileBox = ({ profile, saveSelectedProfile, toastService }) => {
    
    const [ loading, setLoading ] = useState(false);

    const saveSelectedProfileCb = ( err ) => {
        setLoading(false);
        if( !!err ){
            toastService.error('Hubo un error al actualizar su perfil, por favor intente nuevamente');
            return;
        }
        toastService.success('Su perfil ha sido actualizado con éxito');
    }

    const onSaveSelectedProfile = () => {
        setLoading(true);
        saveSelectedProfile( saveSelectedProfileCb );
    }

    return (
        <StyledProfileBox>
            <div className="inner">
            {
                profile === WITHDRAWER_PROFILE ?
                <React.Fragment>
                    <WithdrawProfileBox onStart={ () => onSaveSelectedProfile() }
                        loading={loading}>
                    </WithdrawProfileBox>
                </React.Fragment>
                
                :
                <PaymentProfileBox onStart={ () => onSaveSelectedProfile() }
                loading={loading}>
                </PaymentProfileBox>
            }
            </div>
        </StyledProfileBox>
    );
}

export default withToastService( ProfileBox );