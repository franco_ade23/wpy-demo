import { WITHDRAWER_PROFILE } from "../../../constants/profile.constants";

export const selectProfileState = {
    selectedProfile: WITHDRAWER_PROFILE
}