import { selectProfileState } from "./state";
import { SELECT_PROFILE } from "./action-types";

export const selectProfileReducer = ( state = selectProfileState, action ) => {
    switch(action.type){
        case SELECT_PROFILE:
            return {
                ...state,
                selectedProfile: action.payload.profile
            }
        default:
            return state;
    }
}