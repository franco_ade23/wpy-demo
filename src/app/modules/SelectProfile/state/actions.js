import { SELECT_PROFILE } from "./action-types";
import { WITHDRAWER_PROFILE, PAYMENT_PROFILE } from "../../../constants/profile.constants";
import { selectProfileService } from "../services/selectProfile.service";
import { populate } from "../../Auth/state/actions";

export const setWithdrawerProfile = ( profile ) => {
    return {
        type: SELECT_PROFILE,
        payload: {
            profile: WITHDRAWER_PROFILE
        }
    }
}

export const setPaymentProfile = ( profile ) => {
    return {
        type: PAYMENT_PROFILE,
        payload: {
            profile: WITHDRAWER_PROFILE
        }
    }
}

export const setProfile = ( profile ) => {
    return {
        type: SELECT_PROFILE,
        payload: {
            profile: profile
        }
    }
}

export const saveSelectedProfile = ( cb ) => {
    return async ( dispatch, getState ) => {
        try{
            const { selectedProfile } = getState().selectProfile;
            await selectProfileService.saveProfile( selectedProfile );
            cb( null )
            dispatch( populate() );
        }catch(err){
            cb( err );
        }
    }
}