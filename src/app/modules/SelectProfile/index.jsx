import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import SelectProfileView from "./pages/SelectProfileView";
import {
  RUTA_SELECIONAR_PERFIL,
  RUTA_DASHBOARD
} from "../../constants/routes.constants";
import { NO_PROFILE } from "../../constants/profile.constants";

const SelectProfile = ({ user }) => {
  if (user.typeClient !== NO_PROFILE) {
    return <Redirect to={RUTA_DASHBOARD}></Redirect>;
  }
  return (
    <Switch>
      <Route
        path={RUTA_SELECIONAR_PERFIL}
        component={SelectProfileView}
      ></Route>
    </Switch>
  );
};

export default connect(state => ({ user: state.auth.user }))(SelectProfile);
