// import { loadersDefaultState } from "./default-state";
// import { LoadersActionTypes } from "./action-types";
import { pendingTasksReducer } from 'react-redux-spinner';

export const loadersReducer = pendingTasksReducer;

// export const loadersReducer = ( state = loadersDefaultState, action ) => {

//     switch( action.type ){
//         case LoadersActionTypes.HTTP_SHOW_LOADING:
//             return {
//                 ...state,
//                 http: { loading: true }
//             }
//         case LoadersActionTypes.HTTP_HIDE_LOADING:
//             return {
//                 ...state,
//                 http: { loading: false }
//             }
//         default:
//             return state;
//     }

// }