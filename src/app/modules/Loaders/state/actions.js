import { LoadersActionTypes } from "./action-types";
import { pendingTask, begin, end } from 'react-redux-spinner';

export const LoaderActions = {
    showHttpLoader: () => {
        return {
            type: LoadersActionTypes.HTTP_SHOW_LOADING,
            [ pendingTask ]: begin
        }
    },
    hideHttpLoader: () => {
        return {
            type: LoadersActionTypes.HTTP_HIDE_LOADING,
            [ pendingTask ]: end
        }
    }
}