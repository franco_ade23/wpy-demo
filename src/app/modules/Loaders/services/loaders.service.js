import store from "../../../state/store";
import { LoaderActions } from "../state/actions";

// const httpRequestsHeap = [];

const showHttpLoader = () => {
    // console.log('Showing http loader');
    store.dispatch( LoaderActions.showHttpLoader() )
}

const hideHttpLoader = () => {
    // console.log('Hiding http loader');
    store.dispatch( LoaderActions.hideHttpLoader() )
}

export const loadersService = {
    showHttpLoader,
    hideHttpLoader
}