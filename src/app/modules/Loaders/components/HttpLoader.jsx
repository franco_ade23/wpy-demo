import React from 'react';
import { Spinner } from 'react-redux-spinner';

const HttpLoader = ({ loading }) => {
    return (
        loading ? <div><Spinner /></div> : null
    );
}

export default HttpLoader;