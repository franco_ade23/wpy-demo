import React from 'react';
import { connect } from 'react-redux';

import HttpLoader from '../components/HttpLoader';

const HttpLoaderListener = ({ loading }) => {
    return (
        <HttpLoader loading={true}></HttpLoader>
    );
}

export default connect(

)( HttpLoaderListener );
