import React from "react";

import "./styles.scss";
import "./styleProfile.css";
import ModuleLayout from "../../../components/ModuleLayout";
import EditProfileForm from "../components/EditProfileForm";

const MyProfileView = () => {
  return (
    <ModuleLayout
      title="Mi Perfil"
      iconUrl="/images/dashboard/account-menu-icon.png"
      pushTop={true}
    >
      <div className="myProfileView flex-container ">
        <div className="myProfileView__container p-20 bg-white tw-rounded-lg ">
          <EditProfileForm></EditProfileForm>
        </div>
        

      </div>
    </ModuleLayout>
  );
};

export default MyProfileView;
