import { ProfileApi } from "../api/profile.api";
import { populate } from "../../Auth/state/actions";
import { cachedToastService } from "../../Toasts/hoc/withToastService";

export const updateProfile = user => {
  return async (dispatch, getState) => {
    try {
      const updateProfileResponse = await ProfileApi.updateProfile(user);
      console.log(updateProfileResponse);
      cachedToastService.success("El perfil fue actualizando correctamente");
      dispatch(populate());
    } catch (err) {
      console.error(err);
      cachedToastService.error("Ocurrió un error al actualizar el perfil");
    }
  };
};
