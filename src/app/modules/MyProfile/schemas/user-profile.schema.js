import { validateForm, required } from "redux-form-validators";

export const userProfileSchema = validateForm({
  firstName: [required()],
  lastName: [required()],
  dni: [required()],
  photoUrl: [],
  cellphone: [required()],
  birthday: []
});
