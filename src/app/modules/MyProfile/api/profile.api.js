import { User } from "../../../models/user.model";
import api from "../../../services/api.service";

export class ProfileApi {
  static updateProfile = (user = new User()) => {
    return api.patch("user/", {
      first_name: user.firstName,
      last_name: user.lastName,
      cellphone: user.cellphone,
      dni: user.dni,
      email:user.email,
      photo:user.photoUrl,
      wepoints:user.wepoints
    });
  };
}
