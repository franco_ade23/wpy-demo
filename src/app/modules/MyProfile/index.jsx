import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { RUTA_MI_PERFIL } from "../../constants/routes.constants";
import MyProfileView from "./pages/MyProfileView";

export default () => {
  return (
    <Switch>
      <Route path={RUTA_MI_PERFIL} component={MyProfileView}></Route>
      <Redirect to={RUTA_MI_PERFIL}></Redirect>
    </Switch>
  );
};
