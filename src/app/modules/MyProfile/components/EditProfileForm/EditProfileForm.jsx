import React, { useCallback } from "react";
import { useSelector } from "react-redux";
import { Form } from "react-final-form";
import { userProfileSchema } from "../../schemas/user-profile.schema";
import { renderEditProfileForm } from "./renderForm";
import { useActions } from "../../../../hooks/useActions";
import { updateProfile } from "../../state/actions";

const EditProfileForm = () => {
  const { user } = useSelector(state => state.auth);
  const actions = useActions({ updateProfile });
  const handleSubmit = useCallback(
    (values, form) => {
      actions.updateProfile(values);
    },
    [actions]
  );
  const initialValues = {
    ...user
  };
  
  return (
    <Form
      onSubmit={handleSubmit}
      validate={userProfileSchema}
      render={renderEditProfileForm}
      initialValues={initialValues}
    ></Form>
  );
};

export default EditProfileForm;
