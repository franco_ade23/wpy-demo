import React from "react";
import { Field } from "react-final-form";
import styled from "styled-components";
import MaterialInput from "../../../../components/MaterialInput";
import Username from '../../../../components/Username';
import avatar from '../../../../../assets/images/avatar_placeholder.jpg';
import { connect } from 'react-redux';
import { userInfo } from "os";
import { AuthServices } from "../../../Auth/services";


const StyledLoginForm = styled.div`
  .title {
    font-weight: bold;
  }
  .jusfify-center{
    justify-content:center
  }
  .login {
    &__facebookBtn {
      background-color: #3241b0 !important;
      border-color: #3241b0 !important;
    }
    &__gmailBtn {
      background-color: #d54949 !important;
      border-color: #d54949 !important;
    }
  }
`;


export const renderEditProfileForm = ({handleSubmit, ...others }) => {
  
  

  function handleClick(e) {
    e.preventDefault();
    console.log('The link was clicked.');
    
  }

  console.log("GAAAAA")
  console.log(others);

  return (
    <StyledLoginForm>
      <div className="padding-form-profile bg-relative">
        
        <form onSubmit={handleSubmit} disabled={true}>
          <div className="mb-25">
            <Field
              component={MaterialInput}
              name="firstName"
              className="fill-width"
              label="Nombres"
              InputLabelProps={{
                shrink: true
              }}
            ></Field>
          </div>
          
          <div className="mb-25">
            <Field
              component={MaterialInput}
              name="lastName"
              className="fill-width"
              label="Apellidos"
              InputLabelProps={{
                shrink: true
              }}
            ></Field>
          </div>
          <div className="mb-25">
            <Field
              component={MaterialInput}
              type="number"
              name="dni"
              className="fill-width"
              label="Número de documento"
              InputLabelProps={{
                shrink: true
              }}
            ></Field>
          </div>
          <div className="mb-25">
            <Field
              component={MaterialInput}
              name="cellphone"
              className="fill-width"
              label="Teléfono / Celular"
              InputLabelProps={{
                shrink: true
              }}
            ></Field>
          </div>
          <div className="mb-25">
            <Field
            disabled={true}
              component={MaterialInput}
              name="wepoints"
              className="fill-width"
              label="wepoints"
              InputLabelProps={{
                shrink: true
              }}
            ></Field>
          </div>
          <div className="text-center">
            <button className="wpy-btn">Guardar</button>
          </div>
        </form>
        <div className="img-profile-circle">
          <img src={others.initialValues.photoUrl ? others.initialValues.photoUrl: avatar} width="100%"></img>
        </div>
      </div>
    </StyledLoginForm>
  );
};
