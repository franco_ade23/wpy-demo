import store from "../../../state/store";
import { ToastActions } from "../state/actions";

export const ToastService = {
    showInfo: ( message, title = '' ) => {
        store.dispatch( ToastActions.toastInfo(message,title) )
    },
    showWarning: ( message, title = '' ) => {
        store.dispatch( ToastActions.toastWarning(message,title) )
    },
    showError: ( message, title = '' ) => {
        store.dispatch( ToastActions.toastError(message,title) )
    },
    showSuccess: ( message, title = '' ) => {
        store.dispatch( ToastActions.toastSuccess(message,title) )
    }
}