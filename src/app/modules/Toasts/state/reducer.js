import { toastsDefaultState } from "./default-state";
import { ToastsActionTypes } from "./action-types";

export const toastsReducer = ( state = toastsDefaultState, action ) => {
    switch(action.type){
        case ToastsActionTypes.SHOW_TOAST:
            return {
                ...state,
                ...action.payload
            }
        default:
            return state;
    }
}