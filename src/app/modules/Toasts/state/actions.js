import { ToastsActionTypes } from "./action-types";

export const ToastActions = {
    toastInfo: ( message, title = '' ) => {
        return {
            type: ToastsActionTypes.SHOW_TOAST,
            payload: {
                message,
                title,
                type: 'info'
            }
        }
    },
    toastWarning: ( message, title = '' ) => {
        return {
            type: ToastsActionTypes.SHOW_TOAST,
            payload: {
                message,
                title,
                type: 'info'
            }
        }
    },
    toastError: ( message, title = '' ) => {
        return {
            type: ToastsActionTypes.SHOW_TOAST,
            payload: {
                message,
                title,
                type: 'info'
            }
        }
    },
    toastSuccess: ( message, title = '' ) => {
        return {
            type: ToastsActionTypes.SHOW_TOAST,
            payload: {
                message,
                title,
                type: 'info'
            }
        }
    }
}