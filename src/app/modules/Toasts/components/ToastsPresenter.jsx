
import { withToastManager } from 'react-toast-notifications';

const ToastsPresenter = ({ message, title, type, toastManager, i  }) => {
    if( !!message && !!type ){
        toastManager.add( message, {
            appearance: type,
            autoDismiss: true,
            pauseOnHover: true,
        });
    }
    return null;
}

export default withToastManager( ToastsPresenter );