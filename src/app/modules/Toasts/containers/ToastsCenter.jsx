import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import ToastsPresenter from '../components/ToastsPresenter';
import { ToastProvider, DefaultToastContainer } from 'react-toast-notifications';

// const ToastsCenterListener = 
connect(
    state => ({ ...state.toasts })
)(ToastsPresenter);

const StyledToastContainer = styled.div`
    z-index: 10000;
    position: relative;
`;

const ToastsCenter = ({ children }) => {
    const components = {
        ToastContainer: ({ children, ...props }) => (
            <StyledToastContainer>
                <DefaultToastContainer {...props}>
                    {children}
                </DefaultToastContainer>
            </StyledToastContainer>
        )
    }
    return (
        <ToastProvider placement="top-center" components={components}>
            {/* <ToastsCenterListener></ToastsCenterListener> */}
            { children }
        </ToastProvider>
    );
}

export default ToastsCenter;