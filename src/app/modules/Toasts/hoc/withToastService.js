import React from "react";
// import { ToastService } from '../services/toast.service';
import { withToastManager } from "react-toast-notifications";

const defaultToastManagerConfig = {
  autoDismiss: true,
  autoDismissTimeout: 5000,
  pauseOnHover: true
};

export let cachedToastService = null;

const InnerToastServiceInjector = ({ Component, toastManager, ...others }) => {
  const toastService = {
    info: (message, title) =>
      toastManager.add(message, {
        appearance: "info",
        ...defaultToastManagerConfig
      }),
    warn: (message, title) =>
      toastManager.add(message, {
        appearance: "warning",
        ...defaultToastManagerConfig
      }),
    error: (message, title) =>
      toastManager.add(message, {
        appearance: "error",
        ...defaultToastManagerConfig
      }),
    success: (message, title) =>
      toastManager.add(message, {
        appearance: "success",
        ...defaultToastManagerConfig
      })
  };
  cachedToastService = cachedToastService || toastService;
  return <Component toastService={cachedToastService} {...others}></Component>;
};

const _toastServiceInjector = Component => props => (
  <InnerToastServiceInjector
    Component={Component}
    {...props}
  ></InnerToastServiceInjector>
);

export const withToastService = Component => {
  return withToastManager(_toastServiceInjector(Component));
};
