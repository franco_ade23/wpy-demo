// import _ from "lodash";

import api from "../../../services/api.service";

export const WithdrawService = {
	getAccounts: () => api.get("user-withdraw/bank-account"),
	getCreditCards: () => api.get("user-withdraw/payment-credit-card"),
	getBanks: () => api.get("user/service/banks"),
	getBrandCards: () => api.get("user/service/brand-card"),
	addAccountBank: body => api.post("user-withdraw/bank-account", body),
	deleteAccountBank: id => api.delete("user-withdraw/bank-account/" + id),
	addCreditCard: body => api.post("user-withdraw/payment-credit-card", body),
	deleteCreditCard: id => api.delete("user-withdraw/payment-credit-card/" + id),
	addWithdraw: body => api.post("user-withdraw/withdraws", body),
	getWithdraws: () => api.get("user-withdraw/withdraws"),
	addTransaction: body => api.post("user-withdraw/transactions", body),
	getTransactions: () => api.get("user-withdraw/transactions"),
	getBoletas: () => api.get("user-withdraw/list-payments")
}