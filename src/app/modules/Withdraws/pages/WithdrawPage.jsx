import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoaderContent from '../../../components/LoaderContent'
import ErrorServer from '../../../components/ErrorServer/ErrorServer'
import { withToastService } from '../../Toasts/hoc/withToastService'
import Content from '../components/ContentWithdraws/'

import { loadWithdraws, selectWithdraw } from '../state/actions'
import './styles.css'

class WithdrawPage extends Component {

    async componentDidMount() {
        const { loadWithdraws } = this.props
        await loadWithdraws()
    }


    isLoadingContent() {
        const { loading, error, withdraws, history, selectWithdraw } = this.props;
        if (loading) return <LoaderContent />
        if (error) return <ErrorServer />
        else return <Content
            withdraws={withdraws}
            history={history}
            selectWithdraw={selectWithdraw}
        />
    }

    render() {
        return (
            <div className="WithdrawPage CardsAccountPage">
                {this.isLoadingContent()}
            </div>
        );
    }
}

const mapStateToProps = ({ withdraws: { withdrawsModule } }) => ({
    loading: withdrawsModule.loading,
    error: withdrawsModule.error,
    withdraws: withdrawsModule.withdraws
})

const mapDisptachToProps = dispatch => ({
    loadWithdraws: () => dispatch(loadWithdraws()),
    selectWithdraw: payload => dispatch(selectWithdraw(payload))
})

export default withToastService(connect(
    mapStateToProps,
    mapDisptachToProps
)(WithdrawPage));
