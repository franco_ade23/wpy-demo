import React from 'react';
import ModuleLayout from '../../../components/ModuleLayout';
import NewWithdrawModal from '../components/new-withdraw';
import NewWithdrawButton from '../components/new-withdraw/NewWithdrawButton';
import CardWithDraws from './../components/card-with-draws/card-with-draws';

const PublishedWithdraws = () => {

    const renderTitleRight = () => {
        return (
            <div>
                <NewWithdrawButton>Nuevo Retiro</NewWithdrawButton>
            </div>
        )
    }

    return (
        <React.Fragment>
            <ModuleLayout
                title="Retiros"
                iconUrl="/images/dashboard/withdraw-menu-icon.png"
                renderTitleRight={renderTitleRight}>
                <NewWithdrawModal></NewWithdrawModal>
            </ModuleLayout>
            <div className='pad-as-container'>
                <CardWithDraws></CardWithDraws>
            </div>
        </React.Fragment>
    )
}

export default PublishedWithdraws;