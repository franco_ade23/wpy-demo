import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoaderContent from '../../../components/LoaderContent'
import ErrorServer from '../../../components/ErrorServer/ErrorServer'
import { withToastService } from '../../Toasts/hoc/withToastService'
import Content from '../components/ContentCardsAccount/'

import { loadAccountsAndCreditCards } from "../state/actions"
import './styles.css'

class CardsAccountPage extends Component {
    state = {
        isReady: false
    }

    async componentDidMount() {
        const { loadAccountsAndCreditCards } = this.props;
        await loadAccountsAndCreditCards()
        this.setState({ isReady: true })
        this.mainMessage()
    }

    mainMessage() {
        const { creditCards, accountBanks, toastService } = this.props;
        let message = ""
        if (!creditCards.results.length && !accountBanks.results.length) {
            message = "Para procesar un retiro debe tener una tarjeta de crédito y una cuenta bancaria"
        } else if (!creditCards.results.length) {
            message = "Para procesar un retiro debe tener una tarjeta de crédito asociada"
        } else if (!accountBanks.results.length) {
            message = "Para procesar un retiro debe tener una cuenta de ahorro asociada"
        }

        if (message) {
            toastService.warn(message)
        }
    }

    isLoadingContent = () => {
        const { loading,
            error,
            banks,
            brandCards,
            creditCards,
            accountBanks,
            toastService,
            loadAccountsAndCreditCards
        } = this.props;

        const { isReady } = this.state;
        if (loading) return <LoaderContent />
        if (error) return <ErrorServer />
        else if (!loading && isReady) return <Content
            banks={banks}
            brandCards={brandCards}
            creditCards={creditCards}
            accountBanks={accountBanks}
            toastService={toastService}
            loadAccountsAndCreditCards={loadAccountsAndCreditCards}
        />
    }

    render() {
        return (
            <div className="WithdrawPage CardsAccountPage">
                {this.isLoadingContent()}
            </div>
        );
    }
}

const mapStateToProps = ({ withdraws: { accountsAndCardsModule, banks, brandCards } }) => ({
    loading: accountsAndCardsModule.loading,
    error: accountsAndCardsModule.errorModule,
    accountBanks: accountsAndCardsModule.accountBanks,
    creditCards: accountsAndCardsModule.creditCards,
    banks,
    brandCards
})

const mapDisptachToProps = dispatch => ({
    loadAccountsAndCreditCards: () => dispatch(loadAccountsAndCreditCards())
})

export default withToastService(connect(
    mapStateToProps,
    mapDisptachToProps
)(CardsAccountPage));