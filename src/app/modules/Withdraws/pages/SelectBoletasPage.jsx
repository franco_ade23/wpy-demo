import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoaderContent from '../../../components/LoaderContent';
import ErrorServer from '../../../components/ErrorServer/ErrorServer';
import Content from '../components/ContentSelectBoletas/';
import { withToastService } from '../../Toasts/hoc/withToastService';
import { WithdrawService } from '../services/user-withdraws.service'
import { Redirect } from 'react-router-dom';
import { cleanWithdraw, selectWithdraw } from '../state/actions'
import { RUTA_RETIROS } from '../../../constants/routes.constants';

class SelectBoletasPage extends Component {

    state = {
        loading: true,
        error: false,
        boletas: {},
        mount: 0,
        sendingBoletas: false
    }

    async componentDidMount() {
        try {
            const boletas = await WithdrawService.getBoletas()
            const newBoletas = boletas.results.map(b => {
                b.checked = false
                return b
            })
            this.setState({
                boletas: {
                    ...boletas,
                    results: [...newBoletas]
                },
                loading: false,
                error: false
            })
        } catch{
            this.setState({
                loading: false,
                error: true,
                boletas: {}
            })
        }
    }

    resetBoletas = () => {
        const { boletas } = this.state
        this.setState({
            boletas: {
                ...boletas,
                results: boletas.results.map(b => {
                    b.checked = false
                    return b
                })
            }
        }, () => {
            this.getAmmount()
        })
    }

    cantSelectedBoletas = (boletas = []) => {
        let p = ''
        const cant = boletas.filter(b => b.checked).length
        if (cant > 1) {
            p = `${cant} boletas`
        } else if (cant === 1) {
            p = `${cant} boleta`
        } else {
            p = `${cant} boletas`
        }

        return p
    }

    getAmmount = () => {
        const { boletas } = this.state;
        const { selectedWithdraw, toastService } = this.props;
        const selectedBoletas = boletas.results.filter(b => b.checked)
        const mount = selectedBoletas.reduce((acumulator, currentValue) => acumulator + currentValue.amount, 0)

        if (selectedWithdraw.volume_total && selectedWithdraw.volume_total < mount) {
            toastService.warn('No se que mensaje poner')
        }
        this.setState({
            mount
        })
    }

    sendBoletas = async () => {
        const { boletas, mount } = this.state;
        const { toastService, selectedWithdraw, history, cleanWithdraw } = this.props;
        const cant = boletas.results.filter(b => b.checked).length
        if (!cant) {
            toastService.warn('Selecciona al menos una boleta')
        } else {
            try{
                this.setState({
                    sendingBoletas: true
                })
                // const fd = new FormData()
                // fd.append('withdraw', selectedWithdraw.withdraw);
                // fd.append('volume_total', selectedWithdraw.volume_total);
                // fd.append('net_total', mount);
                // boletas.results.forEach(b => {
                //     if (b.checked) {
                //         fd.append('invoices', b.uid);
                //     }
                // })
                let data = {
                    withdraw: selectedWithdraw.withdraw,
                    volume_total: selectedWithdraw.volume_total,
                    net_total: mount
                }

                const checkedBoletas = boletas.results.filter(b => b.checked)
                const uidCheckedBoletas = []
                checkedBoletas.forEach( b => {
                    uidCheckedBoletas.push(b.uid)
                })

                data = {
                    ...data,
                    invoices: uidCheckedBoletas
                }
                await WithdrawService.addTransaction(data)
                await cleanWithdraw()
                toastService.success('Boletas seleccionadas, se le depositará el dinero en unos momentos')
                history.replace(RUTA_RETIROS)
            }catch(error){
                console.log(error);
                this.resetBoletas()
                toastService.error('Error en la transacción, inténtalo más tarde')
                this.setState({
                    sendingBoletas: false
                })
            }
        }
    }

    toggleCheckbox = e => {
        const { boletas } = this.state;
        const newResults = boletas.results.map(b => {
            if (b.uid == e.target.name) {
                b.checked = e.target.checked
            }
            return b
        })
        this.setState({
            boletas: {
                ...this.state.boletas,
                results: [
                    ...newResults
                ]
            }
        }, () => {
            this.getAmmount()
        })
    }

    isLoadingContent = () => {
        const { loading, error, boletas, mount, sendingBoletas } = this.state;
        const { history, toastService, cleanWithdraw, selectedWithdraw } = this.props;

        if (loading) return <LoaderContent />
        else if (error) return <ErrorServer />
        else return <Content
            boletas={boletas}
            mount={mount}
            toggleCheckbox={this.toggleCheckbox}
            cantSelectedBoletas={this.cantSelectedBoletas}
            resetBoletas={this.resetBoletas}
            toastService={toastService}
            sendBoletas={this.sendBoletas}
            sendingBoletas={sendingBoletas}
            history={history}
            selectedWithdraw={selectedWithdraw}
            cleanWithdraw={cleanWithdraw}
            selectWithdraw={selectWithdraw}
        />
    }

    render() {
        const { selectedWithdraw } = this.props;
        if (!selectedWithdraw.selected) {
            return <Redirect to={RUTA_RETIROS} />
        }
        return (
            <div className="WithdrawPage CardsAccountPage">
                {this.isLoadingContent()}
            </div>
        );
    }
}

const mapStateToProps = ({ withdraws: { selectedWithdraw } }) => ({
    selectedWithdraw
})

const mapDisptachToProps = dispatch => ({
    selectWithdraw: payload => dispatch(selectWithdraw(payload)),
    cleanWithdraw: () => dispatch(cleanWithdraw())
})

export default withToastService(connect(
    mapStateToProps,
    mapDisptachToProps
)(SelectBoletasPage));
