import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoaderContent from '../../../components/LoaderContent';
import ErrorServer from '../../../components/ErrorServer/ErrorServer';
import { withToastService } from '../../Toasts/hoc/withToastService';
import Content from '../components/ContentNewWithdraws/';

import { loadAccountsAndCreditCards, selectWithdraw, cleanWithdraw } from "../state/actions";
import './styles.css';

class NewWithdrawPage extends Component {
    async componentDidMount() {
        const { loadAccountsAndCreditCards } = this.props;
        await loadAccountsAndCreditCards()
    }

    isLoadingContent = () => {
        const { loading, error, accountBanks, creditCards, toastService, history, selectWithdraw, cleanWithdraw } = this.props;
        if (loading) return <LoaderContent />
        else if (error) return <ErrorServer />
        else return <Content
            toastService={toastService}
            accountBanks={accountBanks}
            creditCards={creditCards}
            history={history}
            selectWithdraw={selectWithdraw}
            cleanWithdraw={cleanWithdraw}
        />
    }

    render() {
        return (
            <div className="WithdrawPage CardsAccountPage">
                {this.isLoadingContent()}
            </div>
        );
    }
}

const mapStateToProps = ({ withdraws: { accountsAndCardsModule } }) => ({
    loading: accountsAndCardsModule.loading,
    error: accountsAndCardsModule.error,
    accountBanks: accountsAndCardsModule.accountBanks,
    creditCards: accountsAndCardsModule.creditCards
})

const mapDisptachToProps = dispatch => ({
    loadAccountsAndCreditCards: () => dispatch(loadAccountsAndCreditCards()),
    selectWithdraw: payload => dispatch(selectWithdraw(payload)),
    cleanWithdraw: () => dispatch(cleanWithdraw())
})

export default withToastService(connect(
    mapStateToProps,
    mapDisptachToProps
)(NewWithdrawPage));
