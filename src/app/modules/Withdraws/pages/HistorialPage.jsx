import React, { Component } from 'react';
import { connect } from 'react-redux';
import LoaderContent from '../../../components/LoaderContent'
import ErrorServer from '../../../components/ErrorServer/ErrorServer'
import { withToastService } from '../../Toasts/hoc/withToastService'
import Content from '../components/ContentHistorial/'
import { loadTransactions } from "../state/actions"
import './styles.css'

class HistorialPage extends Component {

    async componentDidMount() {
        const { loadTransactions } = this.props
        await loadTransactions()
    }


    isLoadingContent() {
        const { loading, error, transactions } = this.props;
        if (loading) return <LoaderContent />
        if (error) return <ErrorServer />
        else return <Content
            transactions={transactions}
        />
    }

    render() {
        return (
            <div className="WithdrawPage CardsAccountPage">
                {this.isLoadingContent()}
            </div>
        );
    }
}

const mapStateToProps = ({ withdraws: { transactionsModule } }) => ({
    loading: transactionsModule.loading,
    error: transactionsModule.error,
    transactions: transactionsModule.transactions
})

const mapDisptachToProps = dispatch => ({
    loadTransactions: () => dispatch(loadTransactions())
})

export default withToastService(connect(
    mapStateToProps,
    mapDisptachToProps
)(HistorialPage));
