import React from 'react'
import PropTypes from 'prop-types'
import './styles.css'

const HeaderWithdrawPage = ({ titleHeader, iconSrc, button }) => (
    <div className="HeaderWithdrawPage">
        <div className="HeaderWithdrawPage__wrapper">
            <div className="HeaderWithdrawPage__right">
                <img className="HeaderWithdrawPage__img" src={iconSrc} alt="icon_withdraw" />
                <h3 className="HeaderWithdrawPage__title">{titleHeader}</h3>
            </div>
            <div className="HeaderWithdrawPage__left">
                {button}
            </div>
        </div>
    </div>
)

HeaderWithdrawPage.propTypes = {
    titleHeader: PropTypes.string.isRequired,
    iconSrc: PropTypes.string,
    button: PropTypes.element
}

export default HeaderWithdrawPage