import React from 'react'
import PropTypes from 'prop-types'
import { Icon } from '@material-ui/core'
import styled from "styled-components"

const Button = styled.button`
    background-color: #fff;
    color: #0052be;
    text-align: center;
    vertical-align: middle;
    line-height: 1;
    outline: none;
    border: 1px solid #0052be;
    width: ${ props => props.widthButton ? props.widthButton : "100px"};
    border-radius: 10px;
    transition: all .2s;
    padding: 8px 25px;
`

const ButtonWithDraw = ({ title, isActive, onClick, widthButton, type, toggleForm, toggleFormStatus }) => (
    isActive
        ?
        <Button type={type} widthButton={widthButton} onClick={onClick}>
            {
                toggleForm && <Icon
                    className={toggleFormStatus ? "fa fa-chevron-circle-up fa-b-icon" : "fa fa-chevron-circle-down fa-b-icon"}
                    aria-hidden="true">
                </Icon>
            }
            {title}
        </Button>
        :
        null
)

PropTypes.defaultProps = {
    title: "Crear",
    isActive: false,
    type: 'button',
    toggleFormStatus: false
}

PropTypes.propTypes = {
    title: PropTypes.string,
    isActive: PropTypes.bool,
    onClick: PropTypes.func,
    widthButton: PropTypes.string,
    type: PropTypes.string,
    toggleForm: PropTypes.bool
}

export default ButtonWithDraw