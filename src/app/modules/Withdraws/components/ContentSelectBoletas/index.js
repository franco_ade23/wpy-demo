import React, { Component, Fragment } from 'react';
import ButtonWithdraw from '../../components/ButtonWithdraw/'
import HeaderWithdrawPage from '../../components/HeaderWithdrawPage/'
import { Checkbox } from '@material-ui/core';
import iconAccounts from '../../assets/accounts.png'
import { formatAmount } from '../../../../utils/number.util'

class ContentSelectBoletas extends Component {

    componentWillMount() {
        window.scrollTo(0, 0);
    }

    getComision = () => {
        const { selectedWithdraw } = this.props;
        const comision = (selectedWithdraw.volume_total / 10)
        return comision
    }

    body = () => {
        const {
            boletas,
            mount,
            toggleCheckbox,
            cantSelectedBoletas,
            resetBoletas,
            toastService,
            sendBoletas,
            sendingBoletas,
            history,
            selectedWithdraw,
            cleanWithdraw,
            selectWithdraw
        } = this.props;

        if (!boletas.results.length) {
            return <h3 className="box-title text-center">No hay boletas, {boletas.results.length}</h3>
        } else {
            return (
                <Fragment>
                    <div className="box-header">
                        <h3 className="box-title">2. Selecciona las boletas a pagar: </h3>
                        <span className="count">{cantSelectedBoletas(boletas.results)}</span>
                    </div>
                    <div>
                        {
                            boletas.results.map(b => (
                                <div key={b.uid} className="boleta-item">
                                    <div style={{ width: '100%' }} className="mx-0 px-2 px-sm-3">
                                        <div className="row mx-0">
                                            <div className="col-6 col-sm-7 px-2 px-sm-3">
                                                <div className="pt-12">{b.service}</div>
                                            </div>
                                            <div className="col-4 col-sm-3 px-2">
                                                <div className="pt-12">S/ {formatAmount(b.amount)}</div>
                                            </div>
                                            <div className="col-2 text-right px-2 px-sm-3">
                                                <Checkbox
                                                    name={b.uid}
                                                    onChange={e => { toggleCheckbox(e) }}
                                                    checked={b.checked}
                                                    color="primary"
                                                    value={b.uid}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                    <div className="mt-xg">
                        <div className="box-footer">
                            <div>
                                <div className="detail">
                                    <span className="detail_title">Monto a pagar solicitado</span>
                                    <span>S/ {formatAmount(selectedWithdraw.volume_total)}</span>
                                </div>
                                <div className="detail">
                                    <span className="detail_title">Monto en efectivo(por recibir)</span>
                                    <span>S/ {formatAmount(selectedWithdraw.volume_total - this.getComision())}</span>
                                </div>
                                <div className="detail">
                                    <span className="detail_title">Comisión (10%)</span>
                                    <span>S/ {formatAmount(this.getComision())}</span>
                                </div>
                                <div className="detail_efectivo">
                                    <span className="detail_title">Monto a pagar seleccionado</span>
                                    <span>S/ {formatAmount(mount)}</span>
                                </div>
                            </div>
                            <div>
                                <button type="button" className="mb-3 button_clear" onClick={() => { resetBoletas() }}>Resetear</button>
                                <button type="submit" className="d-block button_submit" disabled={sendingBoletas} onClick={e => { sendBoletas(e) }}>{!sendingBoletas ? 'Pagar boletas' : 'Enviando ...'}</button>
                            </div>
                        </div>
                    </div>
                </Fragment>
            )
        }
    }

    render() {
        return (
            <Fragment>
                <section className="WithdrawSection">
                    <HeaderWithdrawPage
                        titleHeader="Boletas Disponibles"
                        iconSrc={iconAccounts}
                        button={
                            <ButtonWithdraw
                                title="Boletas Disponibles Disabled"
                                type="button"
                                isActive={false}
                                onClick={() => { }}
                                widthButton="auto"
                                toggleForm={false}
                            />
                        }
                    />
                    <div className="box">
                        {this.body()}
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default ContentSelectBoletas;
