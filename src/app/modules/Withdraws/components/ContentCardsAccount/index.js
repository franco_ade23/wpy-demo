import React, { Component, Fragment } from 'react';
import ButtonWithdraw from '../../components/ButtonWithdraw/'
import HeaderWithdrawPage from '../../components/HeaderWithdrawPage/'
import FormAccountBank from '../Forms/FormAccountBank/'
import FormCreditCard from '../Forms/FormCreditCard/'
import iconAccounts from '../../assets/accounts.png'
import { WithdrawService } from '../../services/user-withdraws.service'
import { Icon, Tooltip } from '@material-ui/core'
import Modal from '../Modal/DeleteModal'
import './styles.css'

class ContentCardsAccounts extends Component {

    componentWillMount(){
        window.scrollTo(0, 0);
    }

    state = {
        // AccountBank
        toggleAccountForm: false,
        showDeleteModalAB: false,
        selectedABToDelete: null,
        sendingABToDelete: false,

        // Credit Card
        toggleCreditCardForm: false,
        showDeleteModalCC: false,
        selectedCCToDelete: null,
        sendingCCToDelete: false
    }

    componentDidMount() {
        // Waiting Code
    }


    //Logic Account Bank
    showResultsAccountBanks = () => {
        const { accountBanks, banks, toastService } = this.props;
        if (!accountBanks.results.length) {
            return <FormAccountBank
                onSubmit={this.addAcountBank}
                initialValues={banks}
                banks={banks}
                toastService={toastService}
            />
        } else {
            return this.bodyAccountBank()
        }
    }

    bodyAccountBank = () => {
        const { accountBanks, banks, toastService } = this.props;
        const { toggleAccountForm } = this.state;
        const accounts = accountBanks.results
        return (
            <Fragment>
                {
                    toggleAccountForm && <FormAccountBank
                        onSubmit={this.addAcountBank}
                        initialValues={banks}
                        banks={banks}
                        toastService={toastService}
                    />
                }
                <div className="box box-2">
                    <div className="table-responsive">
                        <table className="t">
                            <thead className="t-header">
                                <tr>
                                    <th style={{ minWidth: 90 }}>Banco</th>
                                    <th style={{ minWidth: 120 }} className="text-right">Nº de tarjeta</th>
                                    <th style={{ minWidth: 150 }} className="text-right">Nº de CCI</th>
                                    <th style={{ minWidth: 70 }}></th>
                                </tr>
                            </thead>
                            <tbody className="t-body">
                                {
                                    accounts.length && accounts.map(account => (
                                        <tr key={account.uid}>
                                            <td className="t-border-start"> {account.bank || '-'}</td>
                                            <td className="text-right">{account.bank_acc_number || '-'}</td>
                                            <td className="text-right">{account.bank_acc_cci || '-'}</td>
                                            <td className="text-right fa-global t-border-end">
                                                <div className="fa-container">
                                                    <Tooltip title="Eliminar">
                                                        <Icon
                                                            className="fa fa-trash fa-icon"
                                                            aria-hidden="true"
                                                            onClick={() => {
                                                                this.setState({
                                                                    selectedABToDelete: account,
                                                                    showDeleteModalAB: true
                                                                })
                                                            }}
                                                        />
                                                    </Tooltip>
                                                </div>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </Fragment>
        )
    }

    addAcountBank = async (values, form) => {
        const { loadAccountsAndCreditCards, toastService } = this.props;
        try {
            const results = await WithdrawService.addAccountBank(values)
            if (results) {
                form.reset()
                await loadAccountsAndCreditCards()
                toastService.success('Cuenta de Ahorro creada correctamente')
            }
        } catch (error) {
            console.log(error);
            toastService.error('Problemas al agregar cuenta de ahorro, inténtalo más tarde')
        }
    }

    deleteAccountBank = async () => {
        const { loadAccountsAndCreditCards, toastService } = this.props;
        const { selectedABToDelete } = this.state;
        this.setState({
            sendingABToDelete: true
        })
        try {
            if (selectedABToDelete) {
                await WithdrawService.deleteAccountBank(selectedABToDelete.uid)
                await loadAccountsAndCreditCards()
                toastService.success('Cuenta de Ahorro eliminada correctamente')
            }
        } catch (error) {
            this.setState({
                showDeleteModalAB: false,
                sendingABToDelete: false,
                selectedABToDelete: null
            })
            toastService.error('Problemas al eliminar cuenta de ahorro, inténtalo más tarde')
        }
    }


    //Logic Credit Card
    showResultsCreditCards = () => {
        const { creditCards, banks, brandCards, toastService } = this.props;
        if (!creditCards.results.length) {
            return <FormCreditCard
                onSubmit={this.addCreditCard}
                initialValues={banks}
                banks={banks}
                brandCards={brandCards}
                toastService={toastService}
            />
        } else {
            return this.bodyCreditCard()
        }
    }

    bodyCreditCard = () => {
        const { creditCards, banks, brandCards, toastService } = this.props;
        const { toggleCreditCardForm } = this.state;
        const creditCardsR = creditCards.results
        return (
            <Fragment>
                {
                    toggleCreditCardForm && <FormCreditCard
                        onSubmit={this.addCreditCard}
                        initialValues={banks}
                        banks={banks}
                        brandCards={brandCards}
                        toastService={toastService}
                    />
                }
                <div className="box box-2">
                    <div className="table-responsive">
                        <table className="t">
                            <thead className="t-header">
                                <tr>
                                    <th style={{ minWidth: 90 }}>Banco de tarjeta</th>
                                    <th style={{ minWidth: 110 }}>Marca</th>
                                    <th className="text-right">Últimos 4 dígitos</th>
                                    <th style={{ minWidth: 70 }}></th>
                                </tr>
                            </thead>
                            <tbody className="t-body">
                                {
                                    creditCardsR.length && creditCardsR.map(creditCard => (
                                        <tr key={creditCard.uid}>
                                            <td className="t-border-start">{creditCard.bank.name_short || '-'}</td>
                                            <td>{creditCard.card_service.slug.toUpperCase() || '-'}</td>
                                            <td className="text-right">{creditCard.last_four_numbers ? `***${creditCard.last_four_numbers}` : '-'}</td>
                                            <td className="text-right fa-global t-border-end">
                                                <div className="fa-container">
                                                    <Tooltip title="Eliminar">
                                                        <Icon
                                                            className="fa fa-trash fa-icon"
                                                            style={{ marginTop: 5 }}
                                                            aria-hidden="true"
                                                            onClick={() => {
                                                                this.setState({
                                                                    selectedCCToDelete: creditCard,
                                                                    showDeleteModalCC: true
                                                                })
                                                            }}
                                                        />
                                                    </Tooltip>
                                                </div>
                                            </td>
                                        </tr>
                                    ))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </Fragment>
        )
    }

    addCreditCard = async (values, form) => {
        const { loadAccountsAndCreditCards, toastService } = this.props;
        try {
            const results = await WithdrawService.addCreditCard(values)
            if (results) {
                await loadAccountsAndCreditCards()
                toastService.success('Cuenta de Ahorro creada correctamente')
            }
        } catch (error) {
            console.log(error);
            toastService.error('Problemas al agregar cuenta de ahorro, inténtalo más tarde')
        }
    }

    deleteCreditCard = async () => {
        const { loadAccountsAndCreditCards, toastService } = this.props;
        const { selectedCCToDelete } = this.state;
        this.setState({
            sendingCCToDelete: true
        })
        try {
            if (selectedCCToDelete) {
                await WithdrawService.deleteCreditCard(selectedCCToDelete.uid)
                await loadAccountsAndCreditCards()
                toastService.success('Tarjeta de crédito eliminada correctamente')
            }
        } catch (error) {
            this.setState({
                showDeleteModalCC: false,
                sendingCCToDelete: false,
                selectedCCToDelete: null
            })
            toastService.error('Problemas al eliminar tarjeta de crédito, inténtalo más tarde')
        }
    }

    render() {
        const { creditCards, accountBanks } = this.props;
        const {
            showDeleteModalAB,
            sendingABToDelete,
            showDeleteModalCC,
            sendingCCToDelete,
            toggleAccountForm,
            toggleCreditCardForm
        } = this.state;
        const itemsAccountsBanks = accountBanks.results.length
        const itemsCreditCards = creditCards.results.length

        return (
            <Fragment>
                <section className="WithdrawSection ContentAccountBanks__Section">
                    <HeaderWithdrawPage
                        titleHeader="Cuentas de Ahorro"
                        iconSrc={iconAccounts}
                        button={
                            <ButtonWithdraw
                                title="Agregar cuenta"
                                type="button"
                                isActive={itemsAccountsBanks > 0 ? true : false}
                                onClick={() => { this.setState({ toggleAccountForm: !this.state.toggleAccountForm }) }}
                                widthButton="auto"
                                toggleForm={true}
                                toggleFormStatus={toggleAccountForm}
                            />
                        }
                    />
                    <div>
                        {this.showResultsAccountBanks()}
                    </div>
                </section>
                <section className="WithdrawSection ContentCreditaCards__Section">
                    <HeaderWithdrawPage
                        titleHeader="Tarjetas de Crédito"
                        iconSrc={iconAccounts}
                        button={
                            <ButtonWithdraw
                                title="Agregar tarjeta"
                                type="button"
                                isActive={itemsCreditCards > 0 ? true : false}
                                onClick={() => { this.setState({ toggleCreditCardForm: !this.state.toggleCreditCardForm }) }}
                                widthButton="auto"
                                toggleForm={true}
                                toggleFormStatus={toggleCreditCardForm}
                            />
                        }
                    />
                    <div>
                        {this.showResultsCreditCards()}
                    </div>
                </section>
                <Modal
                    show={showDeleteModalAB}
                    title="Eliminar cuenta bancaria"
                    handleAccept={this.deleteAccountBank}
                    handleClose={() => {
                        this.setState({
                            selectedABToDelete: null,
                            showDeleteModalAB: false
                        })
                    }}
                    onLoading={sendingABToDelete}
                    backdrop="static"
                />
                <Modal
                    show={showDeleteModalCC}
                    title="Eliminar tarjeta de crédito"
                    handleAccept={this.deleteCreditCard}
                    handleClose={() => {
                        this.setState({
                            selectedCCToDelete: null,
                            showDeleteModalCC: false
                        })
                    }}
                    onLoading={sendingCCToDelete}
                    backdrop="static"
                />
            </Fragment>
        );
    }
}

export default ContentCardsAccounts;