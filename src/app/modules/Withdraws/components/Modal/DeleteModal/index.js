import React from 'react';
import Modal from 'react-bootstrap/Modal'
import PropTypes from 'prop-types'
import { Icon } from '@material-ui/core'
import './styles.css'

const DeleteModal = ({
    show,
    handleClose,
    handleAccept,
    keyboard,
    size,
    title,
    description,
    titleAccept,
    titleCancel,
    onLoading,
    backdrop
}) => {
    return (
        <Modal
            show={show}
            onHide={handleClose}
            keyboard={keyboard}
            size={size}
            centered
            backdrop={backdrop}
        >
            <div className="DeleteModal">
                <p className="DeleteModal__Title">
                    <Icon className="fa fa-trash DeleteModal_Icon" aria-hidden="true" />{title}
                </p>
                <p className="DeleteModal__Description">{description}</p>
                <div className="DeleteModal__Options">
                    <button onClick={handleClose} className="DeleteModal__Option DeleteModal__Option--Cancel">{titleCancel}</button>
                    <button onClick={handleAccept} className="DeleteModal__Option DeleteModal__Option--Accept" disabled={onLoading}>
                        {!onLoading ? titleAccept : 'CARGANDO ...'}
                    </button>
                </div>
            </div>
        </Modal>
    );
}

DeleteModal.propTypes = {
    show: PropTypes.bool.isRequired,
    keyboard: PropTypes.bool,
    handleClose: PropTypes.func,
    handleAccept: PropTypes.func,
    size: PropTypes.string,
    title: PropTypes.string,
    description: PropTypes.string,
    titleAccept: PropTypes.string,
    titleCancel: PropTypes.string,
    onLoading: PropTypes.bool
}

DeleteModal.defaultProps = {
    size: 'md',
    keyboard: false,
    title: 'Eliminar item',
    description: 'Estas seguro de continuar?',
    titleCancel: 'Cancelar',
    titleAccept: 'Aceptar',
    backdrop: true
}

export default DeleteModal;