import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom'
import ButtonWithdraw from '../../components/ButtonWithdraw/'
import HeaderWithdrawPage from '../../components/HeaderWithdrawPage/'
import FormNewWithdraw from '../Forms/FormNewWithdraw/'
import iconAccounts from '../../assets/accounts.png'
import { RUTA_TARJETAS_CUENTAS, RUTA_RETIROS_BOLETAS } from '../../../../constants/routes.constants'
import { WithdrawService } from '../../services/user-withdraws.service'
import moment from 'moment'
moment.locale('es')

class ContentNewWithdraws extends Component {

    componentWillMount() {
        window.scrollTo(0, 0);
    }

    createWithdraw = async (values, form) => {
        const { toastService, history, selectWithdraw } = this.props;
        const data = { ...values }

        // Temporal
        const dataNow = moment().format('YYYY-MM-DD')
        data.credit_line = 100000
        data.limit_date = dataNow

        // console.log(data);
        
        try {
            const results = await WithdrawService.addWithdraw(data)
            

            if (results) {
                form.reset()
                const selectedWithdraw = {
                    selected: true,
                    withdraw: results.uid,
                    volumeTotal: results.requested_amount,
                }
                await selectWithdraw(selectedWithdraw)
                toastService.success('Retiro creado correctamente')
                history.replace(RUTA_RETIROS_BOLETAS)
            }
        } catch{
            form.reset()
            toastService.error('Error al crear un retiro')
        }
    }

    render() {
        const { toastService, accountBanks, creditCards } = this.props;
        if (!accountBanks.results.length || !creditCards.results.length) {
            return <Redirect to={`${RUTA_TARJETAS_CUENTAS}`} />
        }
        return (
            <Fragment>
                <section className="WithdrawSection">
                    <HeaderWithdrawPage
                        titleHeader="Nuevo Retiro"
                        iconSrc={iconAccounts}
                        button={
                            <ButtonWithdraw
                                title="Agregar cuenta"
                                type="button"
                                isActive={false}
                                onClick={() => { }}
                                widthButton="auto"
                                toggleForm={false}
                            />
                        }
                    />
                    <FormNewWithdraw
                        onSubmit={this.createWithdraw}
                        toastService={toastService}
                        accountBanks={accountBanks}
                        creditCards={creditCards}
                    />
                </section>
            </Fragment>
        );
    }
}

export default ContentNewWithdraws;
