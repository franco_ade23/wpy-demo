import React, { Component, Fragment } from 'react';
import ButtonWithdraw from '../../components/ButtonWithdraw/';
import HeaderWithdrawPage from '../../components/HeaderWithdrawPage/';
import NotFoundItems from '../../../../components/NotFoundItems/NotFoundItems'
import iconAccounts from '../../assets/accounts.png';
import { RUTA_RETIROS_NEW } from '../../../../constants/routes.constants'
import CardExpansionWithdraw from '../CardExpansionWithdraw/'

class ContentWithdraws extends Component {

    componentWillMount() {
        window.scrollTo(0, 0);
    }

    listWithdraws = () => {
        const { withdraws, selectWithdraw, history } = this.props;
        return (
            <div>
                {
                    withdraws.results.length ?
                        withdraws.results.map(w => (
                            <CardExpansionWithdraw key={w.uid} data={w} selectWithdraw={selectWithdraw} history={history}/>
                        )) :
                        <NotFoundItems message="No hay retiros" />
                }
            </div>
        )
    }

    render() {
        const { history } = this.props;
        return (
            <Fragment>
                <section className="WithdrawSection">
                    <HeaderWithdrawPage
                        titleHeader="Retiros"
                        iconSrc={iconAccounts}
                        button={
                            <ButtonWithdraw
                                title="Agregar retiro"
                                type="button"
                                isActive={true}
                                onClick={() => { history.push(RUTA_RETIROS_NEW) }}
                                widthButton="auto"
                                toggleForm={false}
                            />
                        }
                    />
                    <div>
                        {/* <p className="subtitle-c">Retiros Publicados</p> */}
                        {this.listWithdraws()}
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default ContentWithdraws;
