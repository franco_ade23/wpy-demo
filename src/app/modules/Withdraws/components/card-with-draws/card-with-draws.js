import React from 'react';
import TagStatus from '../../../../components/tag-status/tag-status';

const CardWithDraws = () => (
    <div className='bg-white tw-rounded-lg'>
        <div className='tw-flex tw-py-4 tw-px-8'>
            <div>
                <h3 className='tw-uppercase tw-leading-none'>Visa</h3>
                <h5 className='text-uppercase tw-leading-none'>cencosud - <span>02/10/2018</span></h5>
                <p>Buscando boletas que se acerquen al monto que necesitas</p>
            </div>
            <div>
                <h2>S/ 1,800</h2>
                <TagStatus />
            </div>
        </div>
    </div>
)
export default CardWithDraws;