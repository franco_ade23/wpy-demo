import React, { Component, Fragment } from 'react';
import ButtonWithdraw from '../../components/ButtonWithdraw/'
import HeaderWithdrawPage from '../../components/HeaderWithdrawPage/'
import NotFoundItems from '../../../../components/NotFoundItems/NotFoundItems'
import iconAccounts from '../../assets/accounts.png'

export class ContentHistorial extends Component {

    componentWillMount() {
        window.scrollTo(0, 0);
    }

    listTransactions = () => {
        const { transactions } = this.props;
        return (
            <div>
                {
                    transactions.results.length ? 
                        <p>hola</p>:
                        <NotFoundItems
                            message = "No hay transacciones"
                        />
                }
            </div>
        )
    }

    render() {
        return (
            <Fragment>
                <section className="WithdrawSection">
                    <HeaderWithdrawPage
                        titleHeader="Historial"
                        iconSrc={iconAccounts}
                        button={
                            <ButtonWithdraw
                                title="Agregar cuenta"
                                type="button"
                                isActive={false}
                                onClick={() => { }}
                                widthButton="auto"
                                toggleForm={false}
                            />
                        }
                    />
                    <div>
                        {/* <p className="subtitle-c">Retiros Publicados</p> */}
                        {this.listTransactions()}
                    </div>
                </section>
            </Fragment>
        );
    }
}

export default ContentHistorial;
