import React, { Component } from 'react'
import { Form, Field } from "react-final-form";
import { validateForm, required } from "redux-form-validators";
import MaterialInput from '../../../../../components/MaterialInput'
import MaterialSelect from '../../../../../components/MaterialSelect'
import MenuItem from "@material-ui/core/MenuItem";
import '../../styles.css'
import '../styles.css'

const validate = validateForm({
    bank: [required()],
    // bank_acc_cci: [required()],
    // service: [exclusion({ in: [SELECT_DEFAULT_VALUE] })],
    bank_acc_number: [required()]
});

class FormAccountBank extends Component {
    listBanks = () => {
        const { banks } = this.props;
        return banks.map(bank => (
            <MenuItem value={bank.id} key={bank.id}>
                {bank.name_short}
            </MenuItem>
        ));
    }

    renderForm = ({ valid, handleSubmit, onSubmitTry, submitting }) => {
        const { banks, toastService } = this.props;

        const _handleSumit = (...args) => {
            args[0].nativeEvent.preventDefault();
            if (!valid) {
                toastService.warn('Completa correctamente los datos del formulario')
            } else {
                handleSubmit(...args)
            }
        }

        return (
            <div className="box">
                <form onSubmit={_handleSumit} className="FormWithdraw">
                    <h3 className="FormWithdraw__title">¿A qué cuentas de ahorros quieres que te depositemos?</h3>
                    <div className="form-row justify-content-between">
                        <div className="mb-4 mb-sm-0 col-md-3">
                            <div>
                                <label className="mb-1 mb-sm-2">
                                    Banco a depositar:
                                </label>
                                <Field
                                    component={MaterialSelect}
                                    name="bank"
                                    initialValue={banks[0] && banks[0].id}
                                    placeholder="Banco"
                                    renderOptions={this.listBanks}
                                />
                            </div>
                        </div>
                        <div className="mb-4 mb-sm-0 col-md-3">
                            <div>
                                <label className="mb-1 mb-sm-2">
                                    Número de cuenta:
                                </label>
                                <Field
                                    component={MaterialInput}
                                    name="bank_acc_number"
                                    type="text"
                                    className="fill-width"
                                    placeholder=""
                                />
                            </div>
                        </div>
                        <div className="mb-2 mb-sm-0 col-md-3">
                            <div>
                                <label className="mb-1 mb-sm-2">
                                    CCI:
                                </label>
                                <Field
                                    component={MaterialInput}
                                    name="bank_acc_cci"
                                    type="text"
                                    className="fill-width"
                                    placeholder=""
                                />
                            </div>
                        </div>
                    </div>
                    <div className="mt-4">
                        <button className="button_submit" type="submit" disable={`${submitting}`}>{submitting ? "Cargando.." : 'Crear cuenta'}</button>
                    </div>
                </form>
            </div>
        )
    }

    // handleSubmit = (values, form) => {
    //     form.reset()
    //     console.log(values);
    //     console.log('dd');
    // }

    render() {
        const { onSubmit } = this.props;
        return (
            <Form
                onSubmit={onSubmit}
                validate={validate}
                render={this.renderForm}
            />
        );
    }
}

export default FormAccountBank