import React, { Component } from 'react';
import { Form, Field } from "react-final-form";
import { validateForm, required, length } from "redux-form-validators";
import MaterialSelect from '../../../../../components/MaterialSelect'
import MaterialInput from '../../../../../components/MaterialInput'
import MenuItem from "@material-ui/core/MenuItem";
import { formatAmount } from '../../../../../utils/number.util';
import '../../styles.css';
import '../styles.css';

const MIN_DIGITS = 3;
const MAX_DIGITS = 7;
const PERCENT_COMISSION = 10

const validate = validateForm({
    payment_card: [required()],
    bank_account: [required()],
    // credit_line: [required()],
    requested_amount: [
        required(),
        // numericality({
        //     int: true,
        // }),
        length({ min: MIN_DIGITS, max: MAX_DIGITS })
    ],
    effective_amount: [
        required(),
        // numericality({
        //     int: true,
        // }),
        length({ min: MIN_DIGITS, max: MAX_DIGITS })
    ],
    // limit_date: [required()],
});

class FormNewWithdraw extends Component {

    state = {
        requested_amount: null,
        effective_amount: null,
        localAmountUser: null,
        localComision: null,
        localAmountPayment: null
    }

    listCreditCards = () => {
        const { creditCards } = this.props;
        return creditCards.results && creditCards.results.map(c => (
            <MenuItem value={c.uid} key={c.uid}>
                {c.bank.name_short} - {(c.card_service.slug).toUpperCase()} - ****{c.last_four_numbers}
            </MenuItem>
        ))
    }

    listAccountBanks = () => {
        const { accountBanks } = this.props;
        return accountBanks.results && accountBanks.results.map(ab => (
            <MenuItem value={ab.uid} key={ab.uid}>
                {ab.bank} - {ab.bank_acc_number}
            </MenuItem>
        ))
    }

    onChange1 = e => {
        const value = e.target.value
        this.setState({
            requested_amount: Number(Number(e.target.value).toFixed(2))
        }, () => {
            if (value.length >= MIN_DIGITS && value.length <= MAX_DIGITS) {
                const newValue = Number(value)
                const comision = Number(Number(newValue * PERCENT_COMISSION / 100).toFixed(2))
                const monto = newValue - comision
                this.setState({
                    effective_amount: Number(monto.toFixed(2)),
                    localAmountUser: Number(monto.toFixed(2)),
                    localComision: comision,
                    localAmountPayment: newValue,
                })
            } else {
                this.setState({
                    effective_amount: null,
                    localAmountUser: null,
                    localComision: null,
                    localAmountPayment: null,
                })
            }
        })
    }

    onChange2 = e => {
        const value = e.target.value
        this.setState({
            effective_amount: Number(Number(value).toFixed(2))
        }, () => {
            if (value.length >= MIN_DIGITS && value.length <= MAX_DIGITS) {
                const newValue = Number(value)
                const monto = Number((newValue / ((100 - PERCENT_COMISSION) / 100)).toFixed(2))
                this.setState({
                    requested_amount: monto,
                    localAmountUser: newValue,
                    localComision: Number((monto - newValue).toFixed(2)),
                    localAmountPayment: monto
                })
            } else {
                this.setState({
                    requested_amount: null,
                    localAmountUser: null,
                    localComision: null,
                    localAmountPayment: null,
                })
            }
        })
    }

    getUserMoney = () => {
        const { localAmountUser } = this.state;
        try {
            if (localAmountUser) {
                return `S/ ${formatAmount(localAmountUser)}`
            } else {
                return '-'
            }
        } catch{
            return 'Error'
        }
    }

    getComision = () => {
        const { localComision } = this.state;
        try {
            if (localComision) {
                return `S/ ${formatAmount(localComision)}`
            }
            else {
                return '-'
            }
        } catch{
            return 'Error'
        }
    }

    getTotalAmmount = () => {
        const { localAmountPayment } = this.state;
        try {
            if (localAmountPayment) {
                return `S/ ${formatAmount(localAmountPayment)}`
            } else {
                return '-'
            }
        } catch{
            return 'Error'
        }
    }

    renderForm = ({ valid, handleSubmit, onSubmitTry, submitting }) => {
        const { effective_amount, requested_amount } = this.state;
        const { toastService, accountBanks, creditCards } = this.props;
        const _handleSumit = (...args) => {
            args[0].nativeEvent.preventDefault();
            if (!valid) {
                toastService.warn('Completa correctamente los datos del formulario')
            } else {
                handleSubmit(...args)
            }
        }

        return (
            <div className="box">
                <form onSubmit={_handleSumit} className="FormWithdraw">
                    <h3 className="FormWithdraw__title">Paso 1: Completa los datos para realizar un nuevo retiro</h3>
                    <div className="form-row form-row-2">
                        <div className="col-sm-5 margin-rf">
                            <div className="mb-4">
                                <label className="mb-1 mb-sm-2">
                                    Tarjeta de crédito
                                </label>
                                <Field
                                    component={MaterialSelect}
                                    name="payment_card"
                                    initialValue={creditCards.results[0] && creditCards.results[0].uid}
                                    placeholder="Tarjeta de crédito"
                                    renderOptions={this.listCreditCards}
                                />
                            </div>
                            <div className="mb-4">
                                <label className="mb-1 mb-sm-2">
                                    Cuenta de ahorro
                                </label>
                                <Field
                                    component={MaterialSelect}
                                    name="bank_account"
                                    initialValue={accountBanks.results[0] && accountBanks.results[0].uid}
                                    placeholder="Cuenta de Ahorro"
                                    renderOptions={this.listAccountBanks}
                                />
                            </div>
                        </div>
                        <div className="col-sm-5">
                            <div className="mb-4">
                                <label className="mb-1 mb-sm-2">
                                    Saldo disponible
                                </label>
                                <Field
                                    component={MaterialInput}
                                    name="requested_amount"
                                    type="number"
                                    initialValue={requested_amount}
                                    value={requested_amount}
                                    onChange={e => { this.onChange1(e) }}
                                    className="fill-width"
                                    placeholder="1800"
                                />
                                <label className="s-message">¿Este es el saldo?</label>
                            </div>
                            <div className="mb-3">
                                <label className="mb-1 mb-sm-2">
                                    Monto en efectivo
                                </label>
                                <Field
                                    component={MaterialInput}
                                    name="effective_amount"
                                    type="number"
                                    initialValue={effective_amount}
                                    value={effective_amount}
                                    onChange={e => { this.onChange2(e) }}
                                    className="fill-width"
                                    placeholder="1800"
                                />
                                <label className="s-message">¿Cuánto quieres que te depositemos?</label>
                            </div>
                            <div className="mt-g">
                                <div><span className="data-info">Te depositaremos: </span><span>{this.getUserMoney()}</span></div>
                                <div><span className="data-info">Comisión (10%):</span><span>{this.getComision()}</span></div>
                                <div><span className="data-info">Monto a pagar: </span><span>{this.getTotalAmmount()}</span></div>
                            </div>
                        </div>
                    </div>
                    <div className="text-right mt-xg">
                        <button className="button_submit" type="submit" disabled={submitting}>{submitting ? "Cargando.." : 'Crear retiro'}</button>
                    </div>
                </form>
            </div>
        )
    }

    render() {
        const { onSubmit } = this.props;
        return (
            <Form
                onSubmit={onSubmit}
                validate={validate}
                render={this.renderForm}
            />
        );
    }
}

export default FormNewWithdraw;
