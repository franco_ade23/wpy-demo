import React, { Component } from 'react';
import { Form, Field } from "react-final-form";
import { validateForm, required, length } from "redux-form-validators";
import MaterialSelect from '../../../../../components/MaterialSelect'
import MaterialInput from '../../../../../components/MaterialInput'
import MenuItem from "@material-ui/core/MenuItem";
import '../../styles.css';
import '../styles.css';


const validate = validateForm({
    bank: [required()],
    card_service: [required()],
    last_four_numbers: [required(), length({ max: 4, min: 4 })],
});

class FormCreditCard extends Component {

    listBanks = () => {
        const { banks } = this.props;
        return banks.map(bank => (
            <MenuItem value={bank.id} key={bank.id}>
                {bank.name_short}
            </MenuItem>
        ));
    }

    listbrandCards = () => {
        const { brandCards } = this.props;
        return brandCards.map(brandCard => (
            <MenuItem value={brandCard.id} key={brandCard.id}>
                {brandCard.name}
            </MenuItem>
        ))
    }

    renderForm = ({ valid, handleSubmit, onSubmitTry, submitting }) => {
        const { banks, brandCards, toastService } = this.props;

        const _handleSumit = (...args) => {
            args[0].nativeEvent.preventDefault();
            if (!valid) {
                toastService.warn('Completa correctamente los datos del formulario')
            } else {
                handleSubmit(...args)
            }
        }

        return (
            <div className="box">
                <form onSubmit={_handleSumit} className="FormWithdraw">
                    <h3 className="FormWithdraw__title">¿Con que tárjeta realizarás la operación?</h3>
                    <div className="form-row justify-content-between">
                        <div className="mb-4 mb-sm-0 col-md-3">
                            <div>
                                <label className="mb-1 mb-sm-2">
                                    Banco de tarjeta
                                </label>
                                <Field
                                    component={MaterialSelect}
                                    name="bank"
                                    initialValue={banks[0] && banks[0].id}
                                    placeholder="Banco de tarjeta"
                                    renderOptions={this.listBanks}
                                />
                            </div>
                        </div>
                        <div className="mb-4 mb-sm-0 col-md-3">
                            <label className="mb-1 mb-sm-2">
                                Marca de tarjeta
                            </label>
                            <Field
                                component={MaterialSelect}
                                name="card_service"
                                initialValue={brandCards[0] && brandCards[0].id}
                                placeholder="Marca de tarjeta"
                                renderOptions={this.listbrandCards}
                            />
                        </div>
                        <div className="mb-2 mb-sm-0 col-md-3">
                            <label className="mb-1 mb-sm-2">
                                Últimos 4 dígitos
                            </label>
                            <Field
                                component={MaterialInput}
                                name="last_four_numbers"
                                type="number"
                                className="fill-width"
                                placeholder="6709"
                            />
                        </div>
                    </div>
                    <div className="mt-4">
                        <button className="button_submit" type="submit" disable={`${submitting}`}>{submitting ? "Cargando.." : 'Crear tarjeta'}</button>
                    </div>
                </form>
            </div>
        )
    }

    render() {
        const { onSubmit } = this.props;
        return (
            <Form
                onSubmit={onSubmit}
                validate={validate}
                render={this.renderForm}
            />
        );
    }
}

export default FormCreditCard;
