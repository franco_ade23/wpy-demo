import React, { Component } from 'react';
import AppIcon from '../../../../components/Icon'
import { StatusHuman, PENDING, APPROVED } from '../../../../constants/status.constants'
import { formatAmount } from "./../../../../utils/number.util";
import './style.css'
import PENDING_STATUS_ICON from '../../../../../assets/icons/pending-status.png'
import APPROVED_STATUS_ICON from '../../../../../assets/icons/pending-status.png'
import { RUTA_RETIROS_BOLETAS } from '../../../../constants/routes.constants'
import moment from 'moment'
moment.locale('es')

class CardExpansionWithdraw extends Component {

    state = {
        expanded: false
    }

    goToSelectBoletas = async (uid, reqAmount) => {
        const { selectWithdraw, history } = this.props
        await selectWithdraw({
            selected: true,
            withdraw: uid,
            volumeTotal: reqAmount,
        })
        history.push(RUTA_RETIROS_BOLETAS)
    }

    getText = (uid = null, reqAmount = null) => {
        const { data } = this.props;
        const status = data.status
        if (status == PENDING) {
            return (
                <div className="Card_Expansion__msg">
                    <span>Tiene que seleccionar boletas para continuar con la transacción.</span>
                    <button type="button" onClick={() => { this.goToSelectBoletas(uid, reqAmount) }} className="Card_Expansion__btn">Seleccionar boletas</button>
                </div>
            )
        }
    }

    getStatus = () => {
        const { data } = this.props;
        const status = data.status
        if (status == PENDING) {
            return (
                <div className="Card_Expansion__status" style={{ color: StatusHuman.PENDING.color }}>
                    <span>{StatusHuman.PENDING.text}</span>
                    <img src={PENDING_STATUS_ICON} />
                </div>
            )
        } else if (status == APPROVED) {
            return (
                <div className="Card_Expansion__status" style={{ color: StatusHuman.APPROVED.color }}>
                    <span>{StatusHuman.APPROVED.text}</span>
                    <img src={PENDING_STATUS_ICON} />
                </div>
            )
        }
    }

    formatDate = date => {
        const p = moment(date)
        if (p.isValid()) {
            return p.format('DD/MM/YYYY')
        } else {
            return null
        }
    }

    render() {
        const { expanded } = this.state;
        const { data } = this.props;
        return (
            <div className="Card_Expansion" onClick={() => { this.setState({ expanded: !this.state.expanded }) }}>
                <div className="mx-0 px-0">
                    <div className="row mx-0 align-items-center">
                        <div className="col-12 col-sm-8">
                            <div className="d-flex">
                                {/* <div className="arrow-nav">
                                    <AppIcon
                                        icon="arrow_down"
                                        height={10}
                                        style={{
                                            transform: expanded ? "rotate(180deg)" : null
                                        }}
                                    ></AppIcon>
                                </div> */}
                                <div className="ml-0 ml-sm-3 mb-3 mb-sm-0">
                                    <p className="Card_Expansion__cardservice">{data.payment_card.card_service || '-'}</p>
                                    <p className="Card_Expansion__info">{data.payment_card.bank || '-'} - <span className="Card_Expansion__date">{this.formatDate(data.created_at)}</span></p>
                                    {this.getText(data.uid, data.requested_amount)}
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-sm-4">
                            <div className="d-flex justify-content-between">
                                <div className="Card_Expansion__requested_amount">
                                    S/ {formatAmount(data.requested_amount)}
                                </div>
                                {this.getStatus()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CardExpansionWithdraw;
