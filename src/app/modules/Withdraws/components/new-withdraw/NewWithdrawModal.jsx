import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import NewWithdraw from './NewWithdraw';

const NewWithdrawModal = ({ open, onClose }) => {
    return (
        <Dialog
        open={open}
        onClose={onClose}
        className="wpy-dialog"
        maxWidth={false}>
            <DialogContent
            className="wpy-dialog__content">
                <NewWithdraw
                onFinish={onClose}></NewWithdraw>
            </DialogContent>
        </Dialog>
    );
}

export default NewWithdrawModal;