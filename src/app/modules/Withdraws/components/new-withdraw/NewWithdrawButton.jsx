import React, { Fragment, useState } from 'react';
import NewWithdrawModal from './NewWithdrawModal';

const NewWithdrawButton = ({ children }) => {

    const [ openModal, setOpenModal ] = useState(false);

    const onCloseModal = ( ) => {
        setOpenModal(false);
    }

    const onNewWithdrawBtnClick = () => {
        setOpenModal(true);
    }

    return (
        <Fragment>
            <button className="wpy-btn"
            onClick={onNewWithdrawBtnClick}>{ children }</button>
            <NewWithdrawModal
            open={openModal}
            onClose={onCloseModal}></NewWithdrawModal>
        </Fragment>
    )
}

export default NewWithdrawButton;