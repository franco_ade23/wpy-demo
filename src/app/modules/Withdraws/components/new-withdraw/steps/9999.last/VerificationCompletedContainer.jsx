import React from 'react';
import VerificationCompleted from './VerificationCompleted';

const VerificationCompletedContainer = ({ onCheckWithdraws, withdrawInfo }) => {
    return (
        <VerificationCompleted
        onCheckWithdraws={onCheckWithdraws}
        withdrawInfo={withdrawInfo}></VerificationCompleted>
    );
}

export default VerificationCompletedContainer;