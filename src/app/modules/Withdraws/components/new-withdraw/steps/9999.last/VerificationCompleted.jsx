import React from 'react';
import styled from 'styled-components';

const StyledVerificationCompleted = styled.div`
    text-align: center;
    padding: 0 80px;
    img{
        width: 120px;
    }
`;

const VerificationCompleted = ({ onCheckWithdraws, withdrawInfo }) => {

    const formattedWithdrawAmount = String(withdrawInfo.withdrawAmount).replace(/(?!^)(?=(?:\d{3})+(?:\.|$))/gm, '.');
    
    return (
        <StyledVerificationCompleted>
            <img src="/images/withdraws/verification-completed-icon.png" alt=""/>
            <div className="spacer-30"></div>
            <h1 className="text-center">
                <b>¡Verificación completada!</b>
            </h1>
            <p>
                Verificaremos tu operación en unos intantes<br/>
                y en unos minutos te estaremos transfiriendo<br/>
                <b>PEN {formattedWithdrawAmount}</b> a tu cuenta
            </p>
            <div className="spacer-30"></div>
            <p className="text-primary">
                Enviaremos una notificacion a tu correo tan pronto<br/>
                realizemos el deposito, o puedes revisar tu<br/>
                estado de operacion en tus retiros recientes
            </p>
            <div className="spacer-30"></div>
            <div className="flex-row-layout jc-center">
                <button className="wpy-btn wpy-btn--primary wpy-btn--rounded"
                onClick={onCheckWithdraws}>
                    Ver mis retiros
                </button>
            </div>
        </StyledVerificationCompleted>
    );
}

export default VerificationCompleted;