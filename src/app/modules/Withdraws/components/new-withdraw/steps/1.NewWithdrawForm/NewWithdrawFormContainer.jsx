import React from 'react';
import NewWithdrawForm from './NewWithdrawForm';
import { validateForm, required } from 'redux-form-validators';

const NewWithdrawFormContainer = ({ onContinue }) => {

    const validateFn = validateForm({
        availableCredit: [ required() ],
        withdrawAmount: [ required() ],
        bank: [ required() ],
        cardType: [ required() ],
        cardLastDigits: [ required() ]
    });

    const handleSubmit = (values) => {
        onContinue(2,values);
    };

    return (
        <NewWithdrawForm
        onSubmit={handleSubmit}
        validate={validateFn}></NewWithdrawForm>
    );
}

export default NewWithdrawFormContainer;