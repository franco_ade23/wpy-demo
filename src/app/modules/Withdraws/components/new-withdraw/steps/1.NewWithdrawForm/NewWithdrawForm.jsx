import React from 'react';
import styled from 'styled-components';
import { Form, Field } from 'react-final-form';
import MaterialInput from '../../../../../../components/MaterialInput';

import InputAdornment from '@material-ui/core/InputAdornment';
import { colors } from '../../../../../../../styles/js/1-settings/colors';

const StyledNewWithdrawForm = styled.div`
    .newWithdrawForm{
        &__row{
            display: flex;
        }
        &__fc{
            padding: 20px 35px;
            flex: 0 0 50%;
            &:first-child{
                padding-left: 0px;
            }
            &:last-child{
                padding-right: 0px;
            }
            label{
                font-weight: 500;
                color: #545454;
                position: relative;
                &::before{
                    content: '\u2022';
                    position: absolute;
                    color: ${colors.grey4};
                    left: -12px;
                    top: 50%;
                    transform: translateY(-50%);
                }
            }
        }
    }
`;

const renderForm = ({ handleSubmit, valid, loading }) => {

    const inputsProps = {
        availableCredit: {
            startAdornment: <InputAdornment position="start">
                <span className="input-prefix">S/</span>
            </InputAdornment>
        },
        withdrawAmount: {
            startAdornment: <InputAdornment position="start">
                <span className="input-prefix">S/</span>
            </InputAdornment>
        }
    }

    return (
        <form className="" onSubmit={handleSubmit}>
            <h2 className="text-primary"><b>Nuevo Retiro</b></h2>
            <div className="newWithdrawForm__row">
                <div className="newWithdrawForm__fc">
                    <label htmlFor="">Línea de crédito disponible</label>
                    <Field component={MaterialInput} name="availableCredit"
                    type="number" className="fill-width" placeholder="1800"
                    InputProps={ inputsProps.availableCredit }></Field>
                    {/* <Field component={MaterialSelect} name="availableCredit"
                    type="number" className="fill-width" placeholder="1800"
                    InputProps={ inputsProps.availableCredit }></Field>   */}
                </div>
                <div className="newWithdrawForm__fc">
                    <label htmlFor="">Monto a retirar</label>
                    <Field component={MaterialInput} name="withdrawAmount"
                    type="number" className="fill-width" placeholder="1500"
                    InputProps={ inputsProps.availableCredit }></Field>
                </div>
            </div>
            <div className="newWithdrawForm__row">
                <div className="newWithdrawForm__fc">
                    <label htmlFor="">Banco</label>
                    <Field component={MaterialInput} name="bank" className="fill-width" 
                    placeholder="Banco de crédito del Perú"></Field>
                </div>
                <div className="newWithdrawForm__fc">
                    <label htmlFor="">Tipo de tarjeta</label>
                    <Field component={MaterialInput} name="cardType"className="fill-width" 
                    placeholder="Visa" InputProps={ inputsProps.cardType }></Field>
                </div>
            </div>
            <div className="newWithdrawForm__row">
                <div className="newWithdrawForm__fc">
                    <label htmlFor="">Nro de cuenta</label>
                    <Field component={MaterialInput} name="cardLastDigits"
                    className="fill-width" placeholder="1554 5544 6658 4889"></Field>
                </div>
                <div className="newWithdrawForm__fc flex-col-layout jc-flex-end">
                    <button className="wpy-btn wpy-btn--primary wpy-btn--rounded fill-width">
                        Publicar
                    </button>
                </div>
            </div>
        </form>
    );
}

const NewWithdrawForm = ({onSubmit, validate, loading}) => {
    return (
        <StyledNewWithdrawForm>
            <Form
            onSubmit={onSubmit}
            validate={validate}
            loading={loading}
            render={renderForm}></Form>            
        </StyledNewWithdrawForm>        
    );
}

export default NewWithdrawForm;