import React from 'react';
import styled from 'styled-components';
import { Form, Field } from 'react-final-form';
import MenuItem from '@material-ui/core/MenuItem';

import PhotoInput from '../../../../../../components/PhotoInput';
import MaterialSelect from '../../../../../../components/MaterialSelect';
import { SELECT_DEFAULT_VALUE } from '../../../../../../constants/forms.contstants';
import MaterialInput from '../../../../../../components/MaterialInput';

const StyledPaymentVerificationForm = styled.form`
    .pvf{
        &__row{
            display: flex;
        }
        &__fc{
            padding: 20px 35px;     
            label{
                margin-bottom: 15px;
                display: block;
                text-align: center;
            }
            flex: 0 0 50%;
            &:first-child{
                padding-left: 0px;
            }
            &:last-child{
                padding-right: 0px;
            }
        }
    }
`;

const renderForm = ({ handleSubmit, valid, values }) => {

    const banks = [
        { id: 1, name: 'BCP' }
    ];

    const renderBanks = () => {
        return banks.map( (b,i) => <MenuItem key={i} value={b.id}>{b.name}</MenuItem> )
    }

    return (
        <StyledPaymentVerificationForm onSubmit={handleSubmit}>
            <h2 className="text-primary text-center"><b>Verificación de pago</b></h2>
            <div className="pvf__row">
                <div className="pvf__fc">
                    <Field component={PhotoInput} name="cardPhoto"
                    text="Foto de la tarjeta mostrando solo los últimos 4 dígitos del número"></Field>
                </div>
                <div className="pvf__fc">
                    <Field component={PhotoInput} name="dniPhoto"
                    text="Foto de tu DNI"></Field>
                </div>
            </div>
            <div className="spacer-20"></div>
            <p className="text-center">
                Ingresa la cuenta bancaria donde recibirás el depósito
            </p>
            <div className="pvf__row">
                <div className="pvf__fc">
                    <Field component={MaterialSelect} name="bank"
                    initialValue={SELECT_DEFAULT_VALUE}
                    placeholder="Banco" renderOptions={renderBanks}></Field>
                </div>
                <div className="pvf__fc">
                    <Field component={MaterialInput} name="accountNumber"
                    placeholder="Nro de Cuenta"></Field>
                </div>
            </div>
            <div className="flex-row-layout jc-center">
                <button className="wpy-btn wpy-btn--primary wpy-btn--rounded">
                    Siguiente
                </button>
            </div>
        </StyledPaymentVerificationForm>
    );
}

const PaymentVerificationForm = ({ onSubmit, validate }) => {
    return (
        <Form
        onSubmit={onSubmit}
        validate={validate}
        render={renderForm}></Form>
    );
}

export default PaymentVerificationForm;