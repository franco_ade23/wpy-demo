import React from 'react';
import PaymentVerificationForm from './PaymentVerificationForm';
import { validateForm, required, exclusion } from 'redux-form-validators';

import { SELECT_DEFAULT_VALUE } from '../../../../../../constants/forms.contstants';

const PaymentVerificationFormContainer = ({ onContinue }) => {

    const validate = validateForm({
        cardPhoto: [ required() ],
        dniPhoto: [ required() ],
        bank: [ exclusion({ in: [ SELECT_DEFAULT_VALUE ] }) ],
        accountNumber: [ required() ]
    })

    const handleSubmit = (values) => {
        onContinue(3,values);
    }

    return (
        <PaymentVerificationForm
        onSubmit={handleSubmit}
        validate={validate}></PaymentVerificationForm>
    );
}

export default PaymentVerificationFormContainer;