import React, { useState } from 'react';
import NewWithdrawForm from './steps/1.NewWithdrawForm';
import PaymentVerificationForm from './steps/2.PaymentVerification';
import VerificationCompleted from './steps/9999.last';

const componentsPerSteps = [
    {
        stepNumber: 1,
        cmp: NewWithdrawForm
    },
    {
        stepNumber: 2,
        cmp: PaymentVerificationForm
    },
    {
        stepNumber: 3,
        cmp: VerificationCompleted
    }
]

const NewWithdraw = ({ onFinish }) => {
    
    const [ { currentStep, newWithdrawInfo }, setState ] = useState({ currentStep: 1, newWithdrawInfo: {} });

    const handleContinue = ( nextStepNumber, withdrawInfo ) => {
        setState({
            currentStep: nextStepNumber,
            newWithdrawInfo: {
                ...newWithdrawInfo,
                ...withdrawInfo
            }
        });
    }

    // console.log(newWithdrawInfo);

    return (
        componentsPerSteps.map( (Step,i) => (
            ( i+1 ) === currentStep ? 
                <Step.cmp key={i} 
                onContinue={handleContinue}
                onCheckWithdraws={onFinish}
                withdrawInfo={newWithdrawInfo}></Step.cmp> 
                : 
                null
        ))
    );
}

export default NewWithdraw;