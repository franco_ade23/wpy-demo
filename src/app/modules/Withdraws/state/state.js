export const withdrawsInitialState = {
    accountsAndCardsModule: {
        loading: true,
        error: false,
        accountBanks: {
            // count: 0,
            // next: null,
            // previous: null,
            // results: []
        },
        creditCards: {
            // count: 0,
            // next: null,
            // previous: null,
            // results: []
        }
    },
    banks: [],
    brandCards: [],
    withdrawsModule: {
        loading: true,
        error: false,
        withdraws: {}
    },
    transactionsModule: {
        loading: true,
        error: false,
        transactions: {
            count: 0,
            next: null,
            previous: null,
            results: []
        }
    },
    selectedWithdraw: {
        selected: false,
        withdraw: null,
        volume_total: null,
        // net_total:  null
    }
}