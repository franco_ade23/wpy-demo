import { WithdrawService } from '../services/user-withdraws.service'
import {
    setAccountsCardsModule,
    setBanks,
    setBrandCards,
    setWithdraws,
    setTransactions,
    setWithdrawB
} from './action-creators'

export const loadAccountsAndCreditCards = () => async (dispatch, getState) => {
    try {
        dispatch(setAccountsCardsModule({ loading: true }))

        const listReq = [WithdrawService.getAccounts(), WithdrawService.getCreditCards()]
        const { banks = [], brandCards = [] } = getState().withdraws

        if (!banks.length) listReq.push(WithdrawService.getBanks())
        if (!brandCards.length) listReq.push(WithdrawService.getBrandCards())

        const [reqAccountBanks, reqCreditCards, reqBanks, reqBrandCards] = await Promise.all(listReq)
        dispatch(
            setAccountsCardsModule({
                accountBanks: reqAccountBanks,
                creditCards: reqCreditCards,
                loading: false
            })
        )
        if (reqBanks) {
            dispatch(setBanks(reqBanks.results))
        }
        if (reqBrandCards) {
            dispatch(setBrandCards(reqBrandCards.results))
        }
    } catch (error) {
        dispatch(setAccountsCardsModule({
            loading: false,
            error: true
        }))
        if (error.message) {
            console.log(error.message);
        }
    }
}


export const loadWithdraws = () => async (dispatch, getState) => {
    try {
        dispatch(setWithdraws({ loading: true }))
        const withdraws = await WithdrawService.getWithdraws()
        dispatch(setWithdraws({
            withdraws,
            loading: false,
            error: false
        }))
        console.log(withdraws);
    } catch (error) {
        dispatch(setWithdraws({
            error: true
        }))
    }
}


export const loadTransactions = () => async (dispatch, getState) => {
    try {
        dispatch(setTransactions({ loading: true }))
        const transactions = await WithdrawService.getTransactions()
        dispatch(setTransactions({
            transactions,
            loading: false,
            error: false
        }))
    } catch (error) {
        dispatch(setTransactions({
            error: true
        }))
    }
}


export const selectWithdraw = payload => async (dispatch) => {
    try {
        dispatch(setWithdrawB({
            selected: payload.selected,
            withdraw: payload.withdraw,
            volume_total: payload.volumeTotal,
        }))
    }
    catch{
        dispatch(setWithdrawB({
            selected: false,
            withdraw: null,
            volume_total: null
        }))
    }
}


export const cleanWithdraw = () => async (dispatch) => {
    dispatch(setWithdrawB({
        selected: false,
        withdraw: null,
        volume_total: null
    }))
}