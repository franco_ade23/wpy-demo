import {
	SET_ACCOUNTS_CARDS_MODULE,
	SET_BRAND_CARDS,
	SET_BANKS,
	SET_WITHDRAWS,
	SET_WITHDRAW_B,
	SET_TRANSACTIONS,
} from "./action-types.js";
import { withdrawsInitialState } from "./state.js";

function withdrawsReducer(state = withdrawsInitialState, action) {
	switch (action.type) {
		case SET_ACCOUNTS_CARDS_MODULE:
			return {
				...state,
				accountsAndCardsModule: {
					...action.payload
				}
			}
		case SET_BRAND_CARDS:
			return {
				...state,
				brandCards: [...action.payload]
			}
		case SET_BANKS:
			return {
				...state,
				banks: [...action.payload]
			}
		case SET_WITHDRAWS: {
			return {
				...state,
				withdrawsModule: {
					...action.payload
				}
			}
		}
		case SET_TRANSACTIONS: {
			return {
				...state,
				transactionsModule: {
					...action.payload
				}
			}
		}
		case SET_WITHDRAW_B: {
			return {
				...state,
				selectedWithdraw: {
					...action.payload
				}
			}
		}
		default:
			return state;
	}
}

export default withdrawsReducer;