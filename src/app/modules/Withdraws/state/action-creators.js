import {
    SET_ACCOUNTS_CARDS_MODULE,
    SET_BRAND_CARDS,
    SET_BANKS,
    SET_WITHDRAWS,
    SET_TRANSACTIONS,
    SET_WITHDRAW_B
} from "./action-types"

export const setAccountsCardsModule = payload => ({
    type: SET_ACCOUNTS_CARDS_MODULE,
    payload
})

export const setBrandCards = payload => ({
    type: SET_BRAND_CARDS,
    payload
})

export const setBanks = payload => ({
    type: SET_BANKS,
    payload
})

export const setWithdraws = payload => ({
    type: SET_WITHDRAWS,
    payload
})

export const setTransactions = payload => ({
    type: SET_TRANSACTIONS,
    payload
})

export const setWithdrawB = payload => ({
    type: SET_WITHDRAW_B,
    payload
})