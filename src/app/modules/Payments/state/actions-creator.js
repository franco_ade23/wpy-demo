/**
 * @module Creador de Acciones
 * @summary Se definen las funciones para los tipos de acciones,
 *          haciendo que las acciones sean portables
 */

import { PAYMENT_ADD, PAYMENT_DELETE } from "./actions-types";

//action creator for PAYMENT_ADD
export function addPayment(payment) {
  return {
    type: PAYMENT_ADD,
    payload: {
      payment: payment
    }
  };
}
export function deletePayment(paymentId) {
  return {
    type: PAYMENT_DELETE,
    payload: {
      paymentId: paymentId
    }
  };
}
