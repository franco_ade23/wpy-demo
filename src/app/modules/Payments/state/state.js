export const paymentsInitialState = {
  banks: [],
  paymentsList: {
    items: [],
    loading: false,
    error: false
  },
  paymentsImages: {}
};
