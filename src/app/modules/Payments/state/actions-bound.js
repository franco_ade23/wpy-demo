/**
 * @module Creador de Acciones Conectados
 * @summary Se definen las coneciones de la accion con el disptach
 */
import { dispatch } from 'react-redux';

import { addPayment, deletePayment } from './actions-creator';


/**
 * @summary Dispara la accion ADD_PAYMENT
 * @param {*} payment
 * @returns Un nuevo estado 
 */
export const boundAddPayment = (payment) => dispatch(addPayment(payment));
export const boundDeletePayment = (paymentId) => dispatch(deletePayment(paymentId));