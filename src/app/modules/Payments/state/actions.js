import { PaymentServices } from "../services/user-payment.service";
import {
  SET_BANKS,
  SET_PAYMENTS_LIST_RESULT,
  SET_PAYMENT_IMAGES
} from "./actions-types";
import { toBase64 } from "../../../utils/files.utils";

export const savePaymentImages = (paymentUid, photos = [], cb) => {
  return async dispatch => {
    try {
      const createPaymentFilesRequests = [];
      const filePhotos = photos.filter(photo => photo instanceof File);
      for (let photo of filePhotos) {
        const req = {
          payment: paymentUid,
          file: photo instanceof File ? await toBase64(photo) : photo
        };
        createPaymentFilesRequests.push(req);
      }
      cb(null);
    } catch (err) {
      console.error(err);
      cb && cb(new Error("Error al cargar las imágenes"));
    }
  };
};

export const createPayment = (
  {
    invoices = [],
    photos = [],
    totalAmount,
    totalAmountToDeposit,
    bank,
    operationCode,
    uid
  },
  cb
) => {
  return async dispatch => {
    try {
      let paymentUid = uid;
      if (!uid) {
        const createInvoicesResult = await Promise.all(
          invoices.map(inv =>
            PaymentServices.createInvoice({
              service: inv.service,
              expiration_date: inv.paymentDateFrom,
              client_id_login: inv.intranetCode,
              client_password: inv.intranetPassword,
              amount: Number(inv.serviceAmount)
            })
          )
        );
        const createdInvoicesUids = createInvoicesResult.map(inv => inv.uid);
        const paymentInfo = {
          bank,
          amount_total: totalAmount,
          amount_deposit: totalAmountToDeposit,
          op_code: operationCode,
          promo_code: null,
          invoices: createdInvoicesUids
        };
        const createdPaymentResult = await PaymentServices.createPayment(
          paymentInfo
        );
        paymentUid = createdPaymentResult.uid;
      } else {
      }
      if (photos.length > 0) {
        dispatch(savePaymentImages(paymentUid, photos, cb));
      } else {
        cb(null);
      }
    } catch (err) {
      console.error(err);
      cb &&
        cb(
          new Error(
            err.details
              ? err.details[0].message
              : `Error al ${uid ? "actualizar" : "crear"} pago`
          )
        );
    }
  };
};

export const loadBanks = () => {
  return async (dispatch, getState) => {
    const { banks = [] } = getState().payments;
    if (banks && banks.length > 0) return;
    const { results } = await PaymentServices.getBanks();
    dispatch({ type: SET_BANKS, payload: { banks: results } });
  };
};

export const loadUserPayments = () => {
  return async dispatch => {
    try {
      dispatch({ type: SET_PAYMENTS_LIST_RESULT, payload: { loading: true } });
      const { results } = await PaymentServices.findPaymentsByUser();
      dispatch({
        type: SET_PAYMENTS_LIST_RESULT,
        payload: { items: results, loading: false }
      });
    } catch (err) {
      console.error(err);
      dispatch({
        type: SET_PAYMENTS_LIST_RESULT,
        payload: { error: true, loading: false }
      });
    }
  };
};

export const loadPaymentFiles = paymentUid => {
  return async dispatch => {
    try {
      const { results } = await PaymentServices.getPaymentImagesByPayment(
        paymentUid
      );
      dispatch({
        type: SET_PAYMENT_IMAGES,
        payload: { paymentUid, images: results }
      });
    } catch (err) {
      console.error(err);
      dispatch({
        type: SET_PAYMENT_IMAGES,
        payload: { paymentUid, images: [] }
      });
    }
  };
};

export const unloadPaymentFiles = paymentUid => ({
  type: SET_PAYMENT_IMAGES,
  payload: {
    paymentUid,
    images: null
  }
});
