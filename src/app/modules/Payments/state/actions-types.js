/**
 * @constant Tipo de Acciones
 * @summary Contiene los tipos de acciones del store
 */
export const PAYMENT_ADD = "PAYMENT_ADD";
export const PAYMENT_DELETE = "PAYMENT_DELETE";
export const SET_BANKS = "SET_BANKS";
export const SET_PAYMENTS_LIST_RESULT = "SET_PAYMENTS_LIST_RESULT";
export const SET_PAYMENT_IMAGES = "SET_PAYMENT_FILES";
