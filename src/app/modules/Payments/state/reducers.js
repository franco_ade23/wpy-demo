import {
  SET_BANKS,
  SET_PAYMENTS_LIST_RESULT,
  SET_PAYMENT_IMAGES
} from "./actions-types.js";
import { paymentsInitialState } from "./state.js";

function paymentsReducer(state = paymentsInitialState, action) {
  switch (action.type) {
    case SET_BANKS:
      return {
        ...state,
        banks: action.payload.banks
      };
    // case PAYMENT_ADD:
    //   return [...state, Object.assign({}, action.payload.payment)];
    // case PAYMENT_DELETE:
    //   return state.filter(item => item.id !== action.payload.paymentId);
    case SET_PAYMENTS_LIST_RESULT:
      return {
        ...state,
        paymentsList: action.payload
      };
    case SET_PAYMENT_IMAGES:
      const { paymentUid, images = [] } = action.payload;
      return {
        ...state,
        paymentsImages: {
          ...state.paymentsImages,
          [paymentUid]: images
        }
      };
    default:
      return state;
  }
}

export default paymentsReducer;
