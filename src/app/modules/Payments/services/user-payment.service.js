import _ from "lodash";

import api from "../../../services/api.service";

export const PaymentServices = {
  findPaymentsByUser: () => api.get("user-payment/payment"),
  getBanks: () => api.get("user/service/banks"),
  createInvoice: invoiceInfo => api.post("user-payment/invoice", invoiceInfo),
  createPayment: paymentInfo => api.post("user-payment/payment", paymentInfo),
  updatePayment: paymentInfo =>
    api.patch(
      `user-payment/payment/${paymentInfo.uid}`,
      _.omit(paymentInfo, ["uid"])
    ),
  createPaymentFile: paymentFileInfo =>
    api.post("user/file/payment-image", paymentFileInfo),
  getPaymentImagesByPayment: paymentUid =>
    api.get(`user/file/payment-image?payment_uid=${paymentUid}`)
};
