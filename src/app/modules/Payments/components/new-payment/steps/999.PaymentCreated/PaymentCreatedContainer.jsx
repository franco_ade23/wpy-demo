import React from "react";
import PaymentCreated from "./PaymentCreated";

const PaymentCreatedContainer = ({ onFinish, ...others }) => {
  return <PaymentCreated onAccept={onFinish} {...others}></PaymentCreated>;
};

export default PaymentCreatedContainer;
