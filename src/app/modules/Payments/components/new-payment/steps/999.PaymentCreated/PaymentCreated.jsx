import React, { useEffect } from "react";
import styled from "styled-components";

const StyledPaymentCreated = styled.div`
  text-align: center;
  img {
    height: 100px;
    margin-left: 50px;
  }
`;

const PaymentCreated = ({
  onAccept,
  setDisableBackdropClick,
  paymentInfo: { uid }
}) => {
  useEffect(() => {
    setDisableBackdropClick(true);
  }, [setDisableBackdropClick]);
  return (
    <StyledPaymentCreated>
      <img src="/images/payments/success-payment.png" alt="" />
      <div className="spacer-20"></div>
      <h3 className="text-primary">
        <b>
          Tu pago ha sido
          <br />
          {uid ? "actualizado" : "creado"} exitosamente
        </b>
      </h3>
      <div className="spacer-30"></div>
      <p>
        Puedes revisar su estado
        <br />
        en nuestra pestaña de <b>Pagos publicados</b>
      </p>
      <div className="spacer-30"></div>
      <button
        className="wpy-btn wpy-btn--primary wpy-btn--rounded"
        onClick={onAccept}
      >
        Aceptar
      </button>
    </StyledPaymentCreated>
  );
};

export default PaymentCreated;
