import React, { useState } from "react";
import { compose } from "redux";
import { validateForm, required, exclusion } from "redux-form-validators";

import NewInvoiceForm from "./NewInvoiceForm";
import { SELECT_DEFAULT_VALUE } from "../../../../../../constants/forms.contstants";
import withConfirm from "../../../../../../hoc/withConfirm";
import NewInvoiceBox from "./NewInvoiceBox";
import { withToastService } from "../../../../../Toasts/hoc/withToastService";
import { AMOUNT_TO_DEPOSIT_COMISSION_PERCENTAGE } from "../../../../../../constants/payments.constants";

const NewInvoiceFormContainer = ({
  onContinue,
  openConfirmModal,
  toastService
}) => {
  const initialState = {
    invoices: []
  };

  const [state, setState] = useState(initialState);

  const validate = validateForm({
    serviceAmount: [required()],
    // serviceCategory: [exclusion({ in: [SELECT_DEFAULT_VALUE] })],
    service: [exclusion({ in: [SELECT_DEFAULT_VALUE] })],
    intranetCode: [required()],
    intranetPassword: [required()],
    paymentDateFrom: [required()]
    // paymentDateUntil: [required()]
  });

  const askUploadPhotos = async (invoices = []) => {
    const confirmUpload = true; /*await openConfirmModal(
      "¿Deseas subir las fotos de los pagos?",
      "Nuevo pago",
      {
        confirmText: "Sí",
        cancelText: "No"
      }
    );*/
    if (typeof confirmUpload === "undefined") {
      return;
    }
    const totalAmount = invoices.reduce(
      (total, inv) => total + Number(inv.serviceAmount),
      0
    );
    const totalAmountToDeposit =
      totalAmount - AMOUNT_TO_DEPOSIT_COMISSION_PERCENTAGE * totalAmount;
    const paymentInfo = {
      totalAmount,
      totalAmountToDeposit,
      invoices: invoices
    };
    if (confirmUpload) {
      onContinue(2, paymentInfo);
    } else {
      onContinue(3, paymentInfo);
    }
  };

  const handleSubmit = async (values, form) => {
    const newInvoices = [...state.invoices, values];
    setState({
      ...state,
      invoices: newInvoices
    });
    const confirmAddOtherInvoince = await openConfirmModal(
      "¿Deseas agregar otro tipo de pago?",
      "Agregar otro pago",
      { confirmText: "Sí", cancelText: "No" }
    );
    form.reset();
    if (!confirmAddOtherInvoince) {
      askUploadPhotos(newInvoices);
    }
  };

  const handleSubmitTry = async (form, valid) => {
    if (state.invoices.length === 0) {
    } else {
      const confirmDiscardAndContinue = await openConfirmModal(
        "Estás creando una boleta, ¿Descartar y continuar?"
      );
      if (confirmDiscardAndContinue) {
        return askUploadPhotos();
      }
    }
  };

  const renderInvoices = state.invoices.map((inv, i) => (
    <NewInvoiceBox invoice={inv} key={i}></NewInvoiceBox>
  ));

  return (
    <div>
      <NewInvoiceForm
        validate={validate}
        onSubmit={handleSubmit}
        onSubmitTry={handleSubmitTry}
      ></NewInvoiceForm>
      {renderInvoices}
    </div>
  );
};

export default compose(
  withConfirm,
  withToastService
)(NewInvoiceFormContainer);
