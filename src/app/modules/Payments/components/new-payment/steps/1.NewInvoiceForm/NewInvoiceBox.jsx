import React from "react";
import styled from "styled-components";
// import moment from 'moment';
import { formatToDDMMYYYY } from "../../../../../../utils/date.util";
import { formatAmount } from "../../../../../../utils/number.util";

const StyledNewInvoiceBox = styled.div`
  margin-top: 15px;
  border: 1px solid rgba(0, 0, 0, 0.3);
  border-radius: 15px;
  padding: 20px;
`;

const NewInvoiceBox = ({ invoice }) => {
  return (
    <StyledNewInvoiceBox>
      <div className="flex-row-layout ai-center">
        <h3>{invoice.serviceName}</h3>
        <span className="flex"></span>
        <div className="flex-col-layout">
          <h3>
            <b>S/ {formatAmount(invoice.serviceAmount)}</b>
          </h3>
          <p className="text-muted">
            {formatToDDMMYYYY(invoice.paymentDateFrom)}
            {/* &nbsp;-&nbsp; 
                        { formatToDDMMYYYY(invoice.paymentDateUntil) } */}
          </p>
        </div>
      </div>
    </StyledNewInvoiceBox>
  );
};

export default NewInvoiceBox;
