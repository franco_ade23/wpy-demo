import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Form, Field } from "react-final-form";
import InputAdornment from "@material-ui/core/InputAdornment";
import MenuItem from "@material-ui/core/MenuItem";

import MaterialInput from "../../../../../../components/MaterialInput";
import { colors } from "../../../../../../../styles/js/1-settings/colors";
import MaterialSelect from "../../../../../../components/MaterialSelect";
import MaterialDatepicker from "../../../../../../components/MaterialDatepicker";

//services
import UserServices from "./../../../../../../services/user-services.js";

const StyledNewInvoiceForm = styled.form`
  .newInvoiceForm {
    &__row {
      display: flex;
      > div {
        flex: 1;
        &:first-child {
          padding-right: 0px;
        }
      }
    }
    &__fc {
      padding: 20px 0;
      &:first-child {
        padding-left: 0px;
      }
      label {
        font-weight: 500;
        color: #545454;
        position: relative;
        white-space: nowrap;
        &::before {
          content: "\u2022";
          position: absolute;
          color: ${colors.grey4};
          left: -12px;
          top: 50%;
          transform: translateY(-50%);
        }
      }
    }
  }
`;

const renderForm = ({ handleSubmit, valid, form, onSubmitTry, values }) => {
  const { services } = values;

  const inputsProps = {
    availableCredit: {
      step: 0.01
    },
    serviceAmount: {
      startAdornment: (
        <InputAdornment position="start">
          <span className="input-prefix">S/</span>
        </InputAdornment>
      )
    }
  };
  // const services = [ { id: 1, name: 'Servicio 1' } ];
  const renderServices = () => {
    if (services.lenght) return <span>cargando..</span>;
    return services.map(item => (
      <MenuItem value={item.id} key={item.id}>
        {item.name}
      </MenuItem>
    ));
  };

  const _handleSubmit = (...args) => {
    if (!valid) {
      args[0].nativeEvent.preventDefault();
      onSubmitTry(form, valid);
    } else {
      handleSubmit(...args);
    }
  };

  return (
    <StyledNewInvoiceForm onSubmit={_handleSubmit}>
      <h2 className="text-primary">
        <b>Nuevo Pago</b>
      </h2>
      <div className="spacer-20"></div>
      <div className="newInvoiceForm__row">


        <div className="row">
          <div className="col-md-6">
            <div className="flex-col-layout">
              <div>
                <div className="newInvoiceForm__fc">
                  <label htmlFor="">Monto de servicio</label>
                  <Field
                    component={MaterialInput}
                    name="serviceAmount"
                    type="number"
                    className="fill-width"
                    placeholder="1800"
                    inputProps={inputsProps.availableCredit}
                  ></Field>
                </div>
              </div>
              {/* <div>
                            <div className="newInvoiceForm__fc">
                                <label htmlFor="">Categoría del servicio</label>
                                <Field component={MaterialSelect} name="serviceCategory"
                                renderValue={ sc => sc.name || "Categoría" }
                                initialValue={SELECT_DEFAULT_VALUE}
                                placeholder="Categoría" renderOptions={renderServiceCategories}></Field>
                            </div>
                        </div> */}
              <div>
                <div className="newInvoiceForm__fc">
                  <label htmlFor="">Servicio</label>
                  <Field
                    component={MaterialSelect}
                    name="service"
                    initialValue={services[0] && services[0].id}
                    placeholder="Universidad"
                    renderOptions={renderServices}
                  ></Field>
                </div>
              </div>
              <div>
                <div className="newInvoiceForm__fc">
                  <label htmlFor="">Código Intranet</label>
                  <Field
                    component={MaterialInput}
                    name="intranetCode"
                    type="text"
                    className="fill-width"
                    placeholder="u2011229022"
                  ></Field>
                </div>
              </div>
              <div>
                <div className="newInvoiceForm__fc">
                  <label htmlFor="">Contraseña Intranet</label>
                  <Field
                    component={MaterialInput}
                    name="intranetPassword"
                    type="password"
                    className="fill-width"
                    placeholder="password"
                  ></Field>
                </div>
              </div>
            </div>

          </div>
          <div className="col-md-6">
            <div className="flex-col-layout">
              <p>
                <b>Fecha de vencimiento del recibo</b>
              </p>
              {/* <div className="spacer-25"></div> */}
              <div>
                <div className="newInvoiceForm__fc">
                  <label htmlFor="">Fecha</label>
                  <Field
                    component={MaterialDatepicker}
                    name="paymentDateFrom"
                    className="fill-width"
                    placeholder="30/02/2019"
                  ></Field>
                </div>
              </div>
              {/* <div>
                            <div className="newInvoiceForm__fc">
                                <label htmlFor="">Hasta</label>
                                <Field component={MaterialDatepicker} name="paymentDateUntil"
                                className="fill-width" placeholder="30/02/2019"></Field>
                            </div>
                        </div> */}
              <span className="flex"></span>
              <div className="flex-row-layout jc-center">
                <button className="wpy-btn wpy-btn--primary wpy-btn--rounded">
                  Siguiente
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </StyledNewInvoiceForm>
  );
};

const NewInvoiceForm = ({ onSubmit, validate, onSubmitTry }) => {
  const [initialValue, setInitialValue] = useState({ services: [] });

  useEffect(() => {
    const fetchServices = async () => {
      const { results } = await UserServices.findServices();
      setInitialValue({ services: results });
    };
    fetchServices();
  }, []);

  const handleSubmit = (...args) => {
    args[0].serviceName = initialValue.services.find(
      s => s.id === args[0].service
    ).name;
    onSubmit(...args);
  };

  return (
    <Form
      onSubmit={handleSubmit}
      validate={validate}
      render={renderForm}
      initialValues={initialValue}
      onSubmitTry={onSubmitTry}
    ></Form>
  );
};

export default NewInvoiceForm;
