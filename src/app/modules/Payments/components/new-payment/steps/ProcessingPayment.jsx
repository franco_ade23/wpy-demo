import { useDispatch } from "react-redux";
import React, { useCallback, useEffect, useState } from "react";

import { createPayment } from "../../../state/actions";

export const ProcessingPayment = ({
  paymentInfo,
  onContinue,
  setDisableBackdropClick
}) => {
  const dispatch = useDispatch();
  const [error, setError] = useState(null);
  const _createPayment = useCallback(
    () =>
      dispatch(
        createPayment(paymentInfo, result => {
          setDisableBackdropClick(false);
          if (result && result.message) {
            setError(result.message);
          } else {
            onContinue(3);
          }
        })
      ),
    [dispatch, paymentInfo, setError, onContinue, setDisableBackdropClick]
  );
  useEffect(() => {
    _createPayment();
  }, [_createPayment]);
  return <span>{error || "Procesando..."}</span>;
};
