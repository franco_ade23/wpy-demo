import React, { useEffect } from "react";
import { useSelector } from "react-redux";

import UploadPaymentPhotosForm from "./UploadPaymentPhotosForm";
import { withToastService } from "../../../../../Toasts/hoc/withToastService";
import { useActions } from "../../../../../../hooks/useActions";
import { loadBanks } from "../../../../state/actions";
import { validateForm } from "redux-form-validators";

const UploadPaymentPhotosContainer = ({
  onContinue,
  paymentInfo,
  toastService,
  onlyShowImages,
  onFinish
}) => {
  const validate = validateForm({});
  const banks = useSelector(state => state.payments.banks);
  const initialValues = { operationCode: "", bank: banks[0] && banks[0].id };
  const actions = useActions(
    {
      loadBanks
    },
    []
  );
  useEffect(() => {
    actions.loadBanks();
  }, [actions]);

  const handleSubmit = ({ photos = [], bank, operationCode }) => {
    const notEmptyFiles = photos.filter(p => !!p);
    if (notEmptyFiles.length <= 0) {
      toastService.warn("Debes subir al menos un archivo");
      return;
    }
    onContinue(3, {
      photos: notEmptyFiles,
      bank,
      operationCode
    });
  };

  const handleOtherOpportunity = ({ bank, operationCode }) => {
    onContinue(3, {
      bank,
      operationCode
    });
  };

  const isReady = banks && banks.length > 0;

  return isReady ? (
    <UploadPaymentPhotosForm
      onSubmit={handleSubmit}
      validate={validate}
      banks={banks}
      paymentInfo={paymentInfo}
      initialValues={initialValues}
      onOtherOpportunityClick={handleOtherOpportunity}
      onlyShowImages={onlyShowImages}
      onFinish={onFinish}
    ></UploadPaymentPhotosForm>
  ) : (
      "cargando..."
    );
};

export default withToastService(UploadPaymentPhotosContainer);
