import React, { useEffect } from "react";
import { Form, Field } from "react-final-form";
import arrayMutators from "final-form-arrays";
import { FieldArray } from "react-final-form-arrays";
import Tooltip from "@material-ui/core/Tooltip";

import PhotoInput from "../../../../../../components/PhotoInput";
import MaterialInput from "../../../../../../components/MaterialInput";
import { StyledUploadPaymentPhotosForm } from "./styles";
import MaterialSelect from "../../../../../../components/MaterialSelect";
import MenuItem from "@material-ui/core/MenuItem";
import AppIcon from "../../../../../../components/Icon";

const RenderForm = ({
  handleSubmit,
  banks = [],
  paymentInfo,
  valid,
  values,
  onOtherOpportunityClick,
  onlyShowImages,
  form: {
    mutators: { push, pop }
  },
  onFinish
}) => {
  useEffect(() => {
    if (!paymentInfo.photos || paymentInfo.photos.length === 0) {
      push("photos", undefined);
    }
  }, [paymentInfo, push]);

  const renderBanks = () => {
    return banks.map(item => (
      <MenuItem value={item.id} key={item.id}>
        {item.name_long}
      </MenuItem>
    ));
  };
  const renderBanks2 = () => {
    return banks.map(item => (
      item.id === 1 ?
        <MenuItem value={item.id} key={item.id}>
          BCP : 1912536523043 <img width="80px" alt="cuenta bcp" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAX8AAACDCAMAAABSveuDAAABFFBMVEX///8AQZL5alMAP5EAPpEAPJAAOo8AOI4AOY8ANo75aFH5ZU0AM4z5ZEsANI0AMYz09/uSosXi6vMALYr7/f7/+fjs8ff5YEbo7/YARZXU3uv5blf/ak3+6ufAzuIAK4r+39r8urD90cv7nY+Eo8p7kbwRUJsxYKPJ1+itwtxagreUq825x93/8vD6iXhukb/6lIVogbL6gm9HbKijtNGrvNYOS5hScKlwibeNqs5Aca39ysL6dmAyWJz92NJ1kb38qp77motieq6IkLPpycjWcG+zbX98cJRnZ5focGTBaXP7sqale41LZp8OWKDCeH5gVY1KU5Dje3GVeJWtc4esfo7ahH/PbnFair2qZ39Pe7P6e2XpvOYhAAAUP0lEQVR4nO1da3uiypaOFgWCiSIaIrhDTMeAiolJRGIutmnPnOm9J+c45zYzPfb//x8DJkYuqy5gP7N3Pw/vh33p1rJ4q2rda3FwUKBAgQIFChQoUKBAgQIFChQoUKBAgQIFChQoUKBAgT8YWqqu96cP42/D9frbuOdZmq6rrd97Vn8wtNS2Zobo98N/alpb/SHjtk2r92jXmoeyJAaoSFKtfoadoTsytWINWrrWtwZTz2tczm7vJ46zsgOsVs7kfjlreFPL3GsVdKuxtGUZC6gUhyDKNfu+Me0zlyCc4Cicodf4gBv832Bkme195vZ7w3J7i9ky4HzuG4IkSxVRFAQkoACCIGCMRbli2JPlzDVz/oLm3tuSKJQIQFgqzZc94ujaNJhguCnmtm+UsCTvIOGSb8+dYI/0Bnln9ztjUUICFjEOCS8lt+eOI0EUkTH/ZmX/AW3oC5g48Mc5EAzHgw5B8PVSMEERb/ZEaobBnwjhXwvBQqyn+t50/H9j4CMWNx+PGixC0+hlkkQtcyxLfL+AcN13U5LEdGqI8/uCWK87nvlzKZNGhZP97VPKxmLEPbrZ8OUMgwvyfeKAtWZZvh+uod34mVagvRSz8R+ugH/JJ2vV6UTmPVzvEO34AmgZt0ewhpLT+3n0sekQ1SIZSHSmHGO3ZwbOPLboxGS4e5h9eoLg9JmTUwcPvYGWl7YdWvpo7PjYfswml7cYcUv/+CPWF8xfG5SkPEOLveggj9lXMJRCskedmtp/rNVkSa47o/1kVcts2E0psKoFLDcfB5nVf8ut5eEoeMT6kP5j+sNZrpUt4WV0ZfNtj5JQcsnEqqP14ZtBhnBzvM8Z0N25vJMfIv7Tp+OMA9xmFv9bSAvaAmizPBt3Q50TUS5aVv2xBfKJJ6A/M3ZKBUnLf7vKxllkdvdCTHoL/p+/dLON4OR8vhCULWZOcqiV94eYRzSwl3t7CD5BBwzmsTVF/r9fP2Xi7AN9J2kc4K9K5yXLcpp599fmCYnOmGmLuccV5hHixnlPUcCFDW2PVq+U2BnCr78pN90MnG1hzdO2mf+bctR54hdCXg7z4gPihGDnWWJ+2uLyZ7LHQLVFemb6ZXrDGf+hVJXXzELITO3+AJW//FJWTi66vIMM4fONwjgZElAqXBZHHZaxlp1b+ATA97tVNed7jITslJfSWgDenPi1oyhH15+z0a9OIOtO+Osv5XK5escr0WCm6vbKaDZXYRC0VK9LRA6EFUi/s8emDfi/3Q018AkbAImiJG3ib5QdgnvJqbkQZ2j1rAScdc4zHYEx6Jcj47dgrLLSueCSQXoTGqQyaLfbmtbWN/8aLewm6SmbgAYwVwz6wzCSXG+ebVCvSWL8lAkR1lz4AKLmajgej3u9xXi9EpsyYXp4kjgAAwP8IP7PashZ+eaUn/4ByTD4S3gAysrRM89g0zowQtp3se4JgVHpW2pI3afTL4gl2xk+DEw9UI+qZnnj+7mP8e6MGbs1VWdg9CFmIQUL/jDxwRVAQtxLNwkHEy/fOKuWP/HS3yYqpq+bsYLz9Mwx2Bg4j4DYDNw0AxRCwjxF/5rm9CJcWd26yVyLPugt51tJIsx37pcGPiVK212jGSiopJiTri4IsSS0eudMOTnnNF084hP+rVN+H+z5nDkMFPzBa8iq8UD+kZ3wHls9mspGFce1wLiFbnkTvFk5cb37U8uGBhNv046fOoU0WcyUOhjB0iec10Zob2QQn/WuTUjiR/j7P5TtAnReGMO0oeeTG6Dd3ID0TWorjnyywYLkkkvJ9GrWUA6e6jAi/UYVkP8G9PU+4HGgSsSVUB+JvhzaKIC3BbjgWYAp0W0S5h9jBQLthT7MwABGMODQJiwLjHgqgCRhNzOrMCJGBy1tjcWz3elTwTUnTXAkpTmRIy76ANJ1b0D//GXH2TVbBOmg7fk2VIT/QAvTzaAeeGYJfrsHSU9jECNwRo7mCVwha3P2uPuf9j20mjH/OAJIvIvDnbSjxMLR1w/+y+WjO+YJUGc2sNhvQ9n/ioylKC+0BYCCb+KSsEtHgIpLyJ8BOZoh3/OlbFoRhaKB4l8get3pCQrOB//qbCWTZCP6NcJZucq2Q9X+QoBXIM5/IM9eyaNoUJAMXxI+bQG+aNxWUsl+r3yfI8zbB0+TuCQ9zn1qO6HSbv+3+j2bkMuM868oHDpA1YZ1aAUS/AeDka0g0LwwSHHbPsC/YEetmQdiqlZe54myw7kJDIR1NmhdpqVyPWaAaos6eAbi/LOkxhbmzE/r/CT/gU1LjEV4gNeCbFLqDpI/OGIsUoI1+DFXkmMNWv/CgPR5oJTgLPHD7YUNPfTXJGcvXBPs3/rJIxe1f7ZmKCGypF4C+1VwSBaiC+jfQzfygR5JwQnJOAAfWqC9jhBxLd30BJL8B7vk0k/JjST/5TJ508agTu8TJnKa/7JyDesTDdpfUfcnBtAYkSO6mmh7Av4qFzQwOIV84he4+A8O8m2yFC9if7I2bRJtF8d+NeJ/7QaDrVATUpdyKmb4jilwbqVh5AMeKeVSdwlDMgDnJvAj8QuA/GlCxpzuJcxH9F9J/ombNg3NiS4n+lsnzf8RqIMtSL01CeJfg6xPHM3UkiJSIpkwOr6B4RriBgn0b9qcFuAqjfYwpofRfydlRmCFcjnCm9917cijp0TZRp90ga/1APGPmvBvaID3gsRe5OksgnuJ/LwFBnPwPJE2CChPI/Z/AlNnd56RD3BWPmGHz7aw7neOT0qUbRbgLi2BVEj8Cw40fmsEyXY8iTI7hIOLCFOy9FRAJy5AhVh31F+l5GnU/03AnH2UtggTkLNyl3+ui3dZiUrAUQpPU9oN0yHzQkwH9IPT2rOhpYpVCuoE6YPzOF4bgMEpin12ME0roBpl8XV3ewJEcM+WeSJBW6huZbP4yElJ/7fFfE7pcxPMvQDS1XOg7AuKJ3+J0a2cyhcOTgUqf0xiVJ2lxb9Mq0NsWcLbN3DaZNkAVpsEjDZpP/FP4FIGY6VsIEj8J6yLVks1F74EhfQFIU4s6CuF2z93Rf4SHLFOrL02096v4NAP35vNIEyeCZv2ezfDfK2JHDiHsPgJDdpkOgyurKz13q59hfe/LG+2gh32Ep7Ho5kqLKyRkHv7g8GpAERXDijlkC4ZRaraMNhb+J9pk/GdtJcsMzYdGU9IQ5WPkjENQjZClgTD8G3bNyr1WoUQUZAmybwL/Dnk597+cO6LvKOtWjoW4zMvKrRnh2j1PyTOlLtulimbk/q/CCOFGiBuz8Le5fvE30D6W9zsJVmAhXUgrbPMPwYP/P3KjLCgbcBCi1YSkaCum7/+RuRfyVaZaH77M2mowJ6NCyAv072SKPuVSVqrEcp4m7nvxMH5TqA24w2Q8kVcul//9r9kzlJCg4EnovgJx4p99FueylYkVOwlcKgJwppiLLIABqcCwBJFXwCxeEx0vmI4vSNzprBzYVEcXxC0b4jqTeyzeQqUK6JzCV5YgHIDpTD9mmX2MZhpZ6oURsfBUJ4GVpbUiYHqGJ6OyJwpnQwlWQcHXYIh9b6WsefLXlmJ/NmUcLVtCpo/sfKDjLDAaB4Ck9P9JZQOFydcP3R8Tdmz5aNM/J9TljKwZqMfJVZWkukvfSNa01Amh21+U9DywNwXxL/aA+rAQweFT/d0aZyVT7Lwf0qR/kn+GzlqZFFpSGgTACtLkWSssKHChdkp+dPSez58sR7Tb4B9gCL9s/J/QV3KmPwnVFayIMpraAVUIPMaoMZJAQCdkMoXZ7tbhqpmjmYC7B8iTL0dtcOnExpnmeT/6RF1KWP2j5nzYgOSjEZaqLRB8xMJ/Pe0kzAPCeIxsAA8bxrAaywmfo1kxMVjs2RcXVM5y2T/3NAUScL+h71LHgj4PiWDgcqPEtFY4QL5Xo4gbcrNsSSL5Np/ZHA6HudlKv9VvjJ+nqGUWEH6NJf4eXs2nMrnwqcpXv2aDYR7OdsplOjXUhE17hnBZ5rBGOCI3/9lDhVdSrD0gX8FzhILAF+izx/6PzgglirzQGDHfd5wdU3VmFniP6cM6ROPf7ZB7xLjsN+PKMm1w4p0SLpVEj5gokgIdibwMncbBv1sD/rxnFfvvNDpL7PqlyOgeb6bob5ENYkJiX/8uHYc53G4aHijAI21f0gsqBrGqIWdVfE2d8ssSq0ym37b4ox6nFfpIgNIWpHwyhoqHv7vQ+qt2dfbWru97YjX0jVv1SRcpxBipiWQed2Pf+iaIh+QzKt6Dz5R3aVM2/+cbnqmhgIrNTGwbQYOvAA45tr+cP5zX/sVhDWvz8dSmMH25zV+GKZPeCE1NlQLEv/4ETq27QWsCuvR/BcsfzBwT4gPWs5rv0icu7xr/vk7Q2Jzl8AdnDNXstONfQGsrCQUNqmXoB4Wh5HVMsEuEmJu+wcq9eWAULvlDvh9vmPRTykcj+OVJceUasKMBXNfdYK3pIJ1ndHKeoL9mbPsNoBL7gNIYf/Q4W+/x9z9/PVvLwzVW1ZSWRzIuyS7jBaYqYxevYa7nZAuCrFxmdk7RFjyXX5x9+mZST9n/efVF5bqTZieISDvEj8SrfUVtBuj4kpbw9HKnPEfDbz3RSNftCcZYn3HTH3JbXp2GW4XvJKQupTJtRrQLe1YpRwcf8ud/oJuOpHJF2vGfWOUwdS6elGY9HMm3p9YWgQMoWrQc0jk64lTyFuIVmoRotnQRWkeTDmbjgVCp364uvWsTHr+9IYpMZQqn+597bDpV9JtCAbA89EuSYClzbFKXTj/krP0ueWCV70Pm8364Xvb4cN6vXkmr9aLqallXONPHFv26JXH8j+9YWleqOwtwAK69ksxVvqQAo7x78LhysNcCZg2pJ6Q7/VNy+tt8OANLDPX2bp6Zcqe8No6B/1X5x1G9Cgc6hmSY1DLVXFGlqAj5v6fgrXKlPJ7GkBvjngvKhO61ydM+stc9HcvmFIs3P0Q/fC1X7CpwhtAeRDjf0RI55CrZSnoQ10f5IccIyVwdc40OwOccORcjs/ZUox49RQky6d0BwCTIbENSWp3iPMUYIG5r7P9u8t3b5hmZznMk7D9ru41z0gkIwqqrKT6SqA3KsdKOwldpJGYfdu2wOWW9+6p/dJhi/6wYwaT/tOLE46Rysp3uAFTC8p94Qn5+TwwGB/XrYT625Lg8xWhRQB2MaDceuTC8dMzh7wO2zWwhM/p+Xe23i2HIU+CBwdf+50Rf1AHg5ulesy2tGD6w4XlCUKoEWMGDE6Rbz1y4fMXnh0b0M+y+4/Pr9lGZ4gjYvwCrNUku6qE+CdaxUybFrH1hsgRhbNmEecD7ElHCg5yofvCtfnLVdBajOLTTZlD74aaNxXz2T0sVFlZJ5HUcuH4f/IeFhij2ACzwnD6wo72Ch5D1r+RP5V/9fqdc8uyOuZ9DvY+10hKhyzGYO9SJnxa7YEtwNKltSb5Ogc2aFdQwwLCWKYAivZRgoMMHJ93+NhXFGbbn+9cpyjQ4XeUhQRjNXAj1QPVfCQkwsVUkz5Ko35BXvdhR6yluXYNxe5p6ZAgk3t5U5nnHA5XiGqH0ijpHbRK9Qj91S9dyiA6lFsFzQvVuky1V9kCpSILI0rHbSQZl0DfQ91qOHVhc0d794cDKOFMCQ4ycMW1Z5UqTw/0K2qp+sdC0vvotiFNidL7v+3dzomUAqVVOpwD2P6AbN+6sboQ3eot7fdLy3bESIW62OTtoBKCVePzRv8FV7bliR24Prru0scAg88lKd5Hz3pYzw3KC9MMwKqf0pt1I4z8+XrsDixrNHgYT+a+sF3eWKEi5Mnlz2QG9jorOR6wr3B2jD9+YVT5cPRc1eB67dqmo/KiN/62doyzpoxpKRCoszt4BS7+G0jAUq0eQg7fpvaxXNGObtpfgR+uUIKDTDwxYgVKh2wrJnFKlUDVDkfjeY2Q20aiFHYUlyrpdzQmIU1AOnI3i492dAMLs2nBQSaOqekuPsn/gS651kE5ueF5+0sbdmczQCTVNYOeExuoFJFmHrA9EC04yMYVOUerHD2fZ7rhdfCZkPJSjsp8/ev1XG90itJPLusf51qAmHMF3aQR5vsFP6/uYB2sVI/431SxRfcO8CcCwc/72hcV7LuRgX7Ku80It7YYiGYSNKjrBr7PSlICx9eACFKqnVyvfTl9SR4Bpfr9hf8UWXvJH5lqirSHOQaPNikAg1PifsG3AMevz4ldq1SfbzK+ceRjsKframS0wN197Wb4OnxZiA8CnjE6uszgeAUN0XvSI0D5I3mf4Ns7Pl8cxTjrvGR9VVoEp09fyifBeIEAOzm6fsqmQQ48OFnLAdFglrfqvWbWBYj0KGxBXSmQ8SPeZ3r16aKz4Swk7fmlm5/9EMdXn16/XF/fvDydZh5Ipb6igggkHHK81jKgUMx2vqI5ShUq5NqjiUoMAWcv13ff7768dq/2Y39PwK9OYrAvlhzOcjZzneW1p0iOGJcaZBxXfsBbSv9YCKyMrK/lFe5dbhpUb0J+Z1WK3eiFCeh1fPLiJ3qdMid0187w4kF8WFpPM21C0yU3208sbEymAalmorf3U6NlLuqHWGDU2YfvtpfqZ46budispbl+HTPqOIPh63F2U01EUcXOf4n+jw3Ve7SNEhZFLARGH4rzIqDwL0qG74yneVNP3sQQyGE8JGBkrBKWfSz4E76nDfOo/J8Wat/tzZYTZx4uhLyFiAx77kyWs55n5W5gsoH5sJwb4fvt4t2uw/ffifbktpe6KDqpS+8I5oF8Z5m/gdNPg5ZmWqOp53luI0TwH950ZPW1PUK+kcH708at40v1MLIqihVJrtVl3xk2vBEk1vuXWzQarjcidLspkAm62bdG7uXidjjc3O22+mab2MR2i4L5H4tW643WVkFsgQIFChQoUKBAgQIFChQoUKBAgQIFChQoUKBAgQK/M/4PcMz7WpYb038AAAAASUVORK5CYII="></img>
        </MenuItem> :
        item.id === 2 ?
          <MenuItem value={item.id} key={item.id}>
            INTERBANK: 6393001448441 <img width="80px" alt="cuenta interbank" src="https://comutelperu.com/wp-content/uploads/2018/05/interbank.png"></img>
          </MenuItem> :
          <MenuItem value={item.id} key={item.id}>
            BBVA: 001100940100005150 <img width="80px" alt="cuenta bbva" src="https://www.stickpng.com/assets/images/5a270db16c723e0970203ea9.png"></img>
          </MenuItem>

    ));
  };

  return (
    <StyledUploadPaymentPhotosForm onSubmit={handleSubmit}>
      {!onlyShowImages && (
        <>
          <div className="flex-row-layout jc-center">
            <div className="row">

              {/* <span className="spacer-h-40"></span> */}

              <div className="col-md-6">
                <div className="uppf__fc">
                  <label htmlFor="">Monto total del recibo:</label>
                  <Field
                    component={MaterialInput}
                    name="totalAmount"
                    type="number"
                    InputProps={{ readOnly: true }}
                  ></Field>
                </div>
              </div>
              <div className="col-md-6">
                <div className="uppf__fc">
                  <label htmlFor="">Monto a depositar:</label>
                  <Field
                    component={MaterialInput}
                    name="totalAmountToDeposit"
                    type="number"
                    InputProps={{ readOnly: true }}
                  ></Field>
                </div>
              </div>
            </div>
          </div>
          <div className="flex-row-layout jc-center">
            <div className="row">
              <div className="col-md-6">
                <div className="uppf__fc">
                  <label htmlFor="">CCI</label>
                  <Field
                    component={MaterialInput}
                    name="cci"
                    type="number"
                    InputProps={{ readOnly: true }}
                  ></Field>


                </div>
              </div>
              {/* <span className="spacer-h-40"></span>  */}
              <div className="col-md-6">
                <div className="uppf__fc">
                  <label htmlFor="">Banco:</label>
                  <Field
                    component={MaterialSelect}
                    name="bank"
                    placeholder="Banco"
                    renderOptions={renderBanks}
                  ></Field>
                </div>
              </div>
            </div>
          </div>
          <div className="flex-row-layout jc-center">
            <div className="row">
              <div className="col-md-12">
                <div className="uppf__fc">
                  <label htmlFor="">Numero de cuenta en Soles:</label>
                  <Field
                    className="select-bank"
                    disabled={true}
                    component={MaterialSelect}
                    name="bank"
                    placeholder="Banco"
                    renderOptions={renderBanks2}
                  ></Field>
                </div>
              </div>

            </div>
          </div>
          <h2 className="text-primary text-center">
            <b>Sube tu comprobante pago</b>
          </h2>
          <div className="spacer-20"></div>
        </>
      )}
      <div className="uppf__photos">
        <FieldArray name="photos">
          {({ fields }) =>
            fields.map((name, index) => (
              <div className="photo animated fadeIn" key={name}>
                <Field name={`${name}`} component={PhotoInput} />
              </div>
            ))
          }
        </FieldArray>
      </div>
      <div className="spacer-15"></div>
      <div className="row">

        <div className="col-md-12">
          <div className="uppf__fc">
            <label htmlFor="">
              <span>Número/Código de operación:</span>
              &nbsp;&nbsp;
              <Tooltip
                title="Para agregar más de 1, separar por comas"
                placement="top"
              >
                <AppIcon icon="info"></AppIcon>
              </Tooltip>
            </label>
            <Field
              component={MaterialInput}
              name="operationCode"
              type="text"
            ></Field>
          </div>
        </div>
        <div className="col-md-12 text-center">
          <div className="uppf__fc ">
            <p>Por ejemplo</p>
            <img alt="ejemplo" src="https://lh3.googleusercontent.com/-lm7w-Inux2vkicc_h7Dd1yUbDMUVTXYZ683tq_aLKmU94zLBjxt0wMVMIn56l8MzbIqobQJXqpCLXEpban3f_R3exisvWMbX0LTIu9uYmEA99v16x3CVqN-Ru4e=w239"></img>
          </div>
        </div>
      </div>
      <div className="flex-col-layout ai-center">
        {!onlyShowImages && (
          <>
            <button
              className="wpy-btn wpy-btn--primary wpy-btn--rounded"
              type="button"
              onClick={() => push("photos", undefined)}
            >
              Agregar otra foto
            </button>
            {!paymentInfo.uid && (
              <>
                <div className="spacer-20"></div>
                <button
                  className="wpy-btn wpy-btn--primary wpy-btn--rounded"
                  type="button"
                  onClick={() => onOtherOpportunityClick(values)}
                >
                  En otro momento
                </button>
              </>
            )}
            <div className="spacer-20"></div>
            <button className="wpy-btn wpy-btn--primary wpy-btn--rounded">
              {paymentInfo.uid ? "Actualizar" : "Enviar"}
            </button>
          </>
        )}
        {onlyShowImages && (
          <button
            type="button"
            className="wpy-btn wpy-btn--primary wpy-btn--rounded"
            onClick={() => onFinish(false)}
          >
            Cerrar
          </button>
        )}
      </div>
    </StyledUploadPaymentPhotosForm>
  );
};

const UploadPaymentPhotosForm = ({
  onSubmit,
  validate,
  initialValues,
  ...others
}) => {
  return (
    <Form
      onSubmit={onSubmit}
      validate={validate}
      render={RenderForm}
      initialValues={{ ...initialValues, ...others.paymentInfo }}
      mutators={{
        ...arrayMutators
      }}
      {...others}
    ></Form>
  );
};

export default UploadPaymentPhotosForm;
