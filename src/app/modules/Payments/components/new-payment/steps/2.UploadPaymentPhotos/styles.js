import styled from "styled-components";
import { colors } from "../../../../../../../styles/js/1-settings/colors";

export const StyledUploadPaymentPhotosForm = styled.form`
  .uppf {
    &__photos {
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      > .photo {
        padding: 10px;
        min-width: 270px;
        max-width: 270px;
        flex: 0 0 33.33%;
        > * {
          heigth: 100%;
        }
      }
    }
    &__row {
      display: flex;
      > div {
        flex: 1;
        &:first-child {
          padding-right: 80px;
        }
      }
    }
    &__fc {
      padding: 20px 0;
      width: 100%;
      &:first-child {
        padding-left: 0px;
      }
      label {
        font-weight: 500;
        color: #545454;
        position: relative;
        white-space: nowrap;
        &::before {
          content: "\u2022";
          position: absolute;
          color: ${colors.grey4};
          left: -12px;
          top: 50%;
          transform: translateY(-50%);
        }
      }
    }
  }
`;
