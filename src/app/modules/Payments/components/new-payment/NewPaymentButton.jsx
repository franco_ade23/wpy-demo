import React, { useState, Fragment } from 'react';
import NewPaymentModal from './NewPaymentModal';

const NewPaymentButton = ({ children }) => {

    const [ modalOpen, setModalOpen ] = useState(false);

    const handleClick = () => {
        setModalOpen(true);
    }

    const handleModalClose = () => {
        setModalOpen(false);
    }

    return (
        <Fragment>
            <button className="wpy-btn"
            onClick={handleClick}>{ children }</button>
            <NewPaymentModal
            open={modalOpen}
            onClose={handleModalClose}></NewPaymentModal>
        </Fragment>
    )

}

export default NewPaymentButton;