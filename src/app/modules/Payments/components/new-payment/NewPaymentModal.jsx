import React, { useState } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import NewPayment from "./NewPayment";

const NewPaymentModal = ({ open, onClose, payment, ...others }) => {
  const [disableBackdropClick, setDisableBackdropClick] = useState(false);
  return (
    <Dialog
      open={open}
      onClose={onClose}
      className="wpy-dialog"
      disableBackdropClick={disableBackdropClick}
      maxWidth={"md"}
    >
      <DialogContent className="wpy-dialog__content">
        <NewPayment
          onFinish={onClose}
          setDisableBackdropClick={setDisableBackdropClick}
          payment={payment}
          {...others}
        ></NewPayment>
      </DialogContent>
    </Dialog>
  );
};

export default NewPaymentModal;
