import React, { useState, useCallback } from "react";

import NewInvoiceForm from "./steps/1.NewInvoiceForm";
import UploadPaymentPhotos from "./steps/2.UploadPaymentPhotos";
import PaymentCreated from "./steps/999.PaymentCreated";
import { ProcessingPayment } from "./steps/ProcessingPayment";
import { useActions } from "../../../../hooks/useActions";
import { loadUserPayments } from "../../state/actions";

const componentsPerSteps = [
  {
    step: 1,
    cmp: NewInvoiceForm
  },
  {
    step: 2,
    cmp: UploadPaymentPhotos
  },
  {
    step: 3,
    cmp: PaymentCreated
  },
  {
    step: "processing",
    cmp: ProcessingPayment
  }
];

const NewPayment = ({
  onFinish,
  setDisableBackdropClick,
  payment,
  onlyShowImages
}) => {
  const [{ currentStep, newPaymentInfo }, setState] = useState({
    currentStep: payment ? 2 : 1,
    newPaymentInfo: { ...payment }
  });

  const actions = useActions(
    {
      loadUserPayments
    },
    [loadUserPayments]
  );

  const handleContinue = useCallback((nextStepNumber, paymentInfo = {}) => {
    if (nextStepNumber === 3 && currentStep !== "processing") {
      setState({
        currentStep: "processing",
        newPaymentInfo: {
          ...newPaymentInfo,
          ...paymentInfo
        }
      });
      setDisableBackdropClick(true);
      return;
    }
    setDisableBackdropClick(false);
    setState({
      currentStep: nextStepNumber,
      newPaymentInfo: {
        ...newPaymentInfo,
        ...paymentInfo
      }
    });
  }, [currentStep, newPaymentInfo, setDisableBackdropClick]);

  const handleFinish = (loadPayments = true) => {
    onFinish();
    loadPayments && actions.loadUserPayments();
  };

  return componentsPerSteps.map((Step, i) =>
    i + 1 === currentStep ||
      (currentStep === "processing" && Step.step === "processing") ? (
        <Step.cmp
          key={i}
          onContinue={handleContinue}
          onFinish={handleFinish}
          paymentInfo={newPaymentInfo}
          setDisableBackdropClick={setDisableBackdropClick}
          onlyShowImages={onlyShowImages}
        ></Step.cmp>
      ) : null
  );
};

export default NewPayment;
