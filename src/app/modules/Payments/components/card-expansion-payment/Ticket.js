import React from 'react';
import PropTypes from 'prop-types';

import LabelText from './LabelText';

const Ticket = ({ ticket }) => {
    return (
        <div className='tw-p-3'>
            <div className='tw-flex tw-justify-between tw-items-end'>
                <div>
                    <h4 style={{color:'#0052be'}} className='tw-font-bold tw-leading-none'>UPC</h4>
                    <div>
                        <LabelText label='Usuario(código)' value='u112002145'/>
                        <LabelText label='Contraseña' value='123456789' />
                    </div>
                </div>
                <div>
                    <LabelText label='Monto' value='S/ 2,000' />
                    <LabelText label='F. Vencimiento' value='02/10/2018' />
                </div>
                <div>
                    Aprobado
                </div>
            </div>
        </div>
    )
}
Ticket.propTypes = {
    ticket: PropTypes.object
}

export default Ticket;