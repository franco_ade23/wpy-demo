import React from "react";
import styled from "styled-components";
import { formatAmount } from "../../../../utils/number.util";
import TagStatus from "../../../../components/tag-status/tag-status";

const StyledInvoiceRow = styled.div`
  padding: 30px 50px;
`;

export const InvoiceRow = ({ invoice }) => {
  const { service, amount, status } = invoice;
  return (
    <StyledInvoiceRow>
      <h4 className="text-primary">
        <b>{service}</b>
      </h4>
      <div className="flex-row-layout ai-center">
        <div className="flex flex-row-layout jc-center">
          <span>
            Monto&nbsp;&nbsp;-&nbsp;&nbsp;S/&nbsp;
            {formatAmount(amount.toFixed(2))}
          </span>
        </div>
        <TagStatus status={status}></TagStatus>
      </div>
    </StyledInvoiceRow>
  );
};
