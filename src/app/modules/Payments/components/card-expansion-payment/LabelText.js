
import React from 'react';

const LabelText = ({ label, value }) => {
    return (
        <div className='tw-inline-block tw-text-xs tw-mr-2'>
            <span className='tw-inline-block tw-align-middle '>{label}</span>
            <span className='tw-inline-block tw-align-middle tw-mx-1'>-</span>
            <span className='tw-inline-block tw-align-middle tw-text-gray-700'>{value}</span>
        </div>
    )
}
export default LabelText;