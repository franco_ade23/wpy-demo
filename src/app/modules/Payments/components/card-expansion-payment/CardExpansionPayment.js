import React, { useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";
//services local
import { formatAmount } from "./../../../../utils/number.util";

//components
import TagStatus from "../../../../components/tag-status/tag-status";
import LinkTicket from "../link-ticket/LinkTicket";
import AppIcon from "../../../../components/Icon";
import { InvoiceRow } from "./InvoiceRow";

const CardExpansionPayment = ({ payment }) => {
  const [showInvoices, setShowInvoices] = useState(false);
  const { code, amount_total, status, created_at, invoices = [] } = payment;
  const total = formatAmount(amount_total);
  const parseDate = moment(created_at).format("L");

  return (
    <div
      className="bg-white tw-rounded-lg tw-py-4 tw-px-8 "
      style={{ cursor: "pointer" }}
      onClick={() => setShowInvoices(!showInvoices)}
    >
      
      <div className="tw-flex tw-justify-between tw-items-center tw-relative">
        <div className="row chart-payment">
        <div className="tw-w-1/4 flex-row-layout col-md-3 col-sm-6 ">
          <div className="arrow-nav"> 
            <AppIcon
              icon="arrow_down"
              height={10}
              style={{
                transform: showInvoices ? "rotate(180deg)" : null
              }}
            ></AppIcon>
          </div>
          
          <div className="spacer-h-20"></div>
          <div>
            <h5 className="tw-uppercase tw-text-sm tw-text-gray-800 ">{code}</h5>
            <h6 className="tw-text-sm mobile-space">Fecha de Pago: {parseDate}</h6>
          </div>
        </div>
        <div className="tw-w-1/4 col-md-3 col-sm-6 space-mobile">
          <span className="tw-text-blue-700">
            Te notificaremos cuando el pago sea realizado
          </span>
        </div>
        <div
          className="tw-w-1/4 tw-text-center col-md-3 col-sm-6 payment-pen "
          onClick={ev => ev.stopPropagation()}
        >
          <LinkTicket status={status} payment={payment} />
        </div>
        <div className="tw-w-1/4 tw-text-right col-md-3 col-sm-6 payment-state">
          <h2 className="tw-text-gray-800 tw-font-semibold payment-pay">S/ {total}</h2>
          <TagStatus status={status} />
        </div>

        </div>
        
      </div>
      {showInvoices && (
        <>
          <hr></hr>
          <div>
            {invoices.map(inv => (
              <InvoiceRow invoice={inv} key={inv.uid}></InvoiceRow>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

CardExpansionPayment.propTypes = {
  payment: PropTypes.object.isRequired
};

export default CardExpansionPayment;
