import React, { useState } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

//assets
import icon_file from "./icon-file.png";

import {
  isStatusApproved,
  isStatusPayed
} from "../../../../utils/status-checked";
import NewPaymentModal from "../new-payment/NewPaymentModal";
import { AMOUNT_TO_DEPOSIT_COMISSION_PERCENTAGE } from "../../../../constants/payments.constants";
import { useActions } from "../../../../hooks/useActions";
import { loadPaymentFiles, unloadPaymentFiles } from "../../state/actions";
import { APPROVED, PAYED } from "../../../../constants/status.constants";

const LinkTicket = ({ status, onToggleExpansion, payment }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const { uid } = payment;
  const actions = useActions({ loadPaymentFiles, unloadPaymentFiles }, []);
  const paymentImages = useSelector(
    state => state.payments.paymentsImages[uid]
  );

  const handleClick = async ev => {
    if (isStatusApproved(status) || isStatusPayed(status)) {
      return;
    }
    actions.loadPaymentFiles(uid);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
    actions.unloadPaymentFiles(uid);
  };

  const text =
    isStatusApproved(status) || isStatusPayed(status)
      ? "Ver vouchers"
      : "Adjuntar vouchers";

  const paymentInfo = {
    uid: payment.uid,
    totalAmount: payment.amount_total,
    totalAmountToDeposit:
      payment.amount_total -
      payment.amount_total * AMOUNT_TO_DEPOSIT_COMISSION_PERCENTAGE,
    bank: payment.bank.id,
    operationCode: payment.op_code,
    photos: paymentImages ? paymentImages.map(img => img.file) : []
  };

  return (
    <>
      <div
        className="tw-inline-flex tw-items-center tw-cursor-pointer"
        onClick={handleClick}
      >
        <img src={icon_file} alt="ver boleta" className="tw-mr-2 tw-w-4" />
        <span className="tw-text-blue-600 mobile-space">{text}</span>
      </div>
      <NewPaymentModal
        open={!!paymentImages && modalOpen}
        onClose={handleModalClose}
        payment={paymentInfo}
        onlyShowImages={status === APPROVED || status === PAYED}
      ></NewPaymentModal>
    </>
  );
};
LinkTicket.propTypes = {
  hasTickets: PropTypes.bool
};
export default LinkTicket;
