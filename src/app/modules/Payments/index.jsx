import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { RUTA_PAGOS } from '../../constants/routes.constants';
import PaymentsPage from './pages/PaymentsPage';

const Payments = () => {
    return (
        <Switch>
            <Route exact path={RUTA_PAGOS} component={PaymentsPage}></Route>
            <Redirect to={RUTA_PAGOS}></Redirect>
        </Switch>
    )
}

export default Payments;