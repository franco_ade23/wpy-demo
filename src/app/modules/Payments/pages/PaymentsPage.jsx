import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

//components
import ModuleLayout from "../../../components/ModuleLayout";
import NewPaymentButton from "../components/new-payment/NewPaymentButton";
import CardExpansionPayment from "../components/card-expansion-payment/CardExpansionPayment";
import LoaderContent from "../../../components/LoaderContent";
import NotFoundItems from "../../../components/NotFoundItems/NotFoundItems";
import ErrorServer from "../../../components/ErrorServer/ErrorServer";
import { loadUserPayments } from "../state/actions";

const renderTitleRight = () => {
  return (
    <div>
      <NewPaymentButton>Nuevo Pago</NewPaymentButton>
    </div>
  );
};

class PaymentsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      payments: [],
      loader: true,
      error: false
    };
  }

  componentDidMount() {
    const { loadUserPayments } = this.props;
    loadUserPayments();
  }

  contentPayments(payments) {
    const _payments = payments.map(payment => (
      <li className="tw-mb-4" key={payment.uid}>
        <CardExpansionPayment payment={payment} />
      </li>
    ));
    return <ul>{_payments}</ul>;
  }

  isLoadingContent(loader, payments, error) {
    if (loader) return <LoaderContent />;
    if (error) return <ErrorServer />;

    if (payments.length === 0) return <NotFoundItems />;
    return this.contentPayments(payments);
  }

  render() {
    const { items = [], loading, error } = this.props;
    return (
      <>
        <ModuleLayout
          title="Pagos"
          iconUrl="/images/dashboard/payment-menu-icon.png"
          renderTitleRight={renderTitleRight}
        ></ModuleLayout>
        <div className="pad-as-container">
          {this.isLoadingContent(loading, items, error)}
        </div>
      </>
    );
  }
}

export default connect(
  state => ({ ...state.payments.paymentsList }),
  dispatch => bindActionCreators({ loadUserPayments }, dispatch)
)(PaymentsPage);
