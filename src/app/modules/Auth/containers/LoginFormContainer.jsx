import React, { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Form } from "react-final-form";
import { validateForm, required, email } from "redux-form-validators";

import LoginForm from "../components/LoginForm";
import { login, loginWithProvider } from "../state/actions";
import { withToastService } from "../../Toasts/hoc/withToastService";

const loginValidate = validateForm({
  email: [email()],
  password: [required()]
});

const Login = ({ login, loginWithProvider, toastService }) => {
  const [loading, setLoading] = useState(false);

  const loginCb = err => {
    setLoading(false);
    if (!!err) {
      toastService.error(err.message, "Login");
    }
  };

  const handleLogin = credentials => {
    setLoading(true);
    login(credentials, loginCb);
  };

  const handleSocials = (provider, providerResponse) => {
    setLoading(true);
    loginWithProvider({ providerName: provider, providerResponse }, loginCb);
  };

  return (
    <Form
      onSubmit={handleLogin}
      handleSocials={handleSocials}
      render={LoginForm}
      validate={loginValidate}
      loading={loading}
    ></Form>
  );
};

export default withToastService(
  connect(
    null,
    dispatch =>
      bindActionCreators(
        {
          login: login,
          loginWithProvider: loginWithProvider
        },
        dispatch
      )
  )(Login)
);
