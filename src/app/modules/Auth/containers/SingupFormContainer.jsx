import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form } from 'react-final-form'
import { validateForm, required, email, length, acceptance } from 'redux-form-validators';

import SignupForm from '../components/SignupForm';
import moment from 'moment';
import { signup } from '../state/actions';
import { withToastService } from '../../Toasts/hoc/withToastService';

const signupFormValidate = validateForm({
    firstName: [ required() ],
    lastName: [ required() ],
    birthdate: [ required() ],
    dni: [ required(), length({ max: 8, min: 8 }) ],
    //district: [ required() ],
    cellphone: [ required() ],
    // address: [ required() ],
    email: [ email() ],
    password: [ required() ],
    acceptTermsAndConditions: [ acceptance() ]
});

const SingupFormContainer = ( { signup, toastService } ) => {

    const [ loading, setLoading ] = useState(false);

    const signupCb = ( err ) => {
        setLoading(false);
        if( !!err ){
            toastService.error( err.message ,'Registro');
        }
    }    

    const handleSignupSubmit = ( signupInfo ) => {
        setLoading(true);
        signup( { 
            ...signupInfo,
            birthdate: moment(signupInfo.moment).format('YYYY-MM-DD')
        }, signupCb );
    }

    return (
        <Form 
            onSubmit={handleSignupSubmit}
            render={SignupForm}
            validate={signupFormValidate}
            loading={loading}>
        </Form>
    );
}

export default withToastService(
    connect(
        null,
        dispatch => bindActionCreators({
            signup: signup
        },dispatch)
    )(SingupFormContainer)
);