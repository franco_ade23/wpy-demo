import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Field } from "react-final-form";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import { RUTA_REGISTRO } from "../../../constants/routes.constants";
import Input from "../../../components/Input";
import { withToastService } from "../../Toasts/hoc/withToastService";
import { Messages } from "../../../constants/messages.constants";

const StyledLoginForm = styled.div`
  .title {
    font-weight: bold;
  }
  .jusfify-center{
    justify-content:center
  }
  .login {
    &__facebookBtn {
      background-color: #3241b0 !important;
      border-color: #3241b0 !important;
    }
    &__gmailBtn {
      background-color: #d54949 !important;
      border-color: #d54949 !important;
    }
  }
`;

const LoginForm = ({
  handleSubmit,
  handleSocials,
  valid,
  toastService,
  loading
}) => {
  const _handleSubmit = (...args) => {
    if (!valid) {
      toastService.warn(Messages.AUTH_EXISTS_INVALID_FIELDS);
    }
    return handleSubmit(...args);
  };
  return (
    
    <StyledLoginForm>
      <p className="title wpy-header-3 text-black">Empieza Ahora</p>
      <p>
        <span className="text-muted">No tienes una cuenta?</span>&nbsp;
        <Link className="wpy-link" to={RUTA_REGISTRO}>
          <b>Registrate aquí</b>
        </Link>
      </p>
      <div className="spacer-30"></div>
      <form onSubmit={_handleSubmit}>
        <div>
          <Field
            component={Input}
            name="email"
            type="email"
            className="fill-width"
            placeholder="Dirección de correo electrónico"
          ></Field>
        </div>
        <div className="spacer-15"></div>
        <div>
          <Field
            component={Input}
            name="password"
            type="password"
            className="fill-width"
            placeholder="Contraseña"
          ></Field>
        </div>
        <div className="spacer-30"></div>
        <div>
          <button className="wpy-btn fill-width" disabled={loading}>
            Iniciar sesión
          </button>
        </div>
      </form>
      <div className="spacer-30"></div>
      <p className="text-muted">Conéctate también con</p>
      <div className="spacer-20"></div>
      <div className="flex-row-layout jusfify-center">
        <FacebookLogin
          appId="1287026248138268"
          callback={fbResponse => handleSocials("FACEBOOK", fbResponse)}
          render={renderProps => (
            <button
              className="wpy-btn login__facebookBtn avenir-roman"
              onClick={renderProps.onClick}
            >
            <i className="fa fa-facebook pr-2" aria-hidden="true"></i>
              Facebook
            </button>
          )}
        />
        {/* <button
          className="wpy-btn login__gmailBtn avenir-roman"
          onClick={() => handleSocials("GMAIL")}
        >
          Gmail
        </button> */}
      </div>
    </StyledLoginForm>
  );
};

export default withToastService(LoginForm);
