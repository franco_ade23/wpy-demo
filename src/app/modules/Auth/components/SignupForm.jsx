import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Field } from 'react-final-form'

import { RUTA_LOGIN } from '../../../constants/routes.constants';

import ReduxCheckbox from '../../../components/Checkbox';
import Input from '../../../components/Input';
import { withToastService } from '../../Toasts/hoc/withToastService';
import { Messages } from '../../../constants/messages.constants';
import Datepicker from '../../../components/Datepicker';

const AcceptTermsAndConditionsCheckBox = ({ input }) => {
    const { value, onChange } = input;
    return (
        <div className="flex-row-layout">
            <ReduxCheckbox input={input}></ReduxCheckbox>
            <div className="spacer-h-10"></div>
            <label onClick={()=>{onChange(!value)}}>
            <a target="_blank"href="https://drive.google.com/file/d/10FfhrFVXh306Xo01Bhw91-73volp8abr/view">Confirmo que he leido todos los términos y condiciones de WePayU y que soy mayor de edad.</a>
            </label>
        </div>
    )
}

const StyledForm = styled.form`
    .fields>*:not(:last-child){
        margin-bottom: 15px;
    }
`;

const SignupForm = ({ className, handleSubmit, values, errors, valid, toastService, loading }) => {
    const _handleSubmit = ( ...args ) => {
        if( !valid ){
            toastService.warn(Messages.AUTH_EXISTS_INVALID_FIELDS)
            if( !values.acceptTermsAndConditions ){
                toastService.warn('Debes aceptar los términos y condiciones');
            }
        }
        handleSubmit( ...args );
    }
    return (
        <div className={className}>
            <p className="text-bold wpy-header-3 text-black">
                Regístrate
            </p>
            <p>
                Ya tienes una cuenta? <b><Link to={RUTA_LOGIN} className="wpy-link">Iniciar Sesión</Link></b>
            </p>
            <div className="spacer-30"></div>
            
            <StyledForm onSubmit={_handleSubmit}>
                <div className="fields">
                    <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Input} name="firstName" className="fill-width" placeholder="Nombres"></Field>
                        </div>
                        <div className="spacer-h-20"></div>
                        <div className="flex">
                            <Field component={Input} name="lastName" className="fill-width" placeholder="Apellidos"></Field>
                        </div>
                    </div>
                    <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Datepicker} name="birthdate" className="fill-width" placeholder="Fecha de nacimiento"></Field>
                        </div>
                    </div>
                    {/* <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Input} type="number" name="dni" className="fill-width" placeholder="Doc de Identidad"></Field>
                        </div>
                    </div> */}
                    <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Input} type="number" name="dni" className="fill-width" placeholder="Doc de Identidad"></Field>
                        </div>
                        <div className="spacer-h-20"></div> 
                        <div className="flex">
                            <Field component={Input} type="number" name="cellphone" className="fill-width" placeholder="Celular"></Field>
                        </div>
                    </div>
                    {/* <div className="flex-row-layout">
                         <div className="flex">
                            <Field component={Input} name="district" className="fill-width" placeholder="Distrito"></Field>
                        </div>
                        <div className="spacer-h-20"></div> 
                        <div className="flex">
                            <Field component={Input} type="number" name="cellphone" className="fill-width" placeholder="Celular"></Field>
                        </div>
                    </div> */}
                    {/* <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Input} name="address" className="fill-width" placeholder="Dirección"></Field>
                        </div>
                    </div> */}
                    <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Input} type="email" name="email" className="fill-width" placeholder="Dirección de correo electrónico"></Field>
                        </div>
                    </div>
                    <div className="flex-row-layout">
                        <div className="flex">
                            <Field component={Input} type="password" name="password" className="fill-width" placeholder="Contraseña"></Field>
                        </div>
                    </div>
                </div>
                <div className="spacer-30"></div>
                <div>
                    <Field component={AcceptTermsAndConditionsCheckBox} name="acceptTermsAndConditions"></Field>
                </div>
                <div className="spacer-30"></div>
                <button className="wpy-btn fill-width" disabled={loading}>
                    Continuar
                </button>
            </StyledForm>
        </div>
    );
}

export default withToastService(SignupForm);