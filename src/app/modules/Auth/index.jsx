import React from 'react';
import { Switch, Route } from 'react-router-dom';
import LoginView from './pages/LoginView';
import styled from 'styled-components';
import SignupView from './pages/SignupView';
import { RUTA_LOGIN, RUTA_REGISTRO ,RUTA_LANDING} from '../../constants/routes.constants';

import { getBackgroundImageStyle } from '../../utils/background-image.util';

// const StyledHomePage = styled.div`
//     .img-login{
//         //background-image: url('/images/auth-bg.png');
//     }
// `; 

const Auth = ({ className, match }) => {
    let authBgStyle = getBackgroundImageStyle({
        backgroundImageUrl: '/images/auth-bg.png'
    });
    return (
        <div className={`page no-scroll flex-col-layout jc-center img-login ${className}`}
        style={ authBgStyle } >
            <br></br>
            <img className="mx-auto animated fadeIn" src="/images/logo-wepayu.png" alt="" width="175"/>
            <div className="spacer-45 mobile-space"></div>
            <div className="flex-row-layout jc-center">
                <Switch>
                    <Route exact path={RUTA_LOGIN} component={LoginView}></Route>
                    <Route exact path={RUTA_REGISTRO} component={SignupView}></Route>
                </Switch>
            </div>
        </div>
    )
}

export default styled(Auth)`
`;