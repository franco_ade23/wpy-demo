import api from "../../../services/api.service";
import { NO_PROFILE } from "../../../constants/profile.constants";

export const AuthServices = {
  login: (email, password) =>
    api.post("user/login", {
      email,
      password
    }),
  signup: signupInfo =>
    api.post("user/create", {
      ...signupInfo,
      type_client: NO_PROFILE,
      first_name: signupInfo.firstName,
      last_name: signupInfo.lastName
    }),
  getUserInfo: () => api.get("user/"),
  loginWithFacebook: ({ accessToken }) =>
    api.post("user/login/mobile/facebook", {
      access_token: accessToken
    })
};
