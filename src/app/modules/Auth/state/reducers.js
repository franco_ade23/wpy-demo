import { authState } from "./state";
import { StorageKeys } from "../../../constants/storage.constans";
import { SAVE_TOKEN, SAVE_USER_INFO, LOGOUT, CLEAR_TOKEN } from "./action-types";
//LOGIN_SUCCESSFUL, SIGNUP_SUCCESSFUL, LOGIN_REQUESTED, SINGUP_REQUESTED, LOGIN_ERROR, 

export const authReducer = ( state = authState, action ) => {
    switch( action.type ){
        case SAVE_TOKEN:
            localStorage.setItem(StorageKeys.AUTHENTICATION_TOKEN,action.payload.token);
            return state;
        case CLEAR_TOKEN:
            localStorage.removeItem(StorageKeys.AUTHENTICATION_TOKEN);
            return state;
        case SAVE_USER_INFO:
            return {
                ...state,
                user: action.payload.user,
                logged: true
            }
        case LOGOUT:
            return {
                ...state,
                user: null,
                logged: false
            }
        default:
            return state;
    }
}