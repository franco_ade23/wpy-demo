export const authState = {
    logged: false,
    user: null,
    loginLoading: false,
    signupLoading: false
}