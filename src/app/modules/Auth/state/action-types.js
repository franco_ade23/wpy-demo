export const SAVE_TOKEN= 'SAVE_TOKEN';
export const CLEAR_TOKEN = 'CLEAR_TOKEN';
export const SAVE_USER_INFO = 'SAVE_USER_INFO';
export const LOGOUT = 'LOGOUT';