import { AuthServices } from "../services";
import {
  SAVE_TOKEN,
  SAVE_USER_INFO,
  LOGOUT,
  CLEAR_TOKEN
} from "./action-types";
import { User } from "../../../models/user.model";
//SHOW_MODULE_ERROR ,  LOGIN_SUCCESSFUL, SIGNUP_SUCCESSFUL, LOGIN_REQUESTED, SINGUP_REQUESTED, LOGIN_ERROR, SIGNUP_ERROR,
export const login = ({ email, password }, cb) => {
  return async dispatch => {
    try {
      const loginResponse = await AuthServices.login(email, password);
      dispatch({ type: SAVE_TOKEN, payload: { token: loginResponse.token } });
      cb(null);
      dispatch(populate());
    } catch (err) {
      console.error(err);
      dispatch({ type: CLEAR_TOKEN });
      cb(new Error(err.details ? err.details[0].message : "Error en login"));
    }
  };
};

export const loginWithProvider = (
  { providerName, providerResponse: { accessToken } },
  cb
) => {
  return async dispatch => {
    try {
      if (providerName === "FACEBOOK") {
        const loginResponse = await AuthServices.loginWithFacebook({
          accessToken: accessToken
        });
        dispatch({ type: SAVE_TOKEN, payload: { token: loginResponse.token } });
        cb(null);
        dispatch(populate());
      }
    } catch (err) {
      console.error(err);
      dispatch({ type: CLEAR_TOKEN });
      cb(new Error(err.details ? err.details[0].message : "Error en login"));
    }
  };
};

export const loginWithGmail = () => {
  return Promise.resolve();
};

export const signup = (signupInfo, cb) => {
  return async dispatch => {
    try {
      const signupResponse = await AuthServices.signup(signupInfo);
      dispatch({ type: SAVE_TOKEN, payload: { token: signupResponse.token } });
      cb(null);
      dispatch(populate());
    } catch (err) {
      console.error(err);
      dispatch({ type: CLEAR_TOKEN });
      cb(new Error(err.details ? err.details[0].message : "Error en registro"));
    }
  };
};

export const populate = cb => {
  return async dispatch => {
    try {
      const userInfoResponse = await AuthServices.getUserInfo();
      dispatch({
        type: SAVE_USER_INFO,
        payload: { user: new User(userInfoResponse) }
      });
      !!cb && cb(null);
    } catch (err) {
      dispatch({ type: CLEAR_TOKEN });
      !!cb && cb(err);
    }
  };
};

export const logout = () => {
  return dispatch => {
    dispatch({ type: LOGOUT });
    dispatch({ type: CLEAR_TOKEN });
  };
};
