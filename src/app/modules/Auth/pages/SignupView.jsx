import React from 'react';
import styled from 'styled-components';
import SingupFormContainer from '../containers/SingupFormContainer';

const SignupView = ({ className }) => {
    return (
        <div className={`animated fadeIn ${className}`}>
            <div className="signup__left">
                <p className="signup__left__title">
                    LOS MEJORES <br/>
                    BENEFICIOS
                </p>
                <p className="signup__left__subtitle">
                    Obtén efectivo de tu tarjeta de crédito <br/>
                    a una comisión justa y gana dinero <br/>
                    por pagar tu universidad
                </p>
            </div>
            <div className="signup__right">
                <SingupFormContainer></SingupFormContainer>
            </div>
        </div>
    )
}

export default styled(SignupView)`
    border-radius: 45px;
    display: flex;
    max-width: 1011px;
    min-height: 608px;
    flex: 0  0 100%;
    overflow: hidden;
    background-image: url(/images/signup-left-bg.png);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    .signup{
        &__left, &__right{
            padding: 50px;
            flex: 0 0 50%;
        }
        &__right{
            background-color: #FFFFFF;
        }
        &__left{
            font-size: 19px;
            color: #FFFFFF;
            &__title{
                line-height: 1.2;
                font-size: 2.5em;
                font-weight: 900;
                margin-bottom: 30px;
            }
            &__subtitle{

            }
        }
    }
`;