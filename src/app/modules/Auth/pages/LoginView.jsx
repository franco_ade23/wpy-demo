import React from 'react';
import styled from 'styled-components';
import Login from '../containers/LoginFormContainer';
import '../pages/styleLogin.css'

const LoginView = ({ className }) => {
    return (
        <div className={`animated fadeIn ${className}`}>
            <div className="login__left">
                <p className="login__left__title text-letter-sp-1">
                    DESCRUBRE <br/>
                    AHORRA <br/>
                    RETIRA
                </p>
                <p className="login__left__subtitle avenir-roman text-letter-sp-normal">
                    Obtén dinero y ahorra en tus <br/>
                    pagos universitarios
                </p>
            </div>
            <div className="login__right flex-col-layout jc-center">
                <Login></Login>
            </div>
        </div>
    )
}

export default styled(LoginView)`
    border-radius: 45px;
    display: flex;
    max-width: 1011px;
    min-height: 503px;
    flex: 0  0 100%;
    overflow: hidden;
    background-image: url(/images/login-left-bg.png);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    .login{
        &__left, &__right{
            padding: 50px;
            flex: 0 0 50%;
        }
        &__right{
            background-color: #FFFFFF;
        }
        &__left{
            font-size: 19px;
            color: #FFFFFF;
            &__title{
                line-height: 1.2;
                font-size: 2.5em;
                font-weight: 900;
                margin-bottom: 30px;
            }
            &__subtitle{

            }
        }
    }
`;