import React, { useState, Fragment } from 'react';
import ConfirmDialog from '../components/ConfirmDialog';

const withConfirm = ( Component ) => {
    
    return ( props ) => {
        
        const state = { 
            openModal: false, 
            message: '', 
            title: '',
            modalConfig: {},
            handleConfirmModalClose: () => {}
        };

        const [ currentState, setState ] = useState(state);

        const openConfirmModal = ( message, title, modalConfig ) => {
            return new Promise((resolve)=>{
                setState({
                    ...currentState,
                    openModal: true,
                    message: message,
                    title: title,
                    modalConfig: modalConfig,
                    handleConfirmModalClose: ( confirm ) => {
                        resolve(confirm);
                        setState({
                            ...currentState,
                            openModal: false,
                            handleConfirmModalClose: () => {}
                        });
                    }
                });
            });;
        }

        return (
            <Fragment>
                <Component {...props} openConfirmModal={openConfirmModal}></Component>
                <ConfirmDialog
                open={currentState.openModal}
                onClose={currentState.handleConfirmModalClose}
                message={currentState.message}
                title={currentState.title}
                {...currentState.modalConfig}></ConfirmDialog>
            </Fragment>
        )
    }

}

export default withConfirm;