import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { logout } from '../modules/Auth/state/actions';

const _doLogout = ( Component ) => {
    return ({ history, doLogout, ...others }) => {
        const logout = ( ) => {
            doLogout();
            history.push('/auth');
        }
        return <Component logout={logout} {...others}></Component>
    }
}

const _routered = ( Component ) => {
    return withRouter( _doLogout(Component) );
}

const withLogout = ( Component ) => {
    return connect(
        null,
        dispatch => bindActionCreators({ doLogout: logout },dispatch)
    )( _routered( Component ) )
}

export {
    withLogout
}