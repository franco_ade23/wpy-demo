import React, { useEffect, useState, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { populate } from '../modules/Auth/state/actions';
//import Username from './Username';
import '../components/components.css'
// import { Spinner } from 'react-redux-spinner';
import Loader from 'react-loader-spinner'

const Initialize = ({ children, populate }) => {

    const [initialized, setInitialized] = useState(false);

    const populateCb = (err) => {
        setInitialized(true);
    }

    useEffect(() => {
        populate(populateCb);
    }, [populate]);

    return (
        initialized ?
            <Fragment>
                {children}
            </Fragment> :
            <Fragment>
                <div className="spinner-logo">

                    <img src="/images/logo-wepayu.png" width="350px" alt='logo wepayu'></img>

                    <div>
                        <Loader
                            type="TailSpin"
                            color="#156fe9"
                            height={80}
                            width={80}
                        />
                    </div>

                </div>
                <div className="spinner-logo-mobile">

                    <img src="/images/logo-wepayu.png" width="150px" alt='logo wepayu'></img>
                    <Loader
                        className="spinner-responsive"
                        type="TailSpin"
                        color="#156fe9"
                        height={50}
                        width={50}
                    />
                </div>
            </Fragment>


    )

}

export default connect(
    null,
    dispatch => bindActionCreators({ populate }, dispatch)
)(Initialize);

