import React from 'react';
import TextField from '@material-ui/core/TextField';

const MaterialInput = ({ input, meta, className, ...others }) => {
    const isInvalid = meta.invalid && meta.touched;
    return (
        <div className="form-input">
            <TextField
            className="fill-width"
            {...input}
            {...others}
            error={isInvalid}/>
        </div>
    );
}

export default MaterialInput;