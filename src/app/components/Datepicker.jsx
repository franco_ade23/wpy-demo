import React, { useState } from 'react';
import moment from 'moment';

import '../constants/date.constants';

const renderValue = (value, inputType) => {
	if (inputType === 'date') {
		return value;
	} else {
		return !value || value === '' ? '' : moment(value).format('DD/MM/YYYY');
	}
}

const onInputFocus = (value, setInputType, onChange) => {
	setInputType('date');
}

const onInputBlur = (value, setInputType, onChange, onBlur) => {
	setInputType('text');
	onBlur();
}

const Datepicker = ({ input, meta, className, placeholder }) => {
	const [inputType, setInputType] = useState('text');
	const invalidClassName = meta.invalid && meta.touched ? 'invalid' : '';
	const formClassName = `form-input ${className} ${invalidClassName}`;
	return (
		<div className={formClassName}>
			<input className="fill-width"
				{...input}
				placeholder={placeholder}
				type={inputType}
				value={renderValue(input.value, inputType)}
				onFocus={() => onInputFocus(input.value, setInputType, input.onChange)}
				onBlur={() => onInputBlur(input.value, setInputType, input.onChange, input.onBlur)} />
		</div>
	);
}

export default Datepicker;