import React from 'react';
import { WITHDRAWER_PROFILE } from '../constants/profile.constants'

const RouteWithdraw = ({ typeClient, children }) => {
    console.log('ROUTEWITHDRAW');
    if (typeClient === WITHDRAWER_PROFILE) {
        return children
    }
    return null
}

export default RouteWithdraw;