import React from "react";
import styled from "styled-components";

import icon from "./error-server.png";

const StyledContainer = styled.div`
  background-color: #fff;
  border-radius: 4px;
  text-align: center;
  padding: 4rem 1rem;
  img {
    width: 50%;
    max-width: 260px;
    display: inline-block;
  }
  p {
    font-weight: 600;
    color: #f44336;
    text-transform: uppercase;
    letter-spacing: 2px;
    max-width: 480px;
    margin: 10px auto 0 auto;
  }
`;

const ErrorServer = () => {
  return (
    <StyledContainer>
      <img src={icon} alt="error server" />
      <p>
        Estamos teniendo problemas con nuestro Servidor, inténtelo en otro
        momento, Gracias!
      </p>
    </StyledContainer>
  );
};

export default ErrorServer;
