import React from 'react';
import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
// import DialogTitle from '@material-ui/core/DialogTitle';
// import Button from '@material-ui/core/Button';

const ConfirmDialog = ({ open, onClose, message, title, confirmText, cancelText }) => {

    return (
        <Dialog 
        open={open}
        disableBackdropClick={true}
        onBackdropClick={()=>onClose(false)}>
            <DialogContent className="p-20 text-center">
                {
                    !!title &&
                    <h2>
                        <b>{title}</b>
                    </h2>
                }
                <p>
                    { message }
                </p>
                <div className="spacer-30"></div>
                <div className="flex-row-layout jc-center">
                    <button onClick={()=>onClose(false)} className="wpy-btn wpy-btn--warn wpy-btn--text">
                        { cancelText || 'Cancelar' }
                    </button>
                    <div className="spacer-h-10"></div>
                    <button onClick={()=>onClose(true)} className="wpy-btn wpy-btn--text" autoFocus>
                        { confirmText || 'Aceptar' }
                    </button>
                </div>
            </DialogContent>
        </Dialog>
    );
}

export default ConfirmDialog;