import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import {displayStatusHuman, displayStatusColor} from "../../utils/status-checked";


const StyledStatusName = styled.span`
    color: ${props => displayStatusColor(props.status)};
    font-size: 12px;
    letter-spacing: 0.03rem;
`;


const TagStatus = ({status}) => {
    return (
        <div className='tw-inline-block py-5'>
            <div className='tw-flex tw-items-center'>
                <StyledStatusName status={status}>
                    {displayStatusHuman(status)}
                </StyledStatusName>
            </div>
        </div>
    )
};

TagStatus.propTypes = {
    status: PropTypes.string.isRequired
};


export default TagStatus;