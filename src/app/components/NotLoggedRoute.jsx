import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { RUTA_DASHBOARD } from '../constants/routes.constants';

const NotLoggedRoute = ({ logged, ...others }) => {
    if (logged) {
        return <Redirect to={RUTA_DASHBOARD}></Redirect>
    } else {
        return <Route {...others}></Route>
    }
}

export default connect(
    state => ({ logged: state.auth.logged })
)(NotLoggedRoute);