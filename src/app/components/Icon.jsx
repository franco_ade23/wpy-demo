import React from "react";

const AppIcon = React.forwardRef(({ icon, height, ...others }, ref) => {
  const iconPath = require(`./../../assets/icons/icon_${icon}.svg`);
  return (
    <img src={iconPath} alt="" height={height || 15} {...others} ref={ref} />
  );
});

export default AppIcon;
