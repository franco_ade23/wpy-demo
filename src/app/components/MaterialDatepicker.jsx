import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';

const renderValue = (value,inputType) => {
  if( inputType === 'date' ){
    return value;
  }else{
    return !value || value === '' ? '' : moment(value).format('DD/MM/YYYY');
  }
}

const onInputFocus = (value, setInputType, onChange) => {
  setInputType('date');
}

const onInputBlur = (value ,setInputType, onChange, onBlur) => {
  setInputType('text');
  onBlur();
}

const MaterialDatepicker = ({ input, meta, className, ...others }) => {

    const [ inputType, setInputType ] = useState('text');
    const isInvalid = meta.invalid && meta.touched;

    return (
        <div className="form-input">
            <TextField
            className="fill-width"
            {...input}
            {...others}
            type={inputType}
            error={isInvalid}
            value={ renderValue(input.value,inputType) }
            onFocus={()=>onInputFocus(input.value,setInputType,input.onChange)}
            onBlur={()=>onInputBlur(input.value,setInputType,input.onChange,input.onBlur)}/>
        </div>
    );
}

export default MaterialDatepicker;