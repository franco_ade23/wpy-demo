import React from 'react';
import styled from 'styled-components';

const Logo = ({ className, variant,...props }) => {
    let src = '/images/logo-wepayu.png';
    switch( variant ){
        case 'white':
            src = '/images/logo-wepayu--white.png';
            break;
        case 'gray':
            src = '/images/logo-wepayu--gray.png';
            break;
        default:break;
    }
    return (
        <img src={src} alt='logo' className={className} {...props}/>
    )
}

export default styled(Logo)``;