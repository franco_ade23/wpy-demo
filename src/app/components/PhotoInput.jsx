import React from "react";
import styled from "styled-components";

const DEFAULT_UPLOAD_MESSAGE =
  "Arrastra o Haz clic para cargar \n en la imagen";

const StyledPhotoInput = styled.div`
  box-shadow: 0 0 9px 0 rgba(12, 12, 12, 0.3);
  background-color: #f4f7f9;
  background-position: center;
  background-size: auto 70%;
  background-repeat: no-repeat;
  border-radius: 13px;
  padding: 30px;
  text-align: center;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  .photoInput {
    &__icon {
      width: 50px;
      margin-bottom: 16px;
    }
    &__text {
      white-space: pre-wrap;
    }
    &__value {
      word-break: break-all;
      margin-top: 15px;
      border-radius: 4px;
      padding: 8px 16px;
      border: 1px solid rgba(0, 0, 0, 0.2);
    }
  }
  input {
    display: none;
  }
  &.invalid {
    background-color: rgba(255, 109, 124, 0.3);
  }
`;

const PhotoInput = ({ input, meta, text = DEFAULT_UPLOAD_MESSAGE }) => {
  let fileInputElm = null;
  let photoInputElm = null;

  const isInvalid = meta.invalid && meta.touched;
  const className = isInvalid ? "invalid" : "";
  const isFileFromBackend = !!input.value && !(input.value instanceof File);

  const handleClick = () => {
    if (isFileFromBackend) return;
    input.onBlur();
    fileInputElm.click();
  };

  const handleFileChange = ev => {
    let file = ev.target.files[0];
    if (!!file) {
      input.onChange(file);
    }
  };

  const handlePhotoInputRef = c => {
    if (!c) return;
    photoInputElm = c;
    photoInputElm.addEventListener("dragover", function(e) {
      e.stopPropagation();
      e.preventDefault();
      e.dataTransfer.dropEffect = "copy";
    });
    photoInputElm.addEventListener("drop", e => {
      e.stopPropagation();
      e.preventDefault();
      const files = e.dataTransfer.files;
      input.onChange(files[0]);
    });
  };

  return (
    <StyledPhotoInput
      onClick={handleClick}
      className={className}
      ref={handlePhotoInputRef}
      style={{
        backgroundImage: isFileFromBackend ? `url(${input.value})` : null,
        height: isFileFromBackend ? "255px" : "100%"
      }}
    >
      {!isFileFromBackend && (
        <img
          className="photoInput__icon"
          src="/images/common/upload-file-icon.png"
          alt=""
        />
      )}
      {!isFileFromBackend && <p className="photoInput__text">{text}</p>}
      {!!input.value && !isFileFromBackend && (
        <p className="photoInput__value">{input.value.name}</p>
      )}
      <input
        type="file"
        ref={c => (fileInputElm = c)}
        onChange={handleFileChange}
      />
    </StyledPhotoInput>
  );
};

export default PhotoInput;
