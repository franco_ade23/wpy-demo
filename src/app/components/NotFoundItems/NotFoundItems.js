import React from "react";
import styled from 'styled-components';
import PropTypes from 'prop-types';

import icon from './NotFoundItems.png';

const StyledContainer = styled.div`
  background-color:#fff;
  border-radius: 4px;
  text-align: center;
  padding: 2rem 1rem;
  img{
    width: 170px;
    display: inline-block;
  }
  p{
    margin-top: 10px;
  }
`;

const NotFoundItems = ({message}) => {
    return (
        <StyledContainer>
            <img src={icon} alt='not found'/>
            <p>{message}</p>
        </StyledContainer>
    )
};

NotFoundItems.propTypes = {
    message: PropTypes.string.isRequired
};

NotFoundItems.defaultProps = {
    message: 'No hay datos'
};


export default NotFoundItems;