import React from 'react';
import PropTypes from 'prop-types';
import styled from "styled-components";
import Loader from 'react-loader-spinner';

const size = {
    sm:'2',
    md:'4',
    lg:'6',
    xl:'8',
    full:'full'
};

const StyledWrapper = styled.section`
  background-color: #FFF;
  display: block;
  border-radius: 4px;
  ${props => {
          if(props.size === size['full']) return `width:100%;height:100%;`;
          return `padding:${size[props.size]}em`;
        }
   }
  &>div{
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: ${props => (props.mode === 'horizontal') ? 'row' : 'column'};
    ${props => (props.size === size['full']) ? `width:100%;height:100%;` : ''};
    
  }
  .message{
    ${props => (props.mode === 'horizontal') ? `margin-left:20px` : `margin-top:20px`};
    display: inline-block;
    line-height: 1;
    text-transform: uppercase;
    letter-spacing: 0.025rem;
    font-size: 14px;
    font-weight: 400;
    color: #404040;
  }

`;

const LoaderContent = ({message, mode,size}) => {

    return (
        <StyledWrapper mode={mode} size={size}>
            <div>
                <Loader
                    type="TailSpin"
                    color="#0051c0"
                    height={30}
                    width={30}
                    timeout={3000}

                />
                <span className='message'>{message}</span>
            </div>
        </StyledWrapper>
    );
};

/*
* mode: horizontal || vertical
* size: sm,md,lg,xl,full, full take parent height
* */
LoaderContent.propTypes = {
    message: PropTypes.string,
    mode: PropTypes.string,
    size: PropTypes.string
};

LoaderContent.defaultProps = {
    message: 'Cargando Datos ...',
    mode: 'vertical',
    size: 'md'
};

export default LoaderContent;