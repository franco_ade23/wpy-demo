import React from 'react';

const Input = ({ input, meta, className, placeholder }) => {
    const invalidClassName = meta.invalid && meta.touched ? 'invalid' : '';
    const formClassName = `form-input ${className} ${invalidClassName}`;
    return (
        <div className={formClassName}>
            <input className="fill-width" {...input} placeholder={placeholder}/>
        </div>
    );
}

export default Input;