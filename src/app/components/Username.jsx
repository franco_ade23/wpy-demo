import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const Username = ({ user }) => {
  if (!user) {
    console.warn("Intento de uso de componente protegido. Redirigiendo...");
    return <Redirect to="/auth"></Redirect>;
  }
  return <span>{`${user.firstName} `}</span>;
};

export default connect(state => ({ user: state.auth.user }))(Username);
