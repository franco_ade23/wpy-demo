import React from 'react';
import { HeaderWithDraw } from './styles'

const HeaderWithDraw = ({ children, title, urlImage  }) => {
    return (
        <HeaderWithDraw>
            <HeaderWithDraw__left>
                <HeaderWithDraw__Icon src={urlImage} />
                <HeaderWithDraw__Title>{title}</HeaderWithDraw__Title>
            </HeaderWithDraw__left>
            <HeaderWithDraw__right>
                {children}
            </HeaderWithDraw__right>
        </HeaderWithDraw>
    );
}

export default HeaderWithDraw;