import React from 'react';
import styled from 'styled-components';
import { colors } from '../../styles/js/1-settings/colors';

/**
 * Componente para utilizar en conjunto con redux-form
 * @param {*} field 
 */
const Checkbox = ( { className, input } ) => {
    const { value, onChange } = input;
    return (
        <div className={className}>
            <span className={`checkmark ${value? 'checked':''}`} onClick={()=>onChange(!value)}></span>
        </div>
    )
};

export default styled(Checkbox)`
    position: relative;
    .checkmark{
        cursor: pointer;
        width: 22px;
        height: 22px;
        border-radius: 6px;
        border: solid 1px ${colors.grey3};
        display: inline-block;
        position: relative;
        &.checked{
            &::after{
                content: '';
                position: absolute;
                left: 10%;
                top: 10%;
                height: 80%;
                width: 80%;
                border-radius: 6px;
                background-color: ${colors.accentColor};
            }
        }
    }
`;