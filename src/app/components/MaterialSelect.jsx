import React from 'react';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import { SELECT_DEFAULT_VALUE } from '../constants/forms.contstants';

const MaterialSelect = ({ input, meta, placeholder, renderOptions, ...others }) => {
    const isInvalid = meta.invalid && meta.touched;
    return (
        <FormControl error={isInvalid}
        className="fill-width">
            <Select
            {...input}
            {...others}>
                <MenuItem value={SELECT_DEFAULT_VALUE} disabled>{placeholder}</MenuItem>
                {
                    renderOptions()
                }
            </Select>
      </FormControl>
    )
}

export default MaterialSelect;