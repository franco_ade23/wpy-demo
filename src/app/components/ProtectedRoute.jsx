import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import { RUTA_LOGIN } from '../constants/routes.constants';

const ProtectedRoute = ({ logged, ...others }) => {
    console.log('Protected Route');
    if (!logged) {
        return <Redirect to={RUTA_LOGIN}></Redirect>
    } else {
        return <Route {...others}></Route>
    }
}

export default connect(
    state => ({ logged: state.auth.logged })
)(ProtectedRoute);