import React from 'react';
import styled from 'styled-components';

const StyledModuleTitle = styled.div`
    img{
        margin-right: 20px;
        width: 40px;
    }
    .moduleTitle{
        &__label{
            color: #000;
            font-weight: bold;
            font-size: 1.8em;
        }
    }  
    .flex-content-pay{
        display:flex;
        justify-content:space-between;
        align-items:center;
    }  
`;

const ModuleTitle = ({ title, iconUrl, renderTitleRight }) => {
    return (
        <StyledModuleTitle>
            <div className="flex-content-pay">
                <div>
                    <img src={iconUrl} alt=""/>
                    <span className="moduleTitle__label">
                        { title } 
                    </span>
                </div>
                <div>
                <span className="flex"></span>
                { renderTitleRight && renderTitleRight() }
                </div>
                
            </div>
        </StyledModuleTitle>
    );
}

export default ModuleTitle;