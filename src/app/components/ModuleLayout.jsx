import React from "react";
import styled from "styled-components";
import ModuleTitle from "./ModuleTitle";

const StyledModuleLayout = styled.div`
  padding: 50px 100px;
`;

const ModuleLayout = ({
  title,
  iconUrl,
  renderTitleRight,
  children,
  pushTop = false
}) => {
  return (
    <StyledModuleLayout>
      <ModuleTitle
        title={title}
        iconUrl={iconUrl}
        renderTitleRight={renderTitleRight}
      ></ModuleTitle>
      {pushTop && <div className="spacer-50"></div>}
      {children}
    </StyledModuleLayout>
  );
};

export default ModuleLayout;
